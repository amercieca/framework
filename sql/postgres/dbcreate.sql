drop 
  database if exists coredb;
drop 
  user if exists sucoredb;
create user sucoredb with password 'gerbil';
create database coredb with owner = sucoredb;
\c coredb;
/* ---------------------------------------------------*/
drop 
  schema if exists auth cascade;
create schema auth authorization sucoredb;
drop 
  schema if exists request cascade;
create schema request authorization sucoredb;
/* ---------------------------------------------------*/
drop 
  table if exists auth.user;
create table auth.user (
  id serial primary key, 
  fullname varchar(255) not null, 
  username varchar(60) not null, 
  password varchar(60) not null, 
  active bool not null default true
);
create unique index ndx_user_fullname on auth.user (fullname);
create unique index ndx_user_username on auth.user (username);
create index ndx_user_active on auth.user (active);
/* ---------------------------------------------------*/
drop 
  table if exists auth.role;
create table auth.role (
  id serial primary key, 
  name varchar(255) not null, 
  active bool not null default true
);
create unique index ndx_group_name on auth.role (name);
create index ndx_group_active on auth.role (active);
/* ---------------------------------------------------*/
drop 
  table if exists auth.user_role;
create table auth.user_role (
  user_id int not null references auth.user (id), 
  role_id int not null references auth.role (id), 
  active bool not null default true, 
  primary key(user_id, role_id)
);
create index ndx_user_group_active on auth.user_role (active);
/* ---------------------------------------------------*/
drop 
  table if exists auth.route;
create table auth.route (
  id serial primary key, 
  name varchar(255) not null, 
  f_create bool not null default false, 
  f_read bool not null default false, 
  f_update bool not null default false, 
  f_delete bool not null default false, 
  f_execute bool not null default false, 
  -- IMMEDIATE=0, HIGH=1, NORMAL=2, LOW=3
  priority int not null default 2 check (
    priority in (0, 1, 2, 3)
  ), 
  timeout integer not null check (timeout > -1),  -- milliseconds
  timespan int not null check (timespan > -1), 
  request_limit int not null check (request_limit > -1), 
  has_file_uploads bool not null default false, 
  file_upload_size_limit integer not null check (
    has_file_uploads = false 
    or file_upload_size_limit > 0
  ), 
  allowed_methods int not null default 3 check (
    allowed_methods between 0 
    and 3
  ), 
  active bool not null default true
);
create unique index ndx_route_name on auth.route (name);
create index ndx_route_f_create on auth.route (f_create);
create index ndx_route_f_read on auth.route (f_read);
create index ndx_route_f_update on auth.route (f_update);
create index ndx_route_f_delete on auth.route (f_delete);
create index ndx_route_f_execute on auth.route (f_execute);
create index ndx_route_priority on auth.route (priority);
create index ndx_route_active on auth.route (active);
/* ---------------------------------------------------*/
drop 
  table if exists auth.user_right;
create table auth.user_right (
  route_id int not null references auth.route(id), 
  user_id int not null references auth.user(id), 
  f_create bool not null default false, 
  f_read bool not null default false, 
  f_update bool not null default false, 
  f_delete bool not null default false, 
  f_execute bool not null default false, 
  active bool not null default true, 
  primary key (route_id, user_id)
);
create index ndx_user_right_active on auth.user_right (active);
/* ---------------------------------------------------*/
drop 
  table if exists auth.role_right;
create table auth.role_right(
  route_id integer not null references auth.route(id), 
  role_id integer not null references auth.role(id), 
  f_create bool not null default false, 
  f_read bool not null default false, 
  f_update bool not null default false, 
  f_delete bool not null default false, 
  f_execute bool not null default false, 
  active bool not null default true, 
  primary key (route_id, role_id)
);
create index ndx_role_right_active on auth.role_right (active);
/* ---------------------------------------------------*/
drop 
  table if exists request.request;
create table request.request (
  id char(36) not null primary key, 
  instant timestamp not null default NOW(), 
  route_id integer not null references auth.route (id), 
  duration bigint not null, 
  status_code int not null, 
  status_msg text default null
);
create index ndx_request_instant on request.request (instant);
create index ndx_request_route_id on request.request (route_id);
create index ndx_request_duration on request.request (duration);
create index ndx_request_status_code on request.request (status_code);
/* ---------------------------------------------------*/
drop 
  table if exists request.request_data;
create table request.request_data(
  request_id char(36) not null references request.request (id), 
  data_type char(1) check (
    data_type in ('H', 'P', 'X')
  ), 
  name varchar(256) not null check(
    LTRIM(
      RTRIM(value)
    ) <> ''
  ), 
  value text default null
);
create index ndx_request_data_request_id on request.request_data (request_id);
create index ndx_request_data_type on request.request_data (data_type);
create index ndx_request_data_name on request.request_data (name);
/* ---------------------------------------------------*/
drop 
  table if exists request.request_file;
create table request.request_file(
  request_id char(36) not null references request.request (id), 
  param_name varchar(512) not null, 
  content_type varchar(512) not null, 
  file_name text, 
  file_size bigint not null check (file_size > 0)
);
create index ndx_request_file_request_id on request.request_file (request_id);
create index ndx_request_file_param_name on request.request_file (param_name);
create index ndx_request_file_content_type on request.request_file (content_type);
/* ---------------------------------------------------*/
insert into auth.route(
  id, name, f_create, f_read, f_update, 
  f_delete, f_execute, priority, timeout, 
  timespan, request_limit, has_file_uploads, 
  file_upload_size_limit, allowed_methods, 
  active
) 
values 
  (
    default, '/not-found-route', true, 
    true, true, true, true, 0, 0, 0, 0, false, 
    0, 3, true
  );
insert into auth.route(
  id, name, f_create, f_read, f_update, 
  f_delete, f_execute, priority, timeout, 
  timespan, request_limit, has_file_uploads, 
  file_upload_size_limit, allowed_methods, 
  active
) 
values 
  (
    default, '/login-route', true, true, 
    true, true, true, 0, 0, 0, 0, false, 0, 
    3, true
  );
/* ---------------------------------------------------*/
alter table 
  auth.user owner to sucoredb;
alter table 
  auth.role owner to sucoredb;
alter table 
  auth.user_role owner to sucoredb;
alter table 
  auth.route owner to sucoredb;
alter table 
  auth.user_right owner to sucoredb;
alter table 
  auth.role_right owner to sucoredb;
alter table 
  request.request owner to sucoredb;
alter table 
  request.request_data owner to sucoredb;
alter table 
  request.request_file owner to sucoredb;
grant all privileges on schema auth to sucoredb;
grant all privileges on all tables in schema auth to sucoredb;
grant all privileges on all sequences in schema auth to sucoredb;
grant all privileges on all functions in schema auth to sucoredb;
grant all privileges on schema request to sucoredb;
grant all privileges on all tables in schema request to sucoredb;
grant all privileges on all sequences in schema request to sucoredb;
grant all privileges on all functions in schema request to sucoredb;
