drop 
  database if exists auth;
drop 
  database if exists request;
drop 
  user if exists sucoredb;
create user 'sucoredb' @'%' identified by 'gerbil';
create database auth;
create database request;
use auth;
/* ---------------------------------------------------*/
drop 
  table if exists user;
create table user (
  id int primary key auto_increment, 
  fullname varchar(255) not null, 
  username varchar(60) not null, 
  password varchar(60) not null, 
  active bool not null default true
);
create unique index ndx_user_fullname on user (fullname);
create unique index ndx_user_username on user (username);
create index ndx_user_active on user (active);
/* ---------------------------------------------------*/
drop 
  table if exists role;
create table role (
  id int primary key, 
  name varchar(255) not null, 
  active bool not null default true
);
create unique index ndx_group_name on role (name);
create index ndx_group_active on role (active);
/* ---------------------------------------------------*/
drop 
  table if exists user_role;
create table user_role (
  user_id int not null references user (id), 
  role_id int not null references role (id), 
  active bool not null default true, 
  primary key(user_id, role_id)
);
create index ndx_user_group_active on user_role (active);
/* ---------------------------------------------------*/
drop 
  table if exists route;
create table route (
  id int primary key auto_increment, 
  name varchar(255) not null, 
  f_create bool not null default false, 
  f_read bool not null default false, 
  f_update bool not null default false, 
  f_delete bool not null default false, 
  f_execute bool not null default false, 
  -- IMMEDIATE=0, HIGH=1, NORMAL=2, LOW=3
  priority int not null default 2 check (
    value in (0, 1, 2, 3)
  ), 
  timeout integer not null check (value > -1), -- milliseconds
  timespan int not null check (value > -1), 
  request_limit int not null check (value > -1), 
  has_file_uploads bool not null default false, 
  allowed_methods int not null default 3 check (value between 0 and 3),  
  active bool not null default true
);
create unique index ndx_route_name on route (name);
create index ndx_route_f_create on route (f_create);
create index ndx_route_f_read on route (f_read);
create index ndx_route_f_update on route (f_update);
create index ndx_route_f_delete on route (f_delete);
create index ndx_route_f_execute on route (f_execute);
create index ndx_route_active on route (active);
/* ---------------------------------------------------*/
drop 
  table if exists user_right;
create table user_right (
  route_id int not null references route(id), 
  user_id int not null references user(id), 
  f_create bool not null default false, 
  f_read bool not null default false, 
  f_update bool not null default false, 
  f_delete bool not null default false, 
  f_execute bool not null default false, 
  active bool not null default true, 
  primary key (route_id, user_id)
);
create index ndx_user_right_active on user_right (active);
/* ---------------------------------------------------*/
drop 
  table if exists role_right;
create table role_right(
  route_id integer not null references route(id), 
  role_id integer not null references role(id), 
  f_create bool not null default false, 
  f_read bool not null default false, 
  f_update bool not null default false, 
  f_delete bool not null default false, 
  f_execute bool not null default false, 
  active bool not null default true, 
  primary key (route_id, role_id)
);
create index ndx_role_right_active on role_right (active);
/* ---------------------------------------------------*/

/* ---------------------------------------------------*/
use request;
drop 
  table if exists request;
create table request (
  id char(36) not null primary key, 
  instant timestamp not null default NOW(), 
  route_id integer not null references auth.route (id), 
  duration bigint not null, 
  status_code int not null, 
  status_msg text default null
);
create index ndx_request_instant on request (instant);
create index ndx_request_route_id on request (route_id);
create index ndx_request_duration on request (duration);
create index ndx_request_status_code on request (status_code);
/* ---------------------------------------------------*/
drop 
  table if exists request_data;
create table request_data(
  request_id char(36) null references request (id), 
  data_type char(1) check (
    value in ('H', 'P', 'X')
  ), 
  name varchar(256) not null check(
    LTRIM(
      RTRIM(value)
    ) <> ''
  ), 
  value text default null
);
create index ndx_request_data_request_id on request_data (request_id);
create index ndx_request_data_type on request_data (data_type);
create index ndx_request_data_name on request_data (name);
/* ---------------------------------------------------*/
drop 
  table if exists request_file;
create table request_file(
  request_id char(36) not null references request (id), 
  param_name varchar(512) not null, 
  content_type varchar(512) not null, 
  file_name text, 
  file_size bigint not null check (value > 0)
);
create index ndx_request_file_request_id on request_file (request_id);
create index ndx_request_file_param_name on request_file (param_name);
create index ndx_request_file_content_type on request_file (content_type);
/* ---------------------------------------------------*/
insert into auth.route(
  id, name, f_create, f_read, f_update, 
  f_delete, f_execute, active, priority, 
  timeout, timespan, request_limit
) 
values 
  (
    default, '/not-found-route', true, true, true, 
    true, true, true, 0, 0, 0, 0
  );
alter table auth.route auto_increment = 2;  
insert into auth.route(
  id, name, f_create, f_read, f_update, 
  f_delete, f_execute, active, priority, 
  timeout, timespan, request_limit
) 
values 
  (
    default, '/login-route', true, true, 
    true, true, true, true, 0, 0, 0, 0
  );
/* ---------------------------------------------------*/  
grant all privileges on auth to 'sucoredb' @'%';
grant all privileges on route to 'sucoredb' @'%';
