select
	gr.*,
	d.name domain_name,
	g.name group_name,
	u.id user_id,
	u.username username
from
	group_right gr
inner join user_group ug on
	gr.group_id = ug.group_id
inner join "user" u on
	ug.user_id = u.id
inner join "domain" d on
	gr.domain_id = d.id
inner join "group" g on
	gr.group_id = g.id
where
	gr.active = true
	and d.active = true
	and u.active = true
	and g.active = true
	and ug.active = true
	and u.id = 250;

/*------------------------------------------------*/
select
	ur.*,
	d.name domain_name,
	u.username username
from
	user_right ur
inner join "user" u on
	ur.user_id = u.id
inner join "domain" d on
	ur.domain_id = d.id
where
	ur.active = true
	and d.active = true
	and u.active = true
	and u.id = 1;
