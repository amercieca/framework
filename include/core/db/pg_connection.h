#ifndef _PG_CONNECTION_H_C6144201DDFD4C5AA544E5F2DAECFF11
#define _PG_CONNECTION_H_C6144201DDFD4C5AA544E5F2DAECFF11
#pragma once

// C++
#include <iomanip>

// Postgres
#ifdef LINUX
#include <postgresql/libpq-fe.h>
#else
#include <libpq-fe.h>
#endif 

// project/local
#include <core/db/connection.h>

namespace core::db {

class pg_connection final : public connection {
public:
  pg_connection(const pg_connection &) = delete;
  pg_connection(pg_connection &&) = delete;
  pg_connection &operator=(pg_connection &) = delete;
  pg_connection &operator=(pg_connection &&) = delete;

  explicit pg_connection(const rapidjson::Value &);
  ~pg_connection();

  // interface
  bool active() const override;
  bool ping() const override;
  void _start_transaction() const override;
  void _commit() const override;
  void _rollback() const override;
  std::string escape_string(const std::string &) const override;
  std::string
  byte_array_to_str(const core::utils::byte_array &ba) const override;

  bool enter_single_row_mode() const;
  bool leave_single_row_mode() const;

private:
  void _connect() override;
  void _disconnect() noexcept override;

  void _preprocess_insert_statement(std::string &,
                                    const std::string &) const override;

  std::unique_ptr<rowset>
  _exec_select(const statement_handle &,
               const std::initializer_list<param> &params = {}) const override;

  result _exec_insert(const statement_handle &,
                      const std::initializer_list<param> &params = {},
                      const std::string &key_field_name =
                          core::globals::empty_string) const override;

  std::size_t
  _exec_update(const statement_handle &,
               const std::initializer_list<param> &params = {}) const override;

  std::unique_ptr<statement_handle>
  _create_statement_handle(const std::string &,
                           const std::string &) const override;

  std::unique_ptr<rowset>
  _exec_select_direct(const std::string &) const override;

  result _exec_insert_direct(const std::string &,
                             const std::string &key_field_name =
                                 core::globals::empty_string) const override;

  std::size_t _exec_update_direct(const std::string &) const override;

private:
  // field/param types Enum
  //(values retrieved from Postgres header (+Java JDBC driver implementation))
  enum class fp_type {
    INT2 = 21,
    INT4 = 23,
    INT8 = 20,
    TEXT = 25,
    NUMERIC = 1700,
    FLOAT4 = 700,
    FLOAT8 = 701,
    BOOL = 16,
    DATE = 1082,
    TIME = 1083,
    TIMETZ = 1266,
    TIMESTAMP = 1114,
    TIMESTAMPTZ = 1184,
    BYTEA = 17,
    VARCHAR = 1043,
    CHAR = 18,
    BPCHAR = 1042
  };

  // utility class wrapping PGresult*
  class pg_result final {
  public:
    pg_result(const pg_result &) = delete;
    pg_result &operator=(const pg_result &) = delete;

    explicit pg_result(PGresult *);
    pg_result(pg_result &&);
    pg_result &operator=(pg_result &&);
    ~pg_result();

    PGresult *get() const;
    PGresult *release();

  private:
    PGresult *_res;
  };

  // base class for Postgres rowset classes
  class r_set : public rowset {
  public:
    r_set(const r_set &) = delete;
    r_set(r_set &&) = delete;
    r_set &operator=(const r_set &) = delete;
    r_set &operator=(r_set &&) = delete;

    r_set(const pg_connection &, PGresult *res = nullptr);
    virtual ~r_set();

    // interface
    std::optional<bool> get_bool(int) const override;
    std::optional<bool> get_bool(const char *) const override;
    std::optional<int> get_int(int) const override;
    std::optional<int> get_int(const char *) const override;
    std::optional<long> get_long(int) const override;
    std::optional<long> get_long(const char *) const override;
    std::optional<float> get_float(int) const override;
    std::optional<float> get_float(const char *) const override;
    std::optional<double> get_double(int) const override;
    std::optional<double> get_double(const char *) const override;
    std::optional<char> get_char(int) const override;
    std::optional<char> get_char(const char *) const override;
    std::optional<std::string> get_string(int) const override;
    std::optional<std::string> get_string(const char *) const override;
    std::optional<core::datetime::date> get_date(int) const override;
    std::optional<core::datetime::date> get_date(const char *) const override;
    std::optional<std::tm> get_timestamp(int) const override;
    std::optional<std::tm> get_timestamp(const char *) const override;
    core::utils::byte_array get_bytes(int) const override;
    core::utils::byte_array get_bytes(const char *) const override;

  protected:
    const pg_connection &_conn;
    mutable PGresult *_res;

    virtual int get_row_index() const = 0;

    field_type get_field_type(int) const override;
    field_type get_field_type(const char *) const override;
    int get_field_no(const char *) const;
    const char *get_field_str(int) const;
    void clear_result() const;
    std::uint16_t num_fields() const override;
  };

  class dr_set final : public r_set {
  public:
    dr_set(const dr_set &) = delete;
    dr_set &operator=(const dr_set &) = delete;

    explicit dr_set(const pg_connection &, PGresult *res);
    dr_set(dr_set &&);
    dr_set &operator=(dr_set &&);

    // interface
    bool next() const override;

  private:
		mutable int _index{-1};
    const int _row_count{0};

    inline virtual int get_row_index() const override;
  };

  class pr_set final : public r_set {
  public:
    pr_set(const pr_set &) = delete;
    pr_set &operator=(const pr_set &) = delete;

    explicit pr_set(const pg_connection &);
    pr_set(pr_set &&);
    virtual ~pr_set();
    pr_set &operator=(pr_set &&);

    // interface
    bool next() const override;

  private:
    inline virtual int get_row_index() const override;
  };

  class pg_statement_handle final : public statement_handle {
  public:
    pg_statement_handle(const pg_statement_handle &) = delete;
    pg_statement_handle(pg_statement_handle &&) = delete;
    pg_statement_handle &operator=(const pg_statement_handle &) = delete;
    pg_statement_handle &operator=(pg_statement_handle &) = delete;

    pg_statement_handle(const pg_connection &, const std::string &);
    ~pg_statement_handle();

    std::size_t param_count() const;

    pg_result execute(const std::initializer_list<param> &) const;

    // the null value operator is only here to satisfy the compiler
    std::string operator()(const param::null&, const Oid&, const int&) const;
    std::string operator()(const bool&, const Oid&, const int&) const;
    std::string operator()(const short&, const Oid&, const int&) const;
    std::string operator()(const int&, const Oid&, const int&) const;
    std::string operator()(const long&, const Oid&, const int&) const;
	//FIXME: Check long long mapping 
    std::string operator()(const long long&, const Oid&, const int&) const;
    std::string operator()(const float&, const Oid&, const int&) const;
    std::string operator()(const double&, const Oid&, const int&) const;
    std::string operator()(const char&, const Oid&, const int&) const;
    std::string operator()(const std::string&, const Oid&, const int&) const;
    std::string operator()(const core::utils::byte_array&, const Oid&,
                           const int&) const;
    std::string operator()(const core::datetime::date&, const Oid&,
                           const int&) const;
    std::string operator()(const std::tm&, const Oid&, const int&) const;

   private:
    const pg_connection& _conn;
    const std::string _md5;
    PGresult *_res;

    template <typename T> std::string fp_to_string(const T &v) const {
      static_assert(std::is_floating_point<T>::value,
                    "T is not a floating point type");
      std::stringstream sstr{};
      sstr << std::scientific
           << std::setprecision(std::numeric_limits<T>::digits10) << v;
      return sstr.str();
    }

    void close() override;
  };

  PGconn *_pgconn;
  mutable std::string _conn_str;
  mutable std::atomic<bool> _in_single_row_mode;

  const std::string &conn_str() const;

  // static functions
  static int result_update_count(PGresult *);
  static core::utils::byte_array hex_string_to_byte_array(const std::string &);
  static std::string byte_array_to_hex_string(const core::utils::byte_array &);
  static bool is_of_type(const Oid &, const fp_type &);
  static bool is_of_type(const Oid &, const std::initializer_list<fp_type> &);

  // statics
  static const char __hex_chars[];
};

} // namespace core::db

#endif