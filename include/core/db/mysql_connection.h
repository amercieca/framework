#ifndef _MYSQL_CONNECTION_H_272623B5E97145368FE704400FB4F33D
#define _MYSQL_CONNECTION_H_272623B5E97145368FE704400FB4F33D
#pragma once

// MySql
#include <my_global.h>
#include <mysql.h>

// project/local
#include <core/db/connection.h>

namespace core::db {

class mysql_connection final : public connection {
public:
  mysql_connection(const mysql_connection &) = delete;
  mysql_connection(mysql_connection &&) = delete;
  mysql_connection &operator=(mysql_connection &) = delete;
  mysql_connection &operator=(mysql_connection &&) = delete;

  explicit mysql_connection(const rapidjson::Value &);
  ~mysql_connection();

private:
  // utility class wrapping MYSQL_RES*
  class mysql_result final {
  public:
    mysql_result(const mysql_result &) = delete;
    mysql_result &operator=(const mysql_result &) = delete;

    explicit mysql_result(MYSQL_RES *);
    mysql_result(mysql_result &&);
    mysql_result &operator=(mysql_result &&);
    ~mysql_result();

    MYSQL_RES *get() const;
    MYSQL_RES *release();

  private:
    MYSQL_RES *_res;
  };

  // interface
  bool active() const override;
  bool ping() const override;
  void _start_transaction() const override;
  void _commit() const override;
  void _rollback() const override;
  std::string escape_string(const std::string &) const override;
  std::string byte_array_to_str(const core::utils::byte_array &) const override;

  void _connect() override;
  void _disconnect() noexcept override;

  void _preprocess_insert_statement(std::string &,
                                    const std::string &) const override;

  std::unique_ptr<rowset>
  _exec_select(const statement_handle &,
               const std::initializer_list<param> &params = {}) const override;

  result _exec_insert(const statement_handle &,
                      const std::initializer_list<param> &params = {},
                      const std::string &key_field_name =
                          core::globals::empty_string) const override;

  std::size_t
  _exec_update(const statement_handle &,
               const std::initializer_list<param> &params = {}) const override;

  std::unique_ptr<connection::statement_handle>
  _create_statement_handle(const std::string &,
                           const std::string &) const override;

  std::unique_ptr<rowset>
  _exec_select_direct(const std::string &) const override;

  result _exec_insert_direct(const std::string &,
                             const std::string &key_field_name =
                                 core::globals::empty_string) const override;

  std::size_t _exec_update_direct(const std::string &) const override;

  // base class for mysql_connection rowsets
  class r_set : public rowset {
  public:
    r_set(const r_set &) = delete;
    r_set(r_set &&) = delete;
    r_set &operator=(const r_set &) = delete;
    r_set &operator=(r_set &&) = delete;

  protected:
    MYSQL *_myconn;
    MYSQL_RES *_res;
    std::vector<MYSQL_FIELD *> _fields;
    std::map<std::string, std::uint16_t> _fieldmap;

    explicit r_set(MYSQL *, MYSQL_RES *);
    virtual ~r_set();

    field_type get_field_type(int) const override;
    field_type get_field_type(const char *fname) const override;

    void init_field_data();

    int get_field_no(const char *fname) const;
  };

  // rowset class for direct queries
  class dr_set final : public r_set {
  public:
    dr_set(const dr_set &) = delete;
    dr_set(dr_set &&) = delete;
    dr_set &operator=(const dr_set &) = delete;
    dr_set &operator=(dr_set &&) = delete;

    explicit dr_set(MYSQL *, MYSQL_RES *);

  private:
    // interface
    std::uint16_t num_fields() const override;
    bool next() const override;
    std::optional<bool> get_bool(int) const override;
    std::optional<bool> get_bool(const char *) const override;
    std::optional<int> get_int(int) const override;
    std::optional<int> get_int(const char *) const override;
    std::optional<long> get_long(int) const override;
    std::optional<long> get_long(const char *) const override;
    std::optional<std::string> get_string(int) const override;
    std::optional<std::string> get_string(const char *) const override;
    std::optional<float> get_float(int) const override;
    std::optional<float> get_float(const char *) const override;
    std::optional<double> get_double(int) const override;
    std::optional<double> get_double(const char *) const override;
    std::optional<char> get_char(int) const override;
    std::optional<char> get_char(const char *) const override;
    std::optional<core::datetime::date> get_date(int) const override;
    std::optional<core::datetime::date> get_date(const char *) const override;
    std::optional<std::tm> get_timestamp(int) const override;
    std::optional<std::tm> get_timestamp(const char *) const override;
    core::utils::byte_array get_bytes(int) const override;
    core::utils::byte_array get_bytes(const char *) const override;

    mutable MYSQL_ROW _row;
    // NOTE: there is no need to free/delete _lengths below
    // because the memory allocated to is free'd internally by the
    // mysql client libray when mysql_free_result is called
    mutable std::size_t *_lengths;

    std::size_t get_field_length(const int) const;
  };

  class result_data final {
  public:
    result_data(const result_data &) = delete;
    result_data &operator=(const result_data &) = delete;

    result_data();
    result_data(result_data &&);
    ~result_data();

    result_data &operator=(result_data &&);
    void clear(const bool);

    void bind_bool(MYSQL_BIND &);
    void bind_short(MYSQL_BIND &);
    void bind_int(MYSQL_BIND &);
    void bind_long(MYSQL_BIND &);
    void bind_float(MYSQL_BIND &);
    void bind_double(MYSQL_BIND &);
    void bind_decimal(MYSQL_BIND &, const std::size_t);
    void bind_date(MYSQL_BIND &);
    void bind_timestamp(MYSQL_BIND &);
    void bind_char(MYSQL_BIND &);
    void bind_string(MYSQL_BIND &, const std::size_t);
    void bind_blob(MYSQL_BIND &bind, const std::size_t);

    bool v_bool() const;
    short v_short() const;
    int v_int() const;
    long v_long() const;
    float v_float() const;
    double v_double() const;
    char v_char() const;
    std::string v_string() const;
    core::utils::byte_array v_byte_array() const;
    core::datetime::date v_date() const;
    std::tm v_tm() const;

  private:
    union {
      bool v_bool;
      short v_short;
      int v_int;
      long v_long;
      float v_float;
      double v_double;
      char *v_decimal_string;
      char v_char;
      char *v_string;
      char *v_byte_array;
      MYSQL_TIME v_tm;
    } _data;
    field_type _field_type{field_type::NONE};
    MYSQL_BIND *_bind{nullptr};

    bool is_string() const;
    bool is_byte_array() const;
    bool is_decimal() const;
  };

  class param_bind final {
  public:
    param_bind(param_bind &&) = delete;
    param_bind &operator=(param_bind &&) = delete;

    param_bind();
    param_bind(const param_bind &);

    param_bind &operator=(const param_bind &);

    void bind(const param &p, MYSQL_BIND *);

    template <typename T> void operator()(const T &v) {
      TRACE_INIT();
      //
      static my_bool zero{0};
      static my_bool one{1};
      //
      clear();
      const auto &[is_null, type, ptr, len] = extract(v);
      _bind->buffer_type = type;
      _bind->buffer = ptr;
      _bind->buffer_length = len;
      _bind->is_null = is_null ? (my_bool *)1 : (my_bool *)0;
      _bind->is_null_value = is_null ? one : zero;
      _bind->is_unsigned = zero;
    }

  private:
    using ext_res = std::tuple<bool, enum_field_types, void *, std::size_t>;
    union {
      char v_bool;
      short v_short;
      int v_int;
      long v_long;
      float v_float;
      double v_double;
      char v_char;
      const char *v_string;
      const char *v_byte_array;
      MYSQL_TIME v_tm;
    } _data;
    mutable MYSQL_BIND *_bind{nullptr};

    void clear(const bool full = false);

    // null value
    ext_res extract(const param::null&) const;
    ext_res extract(const bool&);
    ext_res extract(const short&);
    ext_res extract(const int&);
    ext_res extract(const long&);
    ext_res extract(const float&);
    ext_res extract(const double&);
    ext_res extract(const char&);
    ext_res extract(const std::string&);
    ext_res extract(const core::utils::byte_array&);
    ext_res extract(const core::datetime::date&);
    ext_res extract(const std::tm&);
    	//FIXME: Check implications of long long
    ext_res extract(const long long&);

  };

  // rowset class for prepared queries
  class pr_set final : public r_set {
  public:
    pr_set(const pr_set &) = delete;
    pr_set(pr_set &&) = delete;
    pr_set &operator=(const pr_set &) = delete;
    pr_set &operator=(pr_set &&) = delete;

    explicit pr_set(MYSQL *, MYSQL_STMT *);

  private:
    // interface
    std::uint16_t num_fields() const override;
    bool next() const override;
    std::optional<bool> get_bool(int) const override;
    std::optional<bool> get_bool(const char *) const override;
    std::optional<int> get_int(int) const override;
    std::optional<int> get_int(const char *) const override;
    std::optional<long> get_long(int) const override;
    std::optional<long> get_long(const char *) const override;
    std::optional<std::string> get_string(int) const override;
    std::optional<std::string> get_string(const char *) const override;
    std::optional<float> get_float(int) const override;
    std::optional<float> get_float(const char *) const override;
    std::optional<double> get_double(int) const override;
    std::optional<double> get_double(const char *) const override;
    std::optional<char> get_char(int) const override;
    std::optional<char> get_char(const char *) const override;
    std::optional<core::datetime::date> get_date(int) const override;
    std::optional<core::datetime::date> get_date(const char *) const override;
    std::optional<std::tm> get_timestamp(int) const override;
    std::optional<std::tm> get_timestamp(const char *) const override;
    core::utils::byte_array get_bytes(int) const override;
    core::utils::byte_array get_bytes(const char *) const override;

    MYSQL_STMT *_stmt;
    std::vector<MYSQL_BIND> _rbinds;
    std::vector<result_data> _rdata;
    void setup_bind(MYSQL_FIELD &, const std::size_t);
  };

  class mysql_statement_handle final : public statement_handle {
  public:
    mysql_statement_handle(const mysql_statement_handle &) = delete;
    mysql_statement_handle(mysql_statement_handle &&) = delete;
    mysql_statement_handle &operator=(const mysql_statement_handle &) = delete;
    mysql_statement_handle &operator=(mysql_statement_handle &&) = delete;

    explicit mysql_statement_handle(MYSQL *, MYSQL_STMT *);
    ~mysql_statement_handle();

    void close() override;
    MYSQL_STMT *get() const;
    std::size_t param_count() const;
    void execute(const std::initializer_list<param> &params = {},
                 const bool store_result = false) const;

  private:
    MYSQL *_myconn;
    MYSQL_STMT *_stmt;
  };

  MYSQL *_myconn{nullptr};

  void exec_sql(const std::string &) const;

  // statics
  static bool is_of_type(const MYSQL_FIELD &, const enum_field_types &);
  static bool is_of_type(const MYSQL_FIELD &,
                         const std::initializer_list<enum_field_types> &);
};
} // namespace core::db

#endif