#ifndef _ROWSET_H_A32A4096C5514040913A3DC784CC44D8
#define _ROWSET_H_A32A4096C5514040913A3DC784CC44D8
#pragma once

// C++
#include <cstdint>
#include <optional>

// project/local
#include <core/db/date.h>
#include <core/db/field_type.h>
#include <core/utils/utils.h>

namespace core::db {

// forward declaration
class connection;

class rowset {
public:
  rowset(const rowset &) = delete;
  rowset(rowset &&) = delete;
  rowset &operator=(const rowset &) = delete;
  rowset &operator=(rowset &&) = delete;

  rowset();
  virtual ~rowset();

  // interface
  // FIXME: we have to add support for short here
  virtual bool next() const = 0;
  virtual std::uint16_t num_fields() const = 0;
  virtual std::optional<bool> get_bool(int) const = 0;
  virtual std::optional<bool> get_bool(const char *) const = 0;
  virtual std::optional<int> get_int(int) const = 0;
  virtual std::optional<int> get_int(const char *) const = 0;
  virtual std::optional<long> get_long(int) const = 0;
  virtual std::optional<long> get_long(const char *) const = 0;
  virtual std::optional<float> get_float(int) const = 0;
  virtual std::optional<float> get_float(const char *) const = 0;
  virtual std::optional<double> get_double(int) const = 0;
  virtual std::optional<double> get_double(const char *) const = 0;
  virtual std::optional<char> get_char(int) const = 0;
  virtual std::optional<char> get_char(const char *) const = 0;
  virtual std::optional<std::string> get_string(int) const = 0;
  virtual std::optional<std::string> get_string(const char *) const = 0;
  virtual std::optional<core::datetime::date> get_date(int) const = 0;
  virtual std::optional<core::datetime::date> get_date(const char *) const = 0;
  virtual std::optional<std::tm> get_timestamp(int) const = 0;
  virtual std::optional<std::tm> get_timestamp(const char *) const = 0;
  virtual core::utils::byte_array get_bytes(int) const = 0;
  virtual core::utils::byte_array get_bytes(const char *) const = 0;

  // helpers
  std::optional<bool> get_bool(const std::string &) const;
  std::optional<int> get_int(const std::string &) const;
  std::optional<long> get_long(const std::string &) const;
  std::optional<float> get_float(const std::string &) const;
  std::optional<double> get_double(const std::string &) const;
  std::optional<char> get_char(const std::string &) const;
  std::optional<std::string> get_string(const std::string &) const;
  std::optional<core::datetime::date> get_date(const std::string &) const;
  std::optional<std::tm> get_timestamp(const std::string &) const;

  template <typename... T, typename... F> auto get(F &&... fields) const {
    std::tuple<std::optional<T>...> tpl{};
    get_t(tpl, fields...);
    return tpl;
  }

private:
  // member functions
  template <typename T, typename F, typename... Fs>
  void get_t(T &tpl, F &&fname, Fs &&... fnames) const {
    constexpr int index = std::tuple_size<T>::value - (sizeof...(fnames) + 1);
    CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
            "invalid fname argument");
    get_v(fname, std::get<index>(tpl));
    if constexpr (sizeof...(fnames) > 0) {
      get_t(tpl, fnames...);
    }
  }

  void get_v(const char *, std::optional<bool> &) const;
  void get_v(const char *, std::optional<int> &) const;
  void get_v(const char *, std::optional<long> &) const;
  void get_v(const char *, std::optional<float> &) const;
  void get_v(const char *, std::optional<double> &) const;
  void get_v(const char *, std::optional<char> &) const;
  void get_v(const char *, std::optional<std::string> &) const;
  void get_v(const char *, std::optional<core::utils::byte_array> &) const;
  void get_v(const char *, std::optional<core::datetime::date> &) const;
  void get_v(const char *, std::optional<std::tm> &) const;

protected:
  static const std::optional<bool> null_bool;
  static const std::optional<int> null_int;
  static const std::optional<long> null_long;
  static const std::optional<float> null_float;
  static const std::optional<double> null_double;
  static const std::optional<char> null_char;
  static const std::optional<std::string> null_string;
  static const std::optional<core::datetime::date> null_date;
  static const std::optional<std::tm> null_timestamp;

  // interface functions
  virtual field_type get_field_type(int) const = 0;
  virtual field_type get_field_type(const char *) const = 0;

  // static functions
  static std::optional<bool> str_to_opt_bool(const char *, const char);
  static std::optional<int> str_to_opt_int(const char *);
  static std::optional<long> str_to_opt_long(const char *);
  static std::optional<float> str_to_opt_float(const char *);
  static std::optional<double> str_to_opt_double(const char *);
  static std::optional<char> str_to_opt_char(const char *);
  static std::optional<std::string> str_to_opt_string(const char *);
  static std::optional<core::datetime::date> str_to_opt_date(const char *);
  static std::optional<std::tm> str_to_opt_timestamp(const char *);

private:
  mutable std::recursive_mutex *_mtx{nullptr};

  // for access to _mtx above
  friend class core::db::connection;
};
} // namespace core::db

#endif