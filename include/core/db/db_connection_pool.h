#ifndef _DBPOOL_H_7EC7A5832F184859A32721E114278B1A
#define _DBPOOL_H_7EC7A5832F184859A32721E114278B1A
#pragma once

// C++
#include <atomic>
#include <thread>

// project/local
#include <core/db/connection.h>

namespace core::db {

class db_connection_pool {
 private:
  class conn final {
   public:
    // deleted functions
    conn(const conn&) = delete;
    conn& operator=(const conn&) = delete;

    conn(std::atomic<connection*>*, connection*);
    conn(conn&& o);
    conn& operator=(conn&&);

    connection* conn_obj() const;
    void release();

   private:
    // functions
    void reset();
    void check();

    // members
    std::atomic<connection*>* _slot;
    connection* _conn;
    std::atomic<bool> _released{false};
  };

 public:
  // deletes
  db_connection_pool(const db_connection_pool&) = delete;
  db_connection_pool(db_connection_pool&&) = delete;
  db_connection_pool& operator=(const db_connection_pool&) = delete;
  db_connection_pool& operator=(db_connection_pool&&) = delete;

  db_connection_pool(const rapidjson::Value&);

  void init();
  void fini();

  template <typename T>
  T exec(std::function<T(connection&, bool&)> func,
         const std::int64_t timeout = -1LL) {
    TRACE_INIT();
    // acquire a free connection
    conn c{acquire(timeout)};
    connection& conn{*c.conn_obj()};
    bool commit{true};
    // always start a transaction
    conn.start_transaction();
    try {
      // invoke the function
      if constexpr (std::is_same<T, void>::value) {
        // not returning type mode (void)
        func(conn, commit);
        if (commit) {
          conn.commit();
        } else {
          conn.rollback();
        }
        c.release();
      } else {
        // T returning type mode
        T res{func(conn, commit)};
        // commit (or rollback the transaction)
        if (commit) {
          conn.commit();
        } else {
          conn.rollback();
        }
        c.release();
        return res;
      }
    } catch (...) {
      try {
        // always rollback when an exception is thrown
        conn.rollback();
      } catch (const std::exception& ex) {
        LOG_SEVERE(
            core::utils::build_string(
                "exception encountered rolling back transaction: ", ex.what())
                .c_str());
      } catch (...) {
        LOG_SEVERE("unknown exception encountered rolling back transaction");
      }
      c.release();
      throw;
    }
  }

  // getters
  const rapidjson::Value& conf() const;
  connection::db_type db_type() const;
  const std::string& name() const;
  const std::string& host() const;
  std::uint16_t port() const;
  const std::string& user() const;
  const std::string& password() const;
  const std::string& dbname() const;
  connection::transaction_isolation_level transaction_isolation_level() const;
  std::uint16_t min_conns() const;
  std::uint16_t max_conns() const;
  std::uint32_t extra_connection_max_age() const;

 private:
  const rapidjson::Value& _conf;
  const connection::db_type _dbtype;
  const std::string _name;
  const std::string _host;
  const std::uint16_t _port;
  const std::string _user;
  const std::string _password;
  const std::string _dbname;
  const connection::transaction_isolation_level _trans_isolation_level;
  const std::uint16_t _min;
  const std::uint16_t _max;
  const std::uint32_t _exconn_max_age;
  const std::uint16_t _house_keeper_interval;
  std::vector<std::atomic<connection*>> _objs;
  std::vector<std::chrono::time_point<std::chrono::steady_clock>>
      _exconns_init_times;
  std::thread _thread{};
  std::atomic<bool> _run{false};

  // statics
  static connection* __dead_conn;
  static connection* __held_conn;

  conn acquire(const std::int64_t timeout = static_cast<std::int64_t>(-1LL));
  connection* create_connection(const std::uint16_t);
  void recreate_dead_connections() noexcept;
  void house_keeper();
  bool is_real_conn(connection*) const;
  bool check_connection(connection*) noexcept;
  void disconnect_connections() noexcept;
};

}  // namespace core::db

#endif
