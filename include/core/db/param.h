#ifndef _PARAM_H_09110F38ABED409B9874F99F69BC8041
#define _PARAM_H_09110F38ABED409B9874F99F69BC8041
#pragma once

// C++
#include <string>
#include <variant>

// local/project
#include <core/db/date.h>
#include <core/utils/utils.h>

namespace core::db {
class param final {
 public:
  // this is just a type/place-holder to denote a null value in a value_type
  // (above) variant
  struct null {
    null();
    null(const null&);
    null(null&&);
    null& operator=(const null&);
    null& operator=(null&&);
  };

  using value_type = std::variant<null, bool, short, int, long, long long, float, double,
                                  char, std::string, core::utils::byte_array,
                                  core::datetime::date, std::tm>;

  param();  // null
  param(const bool);
  param(const short);
  param(const int);
  param(const long);
  param(const long long);
  param(const float&);
  param(const double&);
  param(const char);
  param(const char*);
  param(const std::string&);
  param(const core::utils::byte_array&);
  param(const core::datetime::date&);
  param(const std::tm&);
  param(const param&);
  param(param&&);

  const value_type& value() const;

  bool is_null() const;

  std::string to_string() const;

  static const param __null_v;

 private:
  const value_type _value;
};

}  // namespace core::db

#endif