#ifndef _CONNECTION_H_76541305C6244A2DAE7795CA776D3B54
#define _CONNECTION_H_76541305C6244A2DAE7795CA776D3B54
#pragma once

#ifdef _WIN32
#undef min
#undef max
#define NOMINMAX
#endif  // WINDOWS

// C++
#include <atomic>
#include <regex>
#include <set>

// rapidjson
#include <rapidjson/document.h>

// project/local
#include <core/db/param.h>
#include <core/db/result.h>
#include <core/db/rowset.h>
#include <core/globals/globals.h>
#include <core/utils/utils.h>

namespace core::db {

class connection {
 public:
  enum class db_type { MySQL, PgSQL };

  enum class transaction_isolation_level {
    READ_UNCOMMITTED,
    READ_COMMITTED,
    REPEATABLE_READ,
    SERIALIZEABLE
  };

  connection(const connection &) = delete;
  connection(connection &&) = delete;
  connection &operator=(connection &) = delete;
  connection &operator=(connection &&) = delete;

  explicit connection(const rapidjson::Value &, const db_type);
  virtual ~connection() noexcept;

  // getters
  db_type type() const;
  transaction_isolation_level trans_isolation_level() const;
  const std::string &host() const;
  std::uint16_t port() const;
  const std::string &user() const;
  const std::string &password() const;
  const std::string &database() const;
  std::string get_info_str(const std::string &) const;

  // public interface
  virtual bool active() const = 0;
  virtual bool ping() const = 0;
  virtual std::string escape_string(const std::string &) const = 0;
  virtual std::string byte_array_to_str(
      const core::utils::byte_array &) const = 0;

  void connect();
  void disconnect();
  bool in_transaction() const;

  // exec prepared statement methods
  std::unique_ptr<rowset> exec_select(
      const std::string &, const std::initializer_list<param> &params = {},
      const std::set<std::uint16_t> &mask_indices = {});

  result exec_insert(
      const std::string &, const std::initializer_list<param> &params = {},
      const std::string &key_field_name = core::globals::empty_string,
      const std::set<std::uint16_t> &mask_indices = {});

  std::size_t exec_update(
      const std::string &, const std::initializer_list<param> &params = {},
      const std::set<std::uint16_t> &mask_indices = {});

  // exec direct methods
  std::unique_ptr<rowset> exec_select_direct(
      const std::string &, const bool escape_stmt = true) const;

  result exec_insert_direct(
      const std::string &,
      const std::string &key_field_name = core::globals::empty_string,
      const bool escape_stmt = true) const;

  std::size_t exec_update_direct(const std::string &,
                                 const bool escape_stmt = true) const;

  static transaction_isolation_level get_transaction_isolation_level(
      const rapidjson::Value &);

 protected:
  // class that holds a database specific statement handle.
  // provides an abstract 'close' function to allow for
  // database specific closure of the handle (if so required)
  // class required to accommodate prepared statements and their caching
  class statement_handle {
   public:
    statement_handle(const statement_handle &) = delete;
    statement_handle(statement_handle &&) = delete;
    statement_handle &operator=(const statement_handle &) = delete;
    statement_handle &operator=(statement_handle &&) = delete;

    statement_handle();
    virtual ~statement_handle();
    virtual void close() = 0;

   private:
    mutable std::recursive_mutex _mtx;

    friend class core::db::connection;
  };

  // pure virtual methods
  virtual void _connect() = 0;
  virtual void _disconnect() noexcept = 0;

  virtual std::unique_ptr<statement_handle> _create_statement_handle(
      const std::string &, const std::string &) const = 0;

  virtual void _preprocess_insert_statement(std::string &,
                                            const std::string &) const = 0;

  virtual void _start_transaction() const = 0;
  virtual void _commit() const = 0;
  virtual void _rollback() const = 0;

  virtual std::unique_ptr<rowset> _exec_select(
      const statement_handle &,
      const std::initializer_list<param> &params = {}) const = 0;

  virtual result _exec_insert(const statement_handle &,
                              const std::initializer_list<param> &params = {},
                              const std::string &key_field_name =
                                  core::globals::empty_string) const = 0;

  virtual std::size_t _exec_update(
      const statement_handle &,
      const std::initializer_list<param> &params = {}) const = 0;

  virtual std::unique_ptr<rowset> _exec_select_direct(
      const std::string &) const = 0;

  virtual result _exec_insert_direct(const std::string &,
                                     const std::string &key_field_name =
                                         core::globals::empty_string) const = 0;

  virtual std::size_t _exec_update_direct(const std::string &) const = 0;

  // methods
  const rapidjson::Value &conf() const;

  static std::string transaction_isolation_level_to_string(
      const transaction_isolation_level);

  static transaction_isolation_level string_to_transaction_isolation_level(
      const std::string &);

  static bool is_select_statement(const std::string &);

  static bool is_insert_statement(const std::string &);

  static bool is_update_statement(const std::string &);

 protected:
  mutable std::recursive_mutex _mtx;

  void sync_exec(std::function<void()> f) const {
    std::lock_guard<std::recursive_mutex> lg{_mtx};
    f();
  }

 private:
  // class that provides a map of prepared (cached) statements
  class prepared_statement_map final
      : private std::map<std::string, statement_handle *> {
   private:
    using base = std::map<std::string, statement_handle *>;

   public:
    prepared_statement_map(const prepared_statement_map &) = delete;
    prepared_statement_map(prepared_statement_map &&) = delete;
    prepared_statement_map &operator=(const prepared_statement_map &) = delete;
    prepared_statement_map &operator=(prepared_statement_map &&) = delete;

    explicit prepared_statement_map(const connection &);
    ~prepared_statement_map();

    const statement_handle &get(const std::string &);

    using base::empty;

    void clear();

   private:
    const connection &_conn;
    mutable std::recursive_mutex _mtx;
  };

  const rapidjson::Value &_conf;
  const db_type _dbtype;
  const std::string _host;
  const std::uint16_t _port;
  const std::string _user;
  const std::string _password;
  const std::string _database;
  const transaction_isolation_level _trans_isolation_level;
  mutable std::atomic<bool> _in_transaction{false};
  mutable prepared_statement_map _prepared_statement_map;

  // methods
  void start_transaction() const;
  void commit() const;
  void rollback() const;

  std::string process_statement(
      const std::string &, const bool escape_stmt = true,
      const std::string &key_field_name = core::globals::empty_string) const;

  // static methods
  static const char *db_type_to_str(const db_type);

  static std::string params_to_string(
      const std::initializer_list<param> &,
      const std::set<std::uint16_t> &mask_indices = {});

  static bool match_statement(const std::regex &, const std::string &);

  // statics
  static const std::regex __rx_insert;
  static const std::regex __rx_select;
  static const std::regex __rx_update;

  // friends
  friend class db_connection_pool;  // for start_transaction, commit, rollback
                                    // functions
};
}  // namespace core::db

#endif