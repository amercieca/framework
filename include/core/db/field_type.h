#ifndef _FIELD_TYPE_H_C11E3639FBAC4CF8A1FBA6E6F41249C1
#define _FIELD_TYPE_H_C11E3639FBAC4CF8A1FBA6E6F41249C1
#pragma once

namespace core::db {

enum class field_type {
  NONE,
  BOOL,
  SHORT,
  INT,
  LONG,
  FLOAT,
  DOUBLE,
  CHAR,
  STRING,
  BYTE_ARRAY,
  DATE,
  TIMESTAMP
};
}

#endif
