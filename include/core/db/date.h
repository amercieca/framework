#ifndef _DATE_H_81B84F256FFB49F7BB16AA6680941A2F
#define _DATE_H_81B84F256FFB49F7BB16AA6680941A2F
#pragma once

// C++
#include <ctime>
#include <optional>
#include <string>

namespace core::datetime {

// Note: this class (date) is not thread-safe;
// specifically it's assignment and move assignment operators
// are not thread safe
class date final {
 public:
  explicit date(const unsigned short, const unsigned short, const short);
  explicit date(const std::tm&,
                const std::optional<int>& offset = std::optional<int>{});
  explicit date(const std::time_t&, const bool);
  date(date&&);
  date(const date&);

  date& operator=(const date&);
  date& operator=(date&&);

  unsigned short day() const;
  unsigned short month() const;
  short year() const;
  bool is_valid() const;
  std::string to_string() const;

 private:
  const unsigned short _day{0};
  const unsigned short _month{0};
  const short _year{0};

  void init(const std::tm&);
  std::time_t to_time_t() const;
  std::tm to_tm() const;

  static bool is_leap_year(const short);

  static const char* const __UTC;
};

}  // namespace core::datetime
#endif