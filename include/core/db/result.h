#ifndef RESULT_H_D07E93978BC644859787FBCBD8806A84
#define RESULT_H_D07E93978BC644859787FBCBD8806A84
#pragma once

// C++
#include <cstdint>
#include <vector>

namespace core::db {
//
class result {
 public:
  explicit result(const std::size_t, const std::vector<std::size_t>&);
  explicit result(const std::size_t, std::vector<std::size_t>&&);
  result(const result&);
  result(result&&);
  result& operator=(const result&);
  result& operator=(result&&);

  // getters
  std::size_t update_count() const;
  const std::vector<std::size_t>& generated_keys() const;

 private:
  const std::size_t _update_count{0UL};
  const std::vector<std::size_t> _generated_keys;

  void reset();
};
}  // namespace core::db

#endif