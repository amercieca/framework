#ifndef _AUTH_H_3DE0E5F6A46747D9961AB0D2F405C93F
#define _AUTH_H_3DE0E5F6A46747D9961AB0D2F405C93F
#pragma once

// C++
#include <cstdint>

namespace core::auth {

enum class flag {
  create = 0x10,
  read = 0x8,
  update = 0x4,
  del = 0x2,
  exec = 0x1
};

std::uint8_t compile_auth_flags(const bool, const bool, const bool, const bool,
                                const bool);
}  // namespace core::auth

#endif