#ifndef _USER_INFO_H_63C7AF6E692B4764BEBEA47E864BF484
#define _USER_INFO_H_63C7AF6E692B4764BEBEA47E864BF484
#pragma once

#ifdef _WIN32
#undef min
#undef max
#define NOMINMAX
#endif  // WINDOWS

// C++
#include <set>

// rapidjson
#include <rapidjson/document.h>

namespace core::auth {

class user_info final {
 public:
  user_info(const std::int32_t);
  user_info(const user_info &);
  user_info(user_info &&);
  user_info &operator=(const user_info &);
  user_info &operator=(user_info &&);
  bool operator<(const user_info &) const;

  void set_route_access(const std::int32_t, const std::uint8_t);
  std::uint8_t add_route_access(const std::int32_t, const std::uint8_t);
  void prune_route_access();
  void add_role(const std::int32_t);

  std::int32_t id() const;
  const std::set<std::int32_t> &roles() const;
  const std::map<std::int32_t, std::uint8_t> &route_access() const;

  rapidjson::Document to_json() const;
  bool can_create(const std::int32_t) const;
  bool can_read(const std::int32_t) const;
  bool can_update(const std::int32_t) const;
  bool can_delete(const std::int32_t) const;
  bool can_exec(const std::int32_t) const;

  static user_info from_json_string(const std::string &);

 private:
  const std::int32_t _id;
  std::set<std::int32_t> _roles;
  std::map<std::int32_t, std::uint8_t> _route_access;

  bool has_access(const std::int32_t, const std::uint8_t) const;

  static void check_values(const user_info &);
};

}  // namespace core::auth

#endif