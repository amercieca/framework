#ifndef _ROUTE_H_1BB460B1BC474863BEAAB17D7C90B9D8
#define _ROUTE_H_1BB460B1BC474863BEAAB17D7C90B9D8
#pragma once

// C++
#include <functional>
#include <mutex>
#include <string>

// Poco
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

// project/local
#include <core/auth/user_info.h>
#include <core/db/connection.h>
#include <core/globals/globals.h>
#include <core/http/result.h>

namespace core::http {

class route {
 public:
  using route_factory_func = std::function<route*(
      const std::string&, const std::int32_t, const std::uint8_t,
      const Poco::Net::HTTPServerRequest::PRIORITY, const std::uint32_t,
      const std::uint32_t, const std::uint32_t, const bool, const std::uint32_t,
      const std::uint32_t)>;

  // struct to hold file upload data
  struct file_data {
    const std::string param_name;
    const std::string content_type;
    const std::string file_name;
    const std::string file_path;
    const long file_size;
    mutable bool retain{false};
  };

  static const std::string __CT_PLAIN_TEXT;
  static const std::string __CT_TEXT_HTML;
  static const std::string __CT_APPLICATION_JSON;
  static const std::string __ROUTE_NOT_FOUND;
  static const std::string __ROUTE_LOGIN;
  static const std::int32_t __ROUTE_ID_NOT_FOUND;
  static const std::int32_t __ROUTE_ID_LOGIN;

  route(const route&) = delete;
  route(route&&) = delete;
  route& operator=(const route&) = delete;
  route& operator=(route&&) = delete;

  route(const std::int32_t, const std::string&, const std::uint8_t,
        const Poco::Net::HTTPServerRequest::PRIORITY, const std::uint32_t,
        const std::uint32_t, const std::uint32_t, const bool,
        const std::uint32_t, const std::uint32_t);

  virtual ~route();

  bool operator<(const route&) const;

  std::int32_t id() const;

  const std::string& path() const;

  std::uint8_t flags() const;

  Poco::Net::HTTPServerRequest::PRIORITY priority() const;

  std::uint32_t timeout() const;

  std::uint32_t time_span() const;

  std::uint32_t request_limit() const;

  bool has_file_uploads() const;

  std::uint32_t file_upload_size_limit() const;

  std::uint32_t allowed_methods() const;

  bool is_method_allowed(const std::string&) const;

  bool has_too_many_requests() const;

  // pure abstract function to be overriden by derived classes
  virtual void handle_request(Poco::Net::HTTPServerRequest&,
                              Poco::Net::HTTPServerResponse&,
                              const core::auth::user_info&, const std::string&,
                              const Poco::Net::HTMLForm*,
                              const std::list<file_data>&) const = 0;

  // static functions
  static void load(core::db::connection&, route_factory_func);

  static void send_ok_response(Poco::Net::HTTPServerRequest&,
                               Poco::Net::HTTPServerResponse&,
                               core::http::result&);

  static void send_error_response(
      Poco::Net::HTTPServerRequest&, Poco::Net::HTTPServerResponse&,
      Poco::Net::HTTPResponse::HTTPStatus, const std::string&,
      const std::string& message = core::globals::empty_string);

  static const route* get_route(const std::string&);

 protected:
  const std::int32_t _id;
  const std::string _path;
  const std::uint8_t _flags;
  const Poco::Net::HTTPServerRequest::PRIORITY _priority;
  const std::uint32_t _timeout;
  const std::uint32_t _time_span;
  const std::uint32_t _request_limit;
  const bool _has_file_uploads;
  const std::uint32_t _file_upload_size_limit;  // KB
  const std::uint32_t _allowed_methods;
  mutable std::set<std::uint64_t> _time_points;
  mutable std::mutex _mtx;

  static std::map<std::string, std::unique_ptr<route>> __routes;
  static std::mutex __mtx;

  static void set_response_result_value(Poco::Net::HTTPServerResponse& response,
                                        const std::string&);
};

}  // namespace core::http

#endif