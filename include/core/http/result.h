#ifndef _RESULT_H_52AD435BCAA9416EAE1E5A997A998F3E
#define _RESULT_H_52AD435BCAA9416EAE1E5A997A998F3E
#pragma once

#ifdef _WIN32
#undef min
#undef max
#define NOMINMAX
#endif //WINDOWS

// C++
#include <chrono>
#include <string>

// rapidjson
#include <rapidjson/document.h>

// project/local
#include <core/utils/utils.h>

namespace core::http {

class result {
 public:
  result(const result&) = delete;
  result(result&&) = delete;
  result& operator=(const result&) = delete;
  result& operator=(result&&) = delete;

  result(const std::string&,
         const std::chrono::milliseconds instant = core::utils::now(),
         const std::uint16_t status_code = 200,
         const std::string& status_msg = "OK");

  result(const std::string&, const std::uint16_t, const std::string&);

  // getters
  const std::string& uuid() const;
  std::chrono::milliseconds instant() const;
  std::uint16_t status_code() const;
  const std::string& status_msg() const;

  template <typename T>
  result& set(const std::string& name, const T& v) {
    TRACE_INIT();
    //
    CHECK_T(std::invalid_argument, !name.empty(),
            "invalid name parameter (empty)");
    remove_member(name);
    _payload->AddMember(rapidjson::Value{name, _alloc}.Move(), v, _alloc);
    return *this;
  }

  result& set(const std::string& name, const std::string&);
  result& set(const std::string&, rapidjson::Value&);
  result& set(rapidjson::Value&);
  void remove_member(const std::string&);
  const rapidjson::Document& to_json();

 private:
  const std::string _uuid;
  const std::chrono::milliseconds _instant;
  std::uint16_t _status_code;
  std::string _status_msg;
  rapidjson::Document _doc{rapidjson::kObjectType};
  rapidjson::Document::AllocatorType& _alloc;
  rapidjson::Value* _payload{nullptr};
};

}  // namespace core::http

#endif