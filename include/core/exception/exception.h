#ifndef _EXCEPTION_H_5B28636F1F44472FBD86045EE7E4E7EF
#define _EXCEPTION_H_5B28636F1F44472FBD86045EE7E4E7EF
#pragma once

// C++
#include <stdexcept>
#include <string>

namespace core::exception {

class time_out : public std::runtime_error {
 public:
  explicit time_out(const char*);
  explicit time_out(const std::string&);
};

}  // namespace core::exception

#endif