#ifndef _GLOBALS_H_5DEBE550281D4750A89932EC979E7CB2
#define _GLOBALS_H_5DEBE550281D4750A89932EC979E7CB2
#pragma once

// C++
#include <string>

namespace core::globals {

extern const std::string empty_string;

}  // namespace core::globals

#endif