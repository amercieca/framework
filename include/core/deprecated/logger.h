#ifndef _LOGGER_H_E79180C68F684DD380169871260962B6
#define _LOGGER_H_E79180C68F684DD380169871260962B6
#pragma once

// C++
#include <cstdint>
#include <deque>
#include <list>
#include <mutex>
#include <stack>
#include <string>
#include <thread>

namespace core::utils {

class trace final {
 public:
  trace() = delete;
  trace(const trace &) = delete;
  trace(trace &&) = delete;

 private:
  // custom stack to enable iteration on it's underlying items
  class stack : public std::stack<std::string, std::deque<std::string>> {
   public:
    auto rbegin();
    auto rend();
    auto rbegin() const;
    auto rend() const;

   private:
    using base = std::stack<std::string, std::deque<std::string>>;
  };

  static thread_local stack __stack;

  static void push(std::string &&);
  static void pop();
  static std::string dump_stack_trace();

  friend class tracer;
  friend class logger;
};

class tracer final {
 public:
  tracer() = delete;
  tracer(const tracer &) = delete;
  tracer(tracer &&) = delete;
  tracer &operator=(const tracer &) = delete;
  tracer &operator=(tracer &&) = delete;

  tracer(const char *, const char *, std::size_t);
  ~tracer();
  const std::string &file() const;
  const std::string &func() const;
  std::size_t line() const;

 private:
  const std::string _file;
  const std::string _func;
  const std::size_t _line;

  std::string make_source_file_info_string() const;
};

class logger {
 public:
  enum class bucket : std::uint8_t {
    NONE = 0x0,
    INFO = 0x1,
    DEBUG = 0x2,
    SQL = 0x4,
    WARN = 0x8,
    SEVERE = 0x10,
    ALL = 0xFF
  };

  static void init(const std::initializer_list<bucket> &, std::size_t,
                   const char *, const char *, const char *);

  static void fini();

  static inline bool is_enabled(const bucket bckt) {
    return static_cast<std::uint64_t>(bckt) & __buckets;
  }

  static void log(const bucket, const char *, const char *, const int,
                  const char *) noexcept;

 private:
  // static class (no constructors/destructors, copy/move, etc.)
  logger() = delete;
  logger(const logger &) = delete;
  logger &operator=(const logger &) = delete;
  logger(logger &&) = delete;
  logger &operator=(logger &&) = delete;
  ~logger() = delete;

  struct message final {
    message() = delete;
    message(const message &) = delete;
    message &operator=(const message &) = delete;

    explicit message(const bucket, const std::thread::id &, const char *,
                     const char *, const std::size_t, std::string &&);
    message(message &&);
    message &operator=(message &&);

    void reset();

    const bucket _bucket;
    const struct timeval _tv;
    const std::thread::id _thread_id;
    const std::string _file;
    const std::string _func;
    const std::size_t _line;
    const std::string _msg;
    const std::string _trace;
  };

  enum class flag : std::uint8_t {
    RUN = 0x1,
    QUEUE_ACCEPT = 0x2,
    FILES_ERROR = 0x4
  };

  static std::recursive_mutex __mtx;
  static std::atomic<std::uint8_t> __flags;
  static std::ofstream __ofs;
  static std::atomic<std::list<message> *> __msgs;
  static std::thread __thrd;
  static std::uint64_t __buckets;
  static std::int64_t __log_file_size;
  static std::string __log_file_dir;
  static std::string __log_file_prefix;
  static std::string __log_file_suffix;
  static const char *__bucket_strings[];

  static void runner();

  static struct timeval get_tod();

  static struct tm to_tm(const std::time_t);

  static std::ostream &get_ostr() noexcept;

  static const char *get_bucket_str(const bucket);

  static void log_message(const message &, std::ostream &);

  static void log_message(const bucket, const std::thread::id &,
                          const struct timeval &, const char *, const char *,
                          const char *, std::size_t, const char *,
                          std::ostream &);

  static std::string truncate_message(const char *);

  static bool is_flag_set(const flag);

  static std::uint8_t set_flag(const flag);

  static std::uint8_t reset_flag(const flag);

  static std::string get_stack_trace(const bucket);
};

class logger_ctrl final {
 public:
  logger_ctrl() = delete;
  logger_ctrl(const logger_ctrl &o) = delete;
  logger_ctrl &operator=(const logger_ctrl &o) = delete;
  logger_ctrl(logger_ctrl &&o) = delete;
  logger_ctrl &operator=(logger_ctrl &&o) = delete;

  logger_ctrl(const std::initializer_list<logger::bucket> &, std::size_t,
              const char *, const char *, const char *);
  ~logger_ctrl();
};

}  // namespace core::utils

#endif
