#ifndef _SERVER_APP_H_0588B4F437454A04B2D660FE61E37CAC
#define _SERVER_APP_H_0588B4F437454A04B2D660FE61E37CAC
#pragma once

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#endif  // WINDOWS

// C++
#include <regex>
#include <set>

// Poco
#include <Poco/Crypto/Cipher.h>
#include <Poco/Crypto/DigestEngine.h>
#include <Poco/HMACEngine.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Util/ServerApplication.h>

// local/project
#include <core/auth/user_info.h>
#include <core/db/db_connection_pool.h>
#include <core/http/route.h>

namespace core::app {

class web_server_app final : private Poco::Util::ServerApplication {
 private:
  // classes

  // logging subsystem
  class logging final : public Poco::Util::Subsystem {
   public:
    // deletes
    logging(const logging &) = delete;
    logging(logging &&) = delete;
    logging &operator=(const logging &) = delete;
    logging &operator=(logging &&) = delete;

    logging();

    const char *name() const;
    void initialize(Poco::Util::Application &);
    void uninitialize();

   private:
    const std::string _name;
  };

  // DB subsystem
  class db final : public Poco::Util::Subsystem {
   public:
    // deletes
    db(const db &) = delete;
    db(db &&) = delete;
    db &operator=(const db &) = delete;
    db &operator=(db &&) = delete;

    db();

    const char *name() const;
    void initialize(Poco::Util::Application &);
    void uninitialize();
    core::db::db_connection_pool &get_db_conn(const char *) const;
    core::db::db_connection_pool &get_db_conn(const std::string &) const;

   private:
    const std::string _name;
    std::map<std::string, std::unique_ptr<core::db::db_connection_pool>>
        _conn_pool_map{};
    rapidjson::Document _conf{};
    mutable std::mutex _mtx{};
  };

  // Auth subsystem
  class auth final : public Poco::Util::Subsystem {
   private:
    // struct that contains a user's authorisation info
    struct auth_info final {
      auth_info();
      auth_info(const auth_info &);
      auth_info(auth_info &&);
      auth_info &operator=(const auth_info &);
      auth_info &operator=(auth_info &&);

      std::int64_t not_before{-1L};
      std::int64_t issued_at{-1L};
      std::int64_t expiry{-1L};
      std::string token{};
      std::optional<core::auth::user_info> user_info{};
      bool has_error{false};
      std::string error_msg{};
      bool login_required{false};
    };

   public:
    auth(const auth &) = delete;
    auth(auth &&) = delete;
    auth &operator=(const auth &) = delete;
    auth &operator=(auth &&) = delete;

    auth(const Poco::Util::LayeredConfiguration &);

    const char *name() const;
    void initialize(Poco::Util::Application &);
    void uninitialize();

    const core::auth::user_info load_user_info(const std::int32_t,
                                               core::db::connection &) const;

    std::optional<core::auth::user_info> authenticate(
        const std::string &, const std::string &) const;

    std::string gen_jwt(const core::auth::user_info &) const;

    auth_info check_credentials(const int64_t, const std::string &) const;

    std::string encrypt(const std::string &) const;
    std::string decrypt(const std::string &) const;

   private:
    struct SHA256_engine final : public Poco::Crypto::DigestEngine {
      SHA256_engine() : Poco::Crypto::DigestEngine{"SHA256"} {}
      enum { BLOCK_SIZE = 64, DIGEST_SIZE = 32 };
    };

    core::db::db_connection_pool *_db;
    const std::string _name;
    const std::string _jwt_header;
    const std::string _jwt_issuer;
    const std::string _jwt_subject;
    const std::string _jwt_audience;
    const std::int64_t _jwt_lifetime;
    const std::int64_t _refresh_interval;
    //_user_info_map: map indexed/keyed by user id,
    // containing a user_info instance and the time this entry was
    // issued/created (in an std::pair)
    mutable std::map<std::int32_t,
                     std::pair<core::auth::user_info, std::int64_t>>
        _user_info_map;
    mutable Poco::Crypto::Cipher::Ptr _cipher;
    mutable Poco::HMACEngine<SHA256_engine> _hmac;
    mutable std::recursive_mutex _mtx;

    const core::auth::user_info load_user_info(const std::int32_t) const;
    const core::auth::user_info &get_user_info(const std::int64_t,
                                               const std::int32_t) const;
    void set_user_info(const std::int64_t, const core::auth::user_info &) const;
    void erase_user_info(const std::int32_t) const;
  };

  // HTTP server subsystem
  class web_server final : public Poco::Util::Subsystem {
   public:
    // deletes
    web_server(const web_server &) = delete;
    web_server(web_server &&) = delete;
    web_server &operator=(const web_server &) = delete;
    web_server &operator=(web_server &&) = delete;

    // constructor
    web_server(const Poco::Util::LayeredConfiguration &, const auth &,
               core::http::route::route_factory_func);

    const char *name() const;
    void initialize(Poco::Util::Application &);
    void uninitialize();

   private:
    ///////////////////////////////////////////////////////////////////////////
    // class to forward create request handler factory requests to
    // web_server instance (just a router/delegate)
    class request_handler_factory final
        : public Poco::Net::HTTPRequestHandlerFactory {
     public:
      request_handler_factory() = delete;
      request_handler_factory(const request_handler_factory &) = delete;
      request_handler_factory &operator=(const request_handler_factory &) =
          delete;

      request_handler_factory(web_server &);

      inline Poco::Net::HTTPRequestHandler *createRequestHandler(
          const Poco::Net::HTTPServerRequest &,
          Poco::Net::HTTPServerRequest::PRIORITY &, std::uint32_t &) override;

     private:
      web_server &_ws_obj;
    };

    ///////////////////////////////////////////////////////////////////////////
    // class to forward request handling to us (just a router/delegate)
    class request_handler final : public Poco::Net::HTTPRequestHandler {
     public:
      request_handler() = delete;
      request_handler(const request_handler &) = delete;
      request_handler &operator=(const request_handler &) = delete;

      request_handler(web_server &, const bool, const core::http::route *);

      inline void handleRequest(Poco::Net::HTTPServerRequest &,
                                Poco::Net::HTTPServerResponse &) override;

     private:
      web_server &_ws_obj;
      const bool _is_login_request;
      const core::http::route *_route;
    };

    Poco::Net::HTTPServer _server;
    const std::string _name;
    const std::string _login_route_path;
    const auth &_auth;
    core::http::route::route_factory_func _rff;

    std::string get_request_path(const Poco::Net::HTTPServerRequest &) const;

    Poco::Net::HTTPRequestHandler *create_request_handler(
        const Poco::Net::HTTPServerRequest &,
        Poco::Net::HTTPServerRequest::PRIORITY &, std::uint32_t &);

    void log_request(const std::string &, Poco::Net::HTTPServerRequest &);

    void handle_login_request(Poco::Net::HTTPServerRequest &,
                              Poco::Net::HTTPServerResponse &,
                              const std::string &, const Poco::Net::HTMLForm *);

    void handle_request(Poco::Net::HTTPServerRequest &,
                        Poco::Net::HTTPServerResponse &, const bool,
                        const core::http::route *);
  };

 public:
  // deletes
  web_server_app(const web_server_app &) = delete;
  web_server_app(web_server_app &&) = delete;
  web_server_app &operator=(const web_server_app &) = delete;
  web_server_app &operator=(web_server_app &&) = delete;

  // constructor
  web_server_app(core::http::route::route_factory_func);
  ~web_server_app();

  // instance methods
  void initialize(Poco::Util::Application &) override;
  void uninitialize() override;
  std::string fix_config(Poco::Util::Application &);
  int main(const std::vector<std::string> &) override;

  // public static methods
  static int run(int, char **, core::http::route::route_factory_func);
  static core::db::db_connection_pool &get_db_conn(const char *);
  static core::db::db_connection_pool &get_db_conn(const std::string &);

  // instance variables
  logging *_logging{nullptr};
  db *_db{nullptr};
  auth *_auth{nullptr};
  web_server *_ws{nullptr};
  core::http::route::route_factory_func _rff;

  // static variables
  static Poco::SharedPtr<web_server_app> __instance;
  static std::atomic<bool> __initialised;
  static std::atomic<bool> __terminated;

  // static functions
  static bool terminated();
};

}  // namespace core::app

#endif
