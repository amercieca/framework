#ifndef _MD5_H_FA1DA7F5B04F487CB172A7EE0F0D5B09
#define _MD5_H_FA1DA7F5B04F487CB172A7EE0F0D5B09

// C++
#include <string>

namespace core::utils {

class md5 final {
 public:
  // static class (no instantiations)
  md5() = delete;
  md5(const md5&) = delete;
  md5(md5&&) = delete;
  ~md5() = delete;
  md5& operator=(const md5&) = delete;
  md5& operator=(md5&&) = delete;

  static std::string compute(const void*, unsigned long);
  static std::string compute(const std::string&);
};

}  // namespace core::utils

#endif
