#ifndef _SIMPLE_ENCRYPT_H_B98BC0F34AEB4EC0B8286D14CFBC14D0
#define _SIMPLE_ENCRYPT_H_B98BC0F34AEB4EC0B8286D14CFBC14D0
#pragma once

#include <string>

namespace core::utils {

class simple_encrypt final {
 public:
  simple_encrypt() = delete;
  simple_encrypt(const simple_encrypt&) = delete;
  simple_encrypt(simple_encrypt&&) = delete;
  simple_encrypt& operator=(const simple_encrypt&) = delete;
  simple_encrypt& operator=(simple_encrypt&&) = delete;

  static std::string gen_key();
  static std::string encrypt(const std::string&, const std::string&);
  static std::string decrypt(const std::string&, const std::string&);

 private:
};
}  // namespace core::utils

#endif