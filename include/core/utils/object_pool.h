#ifndef _OBJECT_POOL_H_5113EB95263E4D868C97ABA6ADA54AF0
#define _OBJECT_POOL_H_5113EB95263E4D868C97ABA6ADA54AF0
#pragma once

// C++
#include <atomic>
#include <chrono>
#include <cstdint>
#include <functional>
#include <iostream>
#include <thread>
#include <utility>

// project/local
#include <core/exception/exception.h>

namespace core::utils {

template <typename T, std::uint16_t N>
class object_pool final {
 public:
  // deleted functions
  object_pool(const object_pool&) = delete;
  object_pool(object_pool&&) = delete;
  object_pool& operator=(const object_pool&) = delete;
  object_pool& operator=(object_pool&&) = delete;

  class ref final {
   private:
    using data = std::pair<std::atomic<T*>*, T*>;

   public:
    ref()
        : _data_a{nullptr, nullptr},
          _data_b{nullptr, nullptr},
          _data{nullptr} {}

    explicit ref(std::atomic<T*>* s, T* o)
        : _data_a{s, o}, _data_b{nullptr, nullptr}, _data{&_data_a} {
      check<std::invalid_argument>(slot(), "invalid slot parameter (null)");
      check<std::invalid_argument>(obj(), "invalid obj parameter (null)");
    }

    ref(ref&& o)
        : _data_a{o.slot(), o.obj()},
          _data_b{nullptr, nullptr},
          _data{&_data_a} {
      o.reset();
    }

    ref& operator=(ref&& o) {
      if (this != &o) {
        switch_data(o.slot(), o.obj());
        o.reset();
      }
      return *this;
    }

    ~ref() { release(); }

    void release() {
      if (slot()) {
        check(obj(), "object is null when slot is not null!");
        T* null_obj = nullptr;
        check((*slot()).compare_exchange_strong(null_obj, obj()),
              "error reinstating object into pool");
      }
      reset();
    }

    bool is_null() const { return obj == nullptr; }

    T* operator->() { return obj(); }

    const T* operator->() const { return obj(); }

    operator T*() { return obj(); }

    operator const T*() const { return obj(); }

    operator T&() {
      T* o = obj();
      check(o != nullptr, "cannot return T& when _obj is null");
      return *o;
    }

    T& operator*() {
      T* o = obj();
      check(o, "cannot return T& when _obj is null");
      return *o;
    }

   private:
    // deleted functions
    ref(const ref&) = delete;
    ref& operator=(const ref&) = delete;

    std::atomic<T*>* slot() const {
      return _data ? _data.load()->first : nullptr;
    }

    T* obj() const { return _data ? _data.load()->second : nullptr; }

    void switch_data(std::atomic<T*>* slot, T* obj) {
      (_data == &_data_a ? _data_b : _data_a) = std::make_pair(slot, obj);
      _data.store = _data == &_data_a ? &_data_b : &_data_a;
    }

    // functions
    void reset() {
      _data_a = std::make_pair(nullptr, nullptr);
      _data_b = std::make_pair(nullptr, nullptr);
      _data = nullptr;
    }

    // members
    data _data_a;
    data _data_b;
    std::atomic<data*> _data{nullptr};
  };

  template <typename... A>
  object_pool(std::function<void(T&)> preinvk_func = nullptr,
              std::function<void(T&)> fini_func = nullptr, A&&... args)
      : _preinvk_func{preinvk_func}, _fini_func{fini_func} {
    for (std::uint16_t i = 0; i < N; ++i) {
      _objs[i] = new T{std::forward<A>(args)...};
    }
  }

  ~object_pool() {
    for (unsigned int i = 0; i < N; ++i) {
      T* obj = _objs[i].exchange(nullptr);
      if (obj != nullptr) {
        if (_fini_func) {
          try {
            _fini_func(*obj);
          }
          // absorb everything in destructor
          catch (const std::exception& ex) {
            std::cerr << "exception encountered calling finalisation function "
                         "on object @ ["
                      << i << "]: " << ex.what() << std::endl;
          } catch (...) {
            std::cerr << "unknown exception encountered calling finalisation "
                         "function on object @ ["
                      << i << ']' << std::endl;
          }
        }
        delete obj;
      }
    }
  }

  ref acquire(const std::int64_t timeout = -1LL) {
    if (timeout != -1LL) {
      check<std::invalid_argument>(timeout > 0LL, "invalid timeout argument");
    }
    const auto start = std::chrono::steady_clock::now();
    std::atomic<T*>* slot;
    T* obj = nullptr;
    while (true) {
      slot = nullptr;
      obj = nullptr;
      for (std::uint16_t i = 0; i < N; ++i) {
        T* tobj = _objs[i];
        if (tobj != nullptr &&
            _objs[i].compare_exchange_strong(tobj, nullptr)) {
          // object successfully acquired
          obj = tobj;
          slot = &_objs[i];
          break;
        }
      }
      if (obj != nullptr) {
        break;
      }
      // check for timeout; if it's elapsed, throw an exception
      if (timeout != -1LL) {
        const std::int64_t elapsed_ms =
            std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now() - start)
                .count();
        check<core::exception::time_out>(
            elapsed_ms < timeout,
            "timeout encountered whilst acquiring object instance");
      }
      // avoid CPU spin
      std::this_thread::sleep_for(std::chrono::milliseconds(33));
    }
    ref r(slot, obj);
    // call the init function
    if (_preinvk_func) {
      _preinvk_func(*obj);
    }
    return r;
  }

  template <typename R>
  R exec(std::function<R(T&)> func) {
    // invoke the actual function
    if constexpr (std::is_same_v<R, void>) {
      func(*acquire());
    } else {
      return func(*acquire());
    }
  }

  std::uint16_t use_count() {
    std::uint16_t res = 0;
    for (const auto& i : _objs) {
      res += i ? 1 : 0;
    }
    return res;
  }

 private:
  // members
  std::array<std::atomic<T*>, N> _objs;
  std::function<void(T&)> const _preinvk_func;
  std::function<void(T&)> const _fini_func;

  template <typename E = std::runtime_error>
  static void check(const bool cond, const char* msg) {
    static_assert(std::is_constructible_v<E, const char*>,
                  "E is not a type constructible from const char*");
    static_assert(std::is_move_constructible<T>::value,
                  "E is not a move constructible type");
    static_assert(std::is_move_assignable<T>::value,
                  "E is not a move assignable type");
    if (!cond) {
      throw E(msg);
    }
  }
};
}  // namespace core::utils

#endif