#ifndef _UTILS_H_1EED73AF8C874B3DAB8A435CBB5F8614
#define _UTILS_H_1EED73AF8C874B3DAB8A435CBB5F8614
#pragma once

#ifdef _WIN32
#undef min
#undef max
#define NOMINMAX

#include <cctype>
#include <iomanip>
#endif  // WINDOWS

// C++
#include <chrono>
#include <functional>
#include <iostream>
#include <mutex>
#include <sstream>

// RapidJSON
#include <rapidjson/document.h>

// Poco
#include <Poco/Logger.h>
#include <Poco/Util/Application.h>

// fmt
#define FMT_STRING_ALIAS 1
#include <fmt/format.h>

// project/local
#include <core/utils/trace.h>

///////////////////////////////////////////////////////////////////////////////
// logging macros
#define LOG_INFO(a)                                                           \
  poco_information(Poco::Logger::get("INFO   "),                              \
                   fmt::format("{:s} ({:s}:{:d}. [{:s}])", (a), __FILENAME__, \
                               __LINE__, __CFUNC__));

#define LOG_DEBUG(a)                                                    \
  poco_debug(Poco::Logger::get("DEBUG  "),                              \
             fmt::format("{:s} ({:s}:{:d}. [{:s}])", (a), __FILENAME__, \
                         __LINE__, __CFUNC__));

#define LOG_SQL(a)                                                            \
  poco_information(Poco::Logger::get("SQL    "),                              \
                   fmt::format("{:s} ({:s}:{:d}. [{:s}])", (a), __FILENAME__, \
                               __LINE__, __CFUNC__));

#define LOG_WARN(a)                                                       \
  poco_warning(Poco::Logger::get("WARNING"),                              \
               fmt::format("{:s} ({:s}:{:d}. [{:s}])", (a), __FILENAME__, \
                           __LINE__, __CFUNC__));

#define LOG_SEVERE(a)                                                   \
  poco_error(Poco::Logger::get("SEVERE "),                              \
             fmt::format("{:s} ({:s}:{:d}. [{:s}]\r\n[\r\n{:s}])", (a), \
                         __FILENAME__, __LINE__, __CFUNC__,             \
                         core::utils::trace_mgr::dump_stack_trace()));

// check (assert-like) macros
#define CHECK(c, ...) \
  core::utils::check((c), __FILENAME__, __CFUNC__, __LINE__, __VA_ARGS__)

#define CHECK_T(t, c, ...) \
  core::utils::check<t>((c), __FILENAME__, __CFUNC__, __LINE__, __VA_ARGS__)

#define CHECK_F(c, b) \
  core::utils::check_f((c), __FILENAME__, __CFUNC__, __LINE__, (b))

#define CHECK_F_T(t, c, b) \
  core::utils::check_f<t>((c), __FILENAME__, __CFUNC__, __LINE__, (b))

// other utility macros
#define SOURCE_FILE_INFO() \
  fmt::format("{:s}:{:s}:{:d}", __FILENAME__, __CFUNC__, __LINE__)

namespace core::utils {

///////////////////////////////////////////////////////////////////////////////
// functions

inline bool is_str_null(const char *str) { return str == nullptr || *str == 0; }

template <typename T, typename... A>
void print(T &&t, A &&... args) {
  static std::recursive_mutex _mtx_{};
  //
  std::lock_guard<std::recursive_mutex> lg{_mtx_};
  std::cout << t << ' ';
  if constexpr (sizeof...(args) > 0) {
    print(args...);
  } else {
    std::cout << std::endl;
  }
}

template <typename T, typename... A>
void concat(std::stringstream &sstr, T &&t, A &&... args) {
  TRACE_INIT();
  sstr << t;
  if constexpr (sizeof...(args) > 0) {
    concat(sstr, args...);
  }
}

template <typename... A>
std::string build_string(A &&... args) {
  TRACE_INIT();
  std::stringstream sstr{};
  concat(sstr, args...);
  return sstr.str();
}

template <typename T = std::runtime_error, typename... A>
void check(const bool cond, const char *file, const char *func,
           std::size_t line, A &&... args) {
  static_assert(std::is_constructible_v<T, const char *>,
                "T is not a type constructible from const char*");
  static_assert(std::is_move_constructible<T>::value,
                "T is not a move constructible type");
  static_assert(std::is_move_assignable<T>::value,
                "T is not a move assignable type");
  // no tracing here
  if (!cond) {
    const std::string msg(
        core::utils::build_string<A...>(std::forward<A>(args)...));
    poco_error(Poco::Logger::get("SEVERE "),
               fmt::format("{:s} ({:s}:{:d}. [{:s}])", msg, file, line, func));
    throw T(msg.c_str());
  }
}

template <typename T = std::runtime_error>
void check_f(const bool cond, const char *file, const char *func,
             std::size_t line, std::function<std::string()> str_func) {
  static_assert(std::is_constructible_v<T, const char *>,
                "T is not a type constructible from const char*");
  static_assert(std::is_move_constructible_v<T>,
                "T is not a move constructible type");
  static_assert(std::is_move_assignable_v<T>,
                "T is not a move assignable type");
  // no tracing here
  if (!cond) {
    const std::string msg(str_func());
    poco_error(Poco::Logger::get("SEVERE "),
               fmt::format("{:s} ({:s}:{:d}. [{:s}])", msg, file, line, func));
    throw T(msg.c_str());
  }
}

std::string get_os_error_string();

std::string json_to_string(const rapidjson::Value &json,
                           const bool pretty = false);

rapidjson::Document string_to_json(const std::string &);

std::string gen_uuid();

std::time_t now_time_t();

std::string &string_trim(std::string &);

std::string string_trim(const std::string &);

///////////////////////////////////////////////////////////////////////////////
// template functions

template <typename T>
std::tuple<bool, std::string, T> string_to_fp(const char *str) {
  TRACE_INIT();
  static_assert(std::is_floating_point_v<T>, "T is not a floating point type");
  CHECK(!core::utils::is_str_null(str), "invalid str parameter");
  char *e{nullptr};
  // float
  if constexpr (std::is_same_v<T, float>) {
    const auto r{std::strtof(str, &e)};
    if (r == HUGE_VALF) {
      return std::make_tuple(
          false, "string conversion result out of range for type float", 0.0f);
    }
    if (r == 0.0f && e == str) {
      return std::make_tuple(false, "string conversion to float failed", 0.0f);
    }
    return std::make_tuple(true, "", r);
  }
  // double
  if constexpr (std::is_same_v<T, double>) {
    const auto r{std::strtod(str, &e)};
    if (r == HUGE_VAL) {
      return std::make_tuple(
          false, "string conversion result out of range for type double", 0.0);
    }
    if (r == 0.0 && e == str) {
      return std::make_tuple(false, "string conversion to double failed", 0.0);
    }
    return std::make_tuple(true, "", r);
  }
  // long double
  if constexpr (std::is_same_v<T, long double>) {
    const auto r{std::strtold(str, &e)};
    if (r == HUGE_VALL) {
      return std::make_tuple(
          false, "string conversion result out of range for type long double",
          0.0L);
    }
    if (r == 0.0L) {
      return std::make_tuple(false, "string conversion to long double failed",
                             0.0L);
    }
    return std::make_tuple(true, "", r);
  }
  CHECK(false, "unhandled floating point type in call to string_to_fp");
  throw 0;  // there is here only to mute compiler warning
}

template <typename T>
static T get_json_value(const rapidjson::Value &value, const char *param_name) {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(param_name),
          "invalid param_name parameter");
  CHECK_T(std::invalid_argument, value.IsObject(),
          "json value is not of object type");
  const auto &i{value.FindMember(param_name)};
  CHECK(i != value.MemberEnd(),
        fmt::format("missing [{:s}] value", param_name));
  const auto &v{i->value};

  // string
  if constexpr (std::is_same_v<T, std::string>) {
    CHECK(v.IsString(),
          fmt::format("parameter [{:s}] is not of string type", param_name));
    return std::string{v.GetString()};
  }

  // float
  if constexpr (std::is_same_v<T, float>) {
    CHECK(v.IsNumber(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    const double d{v.GetFloat()};
    CHECK(d >= std::numeric_limits<float>::min() &&
              d <= std::numeric_limits<float>::max(),
          "double value conversion to float would cause overflow/narrowing");
    return static_cast<float>(d);
  }

  // double
  if constexpr (std::is_same_v<T, double>) {
    CHECK(v.IsNumber(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    return v.GetDouble();
  }

  // short
  if constexpr (std::is_same_v<T, short>) {
    CHECK(v.IsNumber(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    const double d{v.GetDouble()};
    CHECK(d >= static_cast<double>(std::numeric_limits<short>::min()) &&
              d <= static_cast<double>(std::numeric_limits<short>::max()),
          "numeric value conversion to short would cause overflow/narrowing");
    return static_cast<short>(d);
  }

  // unsigned short
  if constexpr (std::is_same_v<T, unsigned short>) {
    CHECK(v.IsNumber(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    const double d{v.GetDouble()};
    CHECK(
        d >= static_cast<double>(std::numeric_limits<unsigned short>::min()) &&
            d <=
                static_cast<double>(std::numeric_limits<unsigned short>::max()),
        "numeric value conversion to unsigned short would cause "
        "overflow/narrowing");
    return static_cast<unsigned short>(d);
  }

  // int
  if constexpr (std::is_same_v<T, int>) {
    CHECK(v.IsNumber(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    const double d{v.GetDouble()};
    CHECK(d >= static_cast<double>(std::numeric_limits<int>::min()) &&
              d <= static_cast<double>(std::numeric_limits<int>::max()),
          "numeric value conversion to int would cause overflow/narrowing");
    return static_cast<int>(d);
  }

  // unsigned int
  if constexpr (std::is_same_v<T, unsigned int>) {
    CHECK(v.IsNumber(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    const double d{v.GetDouble()};
    CHECK(
        d >= static_cast<double>(std::numeric_limits<unsigned int>::min()) &&
            d <= static_cast<double>(std::numeric_limits<unsigned int>::max()),
        "numeric value conversion to int would cause overflow/narrowing");
    return static_cast<unsigned int>(d);
  }

  // long
  if constexpr (std::is_same_v<T, long>) {
    CHECK(v.IsNumber(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    const double d{v.GetDouble()};
    CHECK(d >= static_cast<double>(std::numeric_limits<long>::min()) &&
              d <= static_cast<double>(std::numeric_limits<long>::max()),
          "numeric value conversion to int would cause overflow/narrowing");
    return static_cast<long>(d);
  }

  // unsigned long
  if constexpr (std::is_same_v<T, unsigned long>) {
    CHECK(v.IsNumber(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    const double d{v.GetDouble()};
    CHECK(
        d >= static_cast<double>(std::numeric_limits<unsigned long>::min()) &&
            d <= static_cast<double>(std::numeric_limits<unsigned long>::max()),
        "numeric value conversion to int would cause overflow/narrowing");
    return static_cast<unsigned long>(d);
  }

  // bool
  if constexpr (std::is_same_v<T, bool>) {
    CHECK(v.IsBool(),
          fmt::format("parameter [{:s}] is not of numeric type", param_name));
    return v.GetBool();
  }
  CHECK(false, "unhandled type T in call to get_json_value");
  throw 0;  // only here to mute compiler warning;
}

template <typename T = std::chrono::milliseconds>
T now() {
  return std::chrono::duration_cast<T>(
      std::chrono::system_clock::now().time_since_epoch());
}

///////////////////////////////////////////////////////////////////////////////
// structs
template <typename T>
struct is_pointer {
  static const bool value = false;
};

template <typename T>
struct is_pointer<T *> {
  static const bool value = true;
};

///////////////////////////////////////////////////////////////////////////////
// classes

///////////////////////////////////////////////////////////////////////////////
// class timer
class timer {
 public:
  timer(timer &&) = delete;
  timer &operator=(timer &&) = delete;

  timer();
  timer(const timer &);
  timer &operator=(const timer &);
  void reset();
  template <typename T = std::chrono::nanoseconds>
  std::int64_t elapsed(const bool r = false) {
    std::int64_t res = std::chrono::duration_cast<T>(
                           std::chrono::high_resolution_clock::now() - _beg)
                           .count();
    if (r) {
      reset();
    }
    return res;
  }

 private:
  std::chrono::time_point<std::chrono::high_resolution_clock> _beg;
};

///////////////////////////////////////////////////////////////////////////////
class byte_array final {
 public:
  byte_array();
  explicit byte_array(const std::size_t);
  explicit byte_array(char *);
  explicit byte_array(const char *);
  explicit byte_array(unsigned char *, const std::size_t);
  explicit byte_array(const unsigned char *, const std::size_t);
  explicit byte_array(const std::string &);
  byte_array(const byte_array &);
  byte_array(byte_array &&);
  ~byte_array();

  byte_array &operator=(const byte_array &);
  byte_array &operator=(byte_array &&);

  const char *get() const;
  std::size_t length() const;
  bool is_str() const;
  bool is_null() const;

 private:
  char *_data;
  std::size_t _length;
  bool _is_str;

  void reset(const bool free);

  static char *copy_string(const char *);
  static char *copy_data(const unsigned char *, std::size_t);
};  // namespace core::utils

///////////////////////////////////////////////////////////////////////////////
template <typename T>
class unique_ptr_array final
    : private std::unique_ptr<T, std::function<void(T *)>> {
 private:
  using base = std::unique_ptr<T, std::function<void(T *)>>;

 public:
  unique_ptr_array(const unique_ptr_array &) = delete;
  unique_ptr_array &operator=(const unique_ptr_array &) = delete;

  unique_ptr_array()
      : base{nullptr, [this](T *) { del(); }},
        _length{0UL},
        _content_owner{false} {
    TRACE_INIT();
  }

  explicit unique_ptr_array(T *p, const std::size_t length,
                            const bool content_owner, const bool init)
      : base{p, [this](T *) { del(); }},
        _length{length},
        _content_owner{content_owner} {
    TRACE_INIT();
    if (_length > 0UL) {
      CHECK(p != nullptr, "invalid p parameter");
    }
    if (init) {
      do_init();
    }
  }

  unique_ptr_array(unique_ptr_array &&o)
      : base{o.release()},
        _length{o._length},
        _content_owner{o._content_owner} {
    TRACE_INIT();
    o.reset(nullptr);
    o._length = 0UL;
    o._content_owner = false;
  }

  unique_ptr_array &operator=(unique_ptr_array &&o) {
    TRACE_INIT();
    if (this != &o) {
      base::reset(o.release());
      _length = o._length;
      _content_owner = o._content_owner;
      o._length = 0UL;
      o._content_owner = false;
    }
    return *this;
  }

  std::size_t length() const {
    TRACE_INIT();
    return _length;
  }
  bool empty() const {
    TRACE_INIT();
    return _length < 1UL;
  }
  bool content_owner() const {
    TRACE_INIT();
    return _content_owner;
  }

  void reset(T *v, const std::size_t length, const bool content_owner,
             const bool init) {
    TRACE_INIT();
    if (length > 0UL) {
      CHECK_T(std::invalid_argument, v != nullptr, "invalid length parameter");
    }
    base::reset(v);
    ((std::size_t &)_length) = length;
    ((bool &)_content_owner) = content_owner;
    if (init) {
      do_init();
    }
  }

  using base::get;

 private:
  const std::size_t _length;
  const bool _content_owner;

  void do_init() { std::memset(base::get(), 0, sizeof(T) * _length); }

  void del() {
    if (base::get() == nullptr) {
      return;
    }
    // if this instance is an array of T pointers,
    // then we have to call delete on each object
    // in the array
    if (_content_owner && is_pointer<T>::value) {
      for (std::size_t i = 0; i < _length; ++i) {
        del(base::get()[i]);
      }
    }
    delete[] base::release();
  }

  template <typename C>
  void del(C &) {}

  template <typename C>
  void del(C *p) {
    delete p;
  }
};

///////////////////////////////////////////////////////////////////////////////
class raii_func final {
 public:
  using func = std::function<void()>;
  raii_func(const raii_func &) = delete;
  raii_func(raii_func &&) = delete;
  raii_func &operator=(const raii_func &) = delete;
  raii_func &operator=(raii_func &&) = delete;

  explicit raii_func(func);
  ~raii_func();

 private:
  func _f;
};

//////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
char *strptime(const char *s, const char *f, struct tm *tm);
#endif

}  // namespace core::utils

#endif
