#ifndef _BASE64_URL_H_0743129E1448423D81AD9041A39EE498
#define _BASE64_URL_H_0743129E1448423D81AD9041A39EE498
#pragma once

// C++
#include <string>
#include <vector>

namespace core::utils {

class base64_url {
 public:
  // static class - no instantiations
  base64_url() = delete;
  base64_url(const base64_url&) = delete;
  base64_url(base64_url&&) = delete;
  base64_url& operator=(const base64_url&) = delete;
  base64_url& operator=(base64_url&&) = delete;

  static std::string encode(
      const char*, const std::size_t length = static_cast<std::size_t>(-1L));
  static std::string encode(const std::string&);
  static std::string encode(const std::vector<unsigned char>&);
  static std::vector<unsigned char> decode(const std::string&);

 private:
  static const std::string __alphabet;
  static const std::map<char, unsigned char> __alphabet_map;
};
}  // namespace core::utils

#endif