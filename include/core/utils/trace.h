#ifndef _TRACE_H_4394361B230D4BF99E3E13381A55FCDC
#define _TRACE_H_4394361B230D4BF99E3E13381A55FCDC
#pragma once

// C++
#include <memory>
#include <stack>
#include <string>

namespace core::utils {

class trace_mgr final {
 public:
  trace_mgr() = delete;
  trace_mgr(const trace_mgr &) = delete;
  trace_mgr(trace_mgr &&) = delete;
  trace_mgr &operator=(const trace_mgr &) = delete;
  trace_mgr &&operator=(trace_mgr &&) = delete;

  static std::string dump_stack_trace();
  static void dump_stack_trace(std::ostream &);

 private:
  // custom stack to enable iteration on it's underlying items
  class stack : public std::stack<std::string, std::deque<std::string>> {
   public:
    auto rbegin();
    auto rend();
    auto rbegin() const;
    auto rend() const;

   private:
    using base = std::stack<std::string, std::deque<std::string>>;
  };

  static thread_local std::unique_ptr<stack> __stack;

  static inline void push(std::string &&);
  static inline void pop();
  static inline stack &get_stack();

  friend class trace_ctx;
  friend class stack_cleaner;
};

class trace_ctx final {
 public:
  trace_ctx() = delete;
  trace_ctx(const trace_ctx &) = delete;
  trace_ctx(trace_ctx &&) = delete;
  trace_ctx &operator=(const trace_ctx &) = delete;
  trace_ctx &operator=(trace_ctx &&) = delete;

  trace_ctx(const char *, const char *, std::size_t);
  ~trace_ctx();
  const std::string &file() const;
  const std::string &func() const;
  std::size_t line() const;

 private:
  const std::string _file;
  const std::string _func;
  const std::size_t _line;
};

}  // namespace core::utils

///////////////////////////////////////////////////////////////////////////////
// tracing macros
#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#ifdef __GNUC__
#define __CFUNC__ __FUNCTION__
#elif _MSVC_LANG
#define __CFUNC__ __FUNCTION__
#else
#error "please specify macro expansion for __CFUNC__"
#endif

#if __CTRACE__ == 1
#define TRACE_INIT() \
  core::utils::trace_ctx ____tracer { __FILENAME__, __CFUNC__, __LINE__ }
#else
#define TRACE_INIT()
#endif

#endif