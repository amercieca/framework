#ifndef _PCH_H_382AC0FA77164CBEA64649A9E93EC153
#define _PCH_H_382AC0FA77164CBEA64649A9E93EC153
#pragma once

#ifdef _WIN32
#undef min
#undef max
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#endif //WINDOWS

///////////////////////////////////////////////////////////////////////////////
// includes
// C++
#include <atomic>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <functional>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mutex>
#include <regex>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <tuple>

// fmt
#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <fmt/posix.h>
#include <fmt/printf.h>
#include <fmt/ranges.h>
#include <fmt/time.h>

// RapidJSON
#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

// Poco
#include <Poco/AsyncChannel.h>
#include <Poco/FileChannel.h>
#include <Poco/FormattingChannel.h>
#include <Poco/LogStream.h>
#include <Poco/Logger.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/PatternFormatter.h>
#include <Poco/SimpleFileChannel.h>
#include <Poco/Util/AbstractConfiguration.h>

// core
#include <core/app/serverapp.h>
#include <core/db/connection.h>
#include <core/db/date.h>
#include <core/db/db_connection_pool.h>
#include <core/db/field_type.h>
#include <core/db/mysql_connection.h>
#include <core/db/param.h>
#include <core/db/pg_connection.h>
#include <core/db/result.h>
#include <core/db/rowset.h>
#include <core/exception/exception.h>
#include <core/globals/globals.h>
#include <core/utils/base64_url.h>
#include <core/utils/md5.h>
#include <core/utils/object_pool.h>
#include <core/utils/trace.h>
#include <core/utils/utils.h>

#endif
