Building on Linux
=================
1) mkdir build && cd build
2) conan install ..
3) cmake .. -G "Unix Makefiles" 
4) cmake --build .

Build on Windows (Debug mode)
=============================
1) mkdir build && cd build
2) conan install .. -s build_type=Debug
3) cmake .. -G "Visual Studio 15 2017 Win64" -DCMAKE_BUILD_TYPE=Debug
4) cmake --build .

Build on Windows (Release mode)
=============================
1) mkdir build && cd build
2) conan install ..
3) cmake .. -G "Visual Studio 15 2017 Win64" -DCMAKE_BUILD_TYPE=Release
4) cmake --build .

*Note: At the end modify bin/x.json with proper parameters for db connections

