// C++
#include <chrono>
#include <random>

// Poco
#include <Poco/Crypto/Cipher.h>
#include <Poco/Crypto/CipherFactory.h>
#include <Poco/Crypto/CipherKey.h>
#include <Poco/Crypto/CipherKeyImpl.h>
#include <Poco/Crypto/Crypto.h>
#include <Poco/UUIDGenerator.h>

// local/project
#include <app/pch.h>
#include <core/app/serverapp.h>
#include <core/auth/auth.h>
#include <core/utils/simple_encrypt.h>

///////////////////////////////////////////////////////////////////////////////

class file_route : public core::http::route {
 public:
  file_route(const std::int32_t id, const std::string &path,
             const std::uint8_t flags,
             const Poco::Net::HTTPServerRequest::PRIORITY priority,
             const std::uint32_t timeout, const std::uint32_t time_span,
             const std::uint32_t request_limit, const bool has_file_uploads,
             const std::uint32_t file_upload_size_limit,
             const std::uint32_t allowed_methods)
      : core::http::route{id,
                          path,
                          flags,
                          priority,
                          timeout,
                          time_span,
                          request_limit,
                          has_file_uploads,
                          file_upload_size_limit,
                          allowed_methods} {}

 private:
  virtual void handle_request(Poco::Net::HTTPServerRequest &request,
                              Poco::Net::HTTPServerResponse &response,
                              const core::auth::user_info &,
                              const std::string &request_id,
                              const Poco::Net::HTMLForm *,
                              const std::list<file_data> &) const {
    TRACE_INIT();
    //
    send_ok_response(request, response,
                     core::http::result{request_id}.set("ok", "ok"));
  }
};

core::http::route *create_route(const std::string &path, const std::int32_t id,
                                const std::uint8_t flags,
                                Poco::Net::HTTPServerRequest::PRIORITY priority,
                                const std::uint32_t timeout,
                                const std::uint32_t timespan,
                                const std::uint32_t request_limit,
                                const bool has_file_upload,
                                const std::uint32_t file_upload_size_limit,
                                const std::uint32_t allowed_methods) {
  TRACE_INIT();
  //
  if (path == "/file") {
    return new file_route{id, path,
                          //
                          flags, priority, timeout, timespan, request_limit,
                          has_file_upload, file_upload_size_limit,
                          allowed_methods};
  }
  CHECK(false, fmt::format("Unhandled route: {:s}", path));
  throw 0;
}

int main(int argc, char *argv[]) {
  TRACE_INIT();
  //
  try {
    if (argc > 1 && std::string(argv[1]) == "--keygen") {
      std::cout << core::utils::simple_encrypt::gen_key() << std::endl;
      return 0;
    }
    //
    return core::app::web_server_app::run(argc, argv, create_route);
  } catch (const std::exception &ex) {
    std::cerr << fmt::format("Exception encountered: {:s}", ex.what());
    return -1;
  } catch (...) {
    std::cerr << "Unknown exception encountered" << std::endl;
    return -1;
  }
}
