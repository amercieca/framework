// project/local
#include <core/exception/exception.h>
#include <core/utils/trace.h>

namespace core::exception {

time_out::time_out(const char* what) : std::runtime_error{what} {
  TRACE_INIT();
  //
}

time_out::time_out(const std::string& what) : std::runtime_error{what} {
  TRACE_INIT();
  //
}

}  // namespace core::exception