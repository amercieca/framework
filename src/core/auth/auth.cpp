// project/local
#include <core/auth/auth.h>
#include <core/utils/trace.h>

namespace core::auth {

std::uint8_t compile_auth_flags(const bool create, const bool read,
                                const bool update, const bool del,
                                const bool exec) {
  TRACE_INIT();
  //
  return ((create ? static_cast<std::uint8_t>(flag::create) : 0) |
          (read ? static_cast<std::uint8_t>(flag::read) : 0) |
          (update ? static_cast<std::uint8_t>(flag::update) : 0) |
          (del ? static_cast<std::uint8_t>(flag::del) : 0) |
          (exec ? static_cast<std::uint8_t>(flag::exec) : 0));
}

}  // namespace core::auth
