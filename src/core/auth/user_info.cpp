
// C++
#include <map>
#include <set>

// project/local
#include <core/auth/auth.h>
#include <core/auth/user_info.h>
#include <core/utils/utils.h>

namespace core::auth {

user_info::user_info(const std::int32_t id) : _id{id} {
  TRACE_INIT();
  //
  check_values(*this);
}

user_info::user_info(const user_info &o)
    : _id{o._id}, _roles{o._roles}, _route_access{o._route_access} {
  TRACE_INIT();
  //
  check_values(*this);
}

user_info::user_info(user_info &&o)
    : _id{o._id}, _roles{o._roles}, _route_access{o._route_access} {
  TRACE_INIT();
  //
  check_values(*this);
}

user_info &user_info::operator=(const user_info &o) {
  TRACE_INIT();
  //
  if (this != &o) {
    check_values(o);
    *((std::int32_t *)&_id) = o._id;
    _roles = o._roles;
    _route_access = o._route_access;
  }
  return *this;
}

user_info &user_info::operator=(user_info &&o) {
  TRACE_INIT();
  //
  if (this != &o) {
    check_values(o);
    *((std::int32_t *)&_id) = o._id;
    _roles = o._roles;
    _route_access = o._route_access;
  }
  return *this;
}

bool user_info::operator<(const user_info &o) const {
  TRACE_INIT();
  //
  if (this == &o) {
    return false;
  }
  return _id < o._id;
}

void user_info::set_route_access(const std::int32_t route_id,
                                 const std::uint8_t flags) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, route_id >= 0, "invalid route_id parameter");
  if (flags == 0U) {
    // if flags are zero, then remove the route entry (if present)
    auto i{_route_access.find(route_id)};
    if (i != _route_access.end()) {
      _route_access.erase(i);
    }
    // else don't do anything (no point in adding an entry
    // for a route with zero flags)
  } else {
    // set (i.e. overwrite) any existing flags for this route
    _route_access[route_id] = flags;
  }
}

std::uint8_t user_info::add_route_access(const std::int32_t route_id,
                                         const std::uint8_t flags) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, route_id >= 0, "invalid route_id parameter");
  auto i{_route_access.find(route_id)};
  if (i == _route_access.end()) {
    // new route entry
    if (flags == 0U) {
      // if this is a new route entry but with no (0) flags being set
      // then don't add the entry and return zero as
      // the current flags for this route
      return 0U;
    }
    // else, add the new entry for this route (with the flags passed in)
    CHECK(_route_access.insert(std::make_pair(route_id, flags)).second,
          fmt::format(
              "could not add access rights flags for user: {:d} / route: {:d}",
              _id, route_id));
    // return the flags as the current flags for this (new) route
    return flags;
  } else {
    // add (by or'ing) these flags to the existing route entry found
    i->second |= flags;
    if (i->second == 0U) {
      // if the flags for this route are now 0, them remove the route entry
      _route_access.erase(i);
      // and return 0 as the current flags for this route
      return 0U;
    } else {
      // else, return the new, composite flags as the current flags
      // for this route
      return i->second;
    }
  }
}

void user_info::prune_route_access() {
  TRACE_INIT();
  //
  for (auto i{_route_access.begin()}; i != _route_access.end();) {
    if (i->second == 0U) {
      i = _route_access.erase(i);
    } else {
      ++i;
    }
  }
}

void user_info::add_role(const std::int32_t role_id) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, role_id >= 0, "invalid role_id parameter");
  _roles.insert(role_id);
}

std::int32_t user_info::id() const {
  TRACE_INIT();
  //
  return _id;
}

const std::set<std::int32_t> &user_info::roles() const {
  TRACE_INIT();
  //
  return _roles;
}

const std::map<std::int32_t, std::uint8_t> &user_info::route_access() const {
  TRACE_INIT();
  //
  return _route_access;
}

rapidjson::Document user_info::to_json() const {
  TRACE_INIT();
  //
  rapidjson::Document d{rapidjson::kObjectType};
  auto &a{d.GetAllocator()};
  // id
  d.AddMember("id", _id, a);
  // roles
  rapidjson::Value roles{rapidjson::kArrayType};
  for (const auto r : _roles) {
    roles.PushBack(r, a);
  }
  d.AddMember("roles", roles, a);
  //
  // access rights (stored as:)
  // first (most significant) 32 bits: route id
  // next 8 bits: ignored/not used
  // next 8 bits: access flags
  rapidjson::Value access{rapidjson::kArrayType};
  for (const auto [id, f] : _route_access) {
    access.PushBack(
        (((static_cast<std::uint64_t>(id) << 32) &
          static_cast<std::uint64_t>(0xFFFFFFFF00000000)) |
         (static_cast<std::uint64_t>(f) & static_cast<std::uint64_t>(0xFF))),
        a);
  }
  d.AddMember("route_access", access, a);
  return d;
}

bool user_info::can_create(const std::int32_t rid) const {
  TRACE_INIT();
  //
  return has_access(rid, static_cast<std::uint8_t>(core::auth::flag::create));
}

bool user_info::can_read(const std::int32_t rid) const {
  TRACE_INIT();
  //
  return has_access(rid, static_cast<std::uint8_t>(core::auth::flag::read));
}

bool user_info::can_update(const std::int32_t rid) const {
  TRACE_INIT();
  //
  return has_access(rid, static_cast<std::uint8_t>(core::auth::flag::update));
}

bool user_info::can_delete(const std::int32_t rid) const {
  TRACE_INIT();
  //
  return has_access(rid, static_cast<std::uint8_t>(core::auth::flag::del));
}

bool user_info::can_exec(const std::int32_t rid) const {
  TRACE_INIT();
  //
  return has_access(rid, static_cast<std::uint8_t>(core::auth::flag::exec));
}

bool user_info::has_access(const std::int32_t rid,
                           const std::uint8_t flag) const {
  TRACE_INIT();
  //
  const auto i{_route_access.find(rid)};
  if (i == _route_access.end()) {
    return false;
  }
  return (i->second & flag) == flag;
}

user_info user_info::from_json_string(const std::string &str) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !str.empty(), "invalid str parameter");
  rapidjson::Document d{};
  d.Parse(str);
  const auto id{core::utils::get_json_value<std::int32_t>(d, "id")};
  CHECK(id >= 0,
        "invalid user_id value ({:d}) extracted from json string: {:s}", id,
        str);
  user_info ui{id};
  CHECK(d.HasMember("roles"), "JSON object contains no 'roles' member");
  const auto &roles{d["roles"].GetArray()};
  for (const auto &r : roles) {
    ui._roles.insert(static_cast<std::int32_t>(r.GetInt()));
  }
  CHECK(d.HasMember("route_access"),
        "JSON object contains no 'route_access' member");
  // access rights (stored as:)
  // first (most significant) 32 bits: route id
  // next 8 bits: ignored/not used
  // next 8 bits: access flags
  const auto &route_access{d["route_access"].GetArray()};
  for (const auto &ra : route_access) {
    const auto v{static_cast<std::uint64_t>(ra.GetInt64())};
    const auto route_id{static_cast<std::int32_t>(v >> 32)};
    CHECK(route_id >= 0,
          fmt::format("invalid route_id value encountered: {:d}", route_id));
    ui._route_access[route_id] = static_cast<std::uint8_t>(v & 0xFF);
  }
  check_values(ui);
  return ui;
}

void user_info::check_values(const user_info &ui) {
  TRACE_INIT();
  //
  CHECK(ui._id >= 0, "Invalid _id value");
  for (const auto &v : ui._roles) {
    CHECK(v >= 0, fmt::format("invalid role value: {:d}", v));
  }
  for (const auto &v : ui._route_access) {
    CHECK(v.first >= 0, fmt::format("invalid route id value: {:d}", v.first));
    CHECK(v.second >= 0,
          fmt::format("invalid route access flags value: {:d}", v.second));
  }
}

}  // namespace core::auth