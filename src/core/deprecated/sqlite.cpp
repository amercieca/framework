
#if 0


//NOTE: this code is just here so as to not throw it away at this stage


class sqlite final {
 public:
  // deletes
  sqlite(const sqlite &) = delete;
  sqlite(sqlite &&) = delete;
  sqlite &operator=(const sqlite &) = delete;
  sqlite &operator=(sqlite &&) = delete;

  // constructor
  sqlite() : _db_name{} {}

  // destructor
  ~sqlite() {
    if (is_open()) {
      close(false);
    }
  }

  // member functions
  void open(const char *db_name, const bool load_in_memory = false) {
    CHECK_T(std::invalid_argument, !core::utils::is_str_null(db_name),
            "invalid db_name parameter (empty)");
    CHECK(_db == nullptr, "db already open");
    //
    try {
      if (__sh_cnt.fetch_add(1) == 0) {
        // first connection
        check_status(sqlite3_config(SQLITE_CONFIG_SERIALIZED));
        check_status(sqlite3_initialize());
      }
      _db_name = db_name;
      check_status(sqlite3_open(_db_name.c_str(), &_db));
      check_status(sqlite3_exec(_db, "PRAGMA foreign_keys = ON",
                                //
                                nullptr, nullptr, nullptr));

      // load into memory if so requested
      if (load_in_memory) {
        sqlite3 *db{nullptr};
        check_status(sqlite3_open(":memory:", &db));
        sqlite3_backup *pb{sqlite3_backup_init(db, "main", _db, "main")};
        CHECK(pb != nullptr, sqlite3_errmsg(db));
        check_status(sqlite3_backup_step(pb, -1), SQLITE_DONE);
        check_status(sqlite3_backup_finish(pb));
        check_status(sqlite3_close(_db));
        _db = db;
      }
    } catch (...) {
      _db_name = core::globals::empty_string;
      if (_db != nullptr) {
        close(false);
        _db = nullptr;
      }
      if (__sh_cnt.fetch_add(-1) == 1) {
        // only connection
        sqlite3_shutdown();
      }
      throw;
    }
  }

  void close(const bool check) {
    if (_db == nullptr) {
      return;
    }
    if (check) {
      check_status(sqlite3_close(_db));
    } else {
      sqlite3_close(_db);
    }
    if (__sh_cnt.fetch_add(-1) == 1) {
      // only connection
      check_status(sqlite3_shutdown());
    }
  }

  void exec(const char *sql, int (*callback)(void *, int, char **, char **),
            void *p = nullptr) {
    CHECK(is_open(), "db is not open");
    CHECK_T(std::invalid_argument, !core::utils::is_str_null(sql),
            "invalid sql parameter (empty)");
    char *err_msg{nullptr};
    switch (sqlite3_exec(_db, sql, callback, p, &err_msg)) {
      case SQLITE_OK:
      case SQLITE_ABORT:
        break;
      default:
        CHECK(false, err_msg);
    }
  }

  void exec(const std::string &sql,
            int (*callback)(void *, int, char **, char **), void *p = nullptr) {
    exec(sql.c_str(), callback, p);
  }

  bool is_open() const { return _db != nullptr; }

  const std::string &db_name() const { return _db_name; }

  static bool get_bool(const char *str) {
    CHECK_T(std::invalid_argument, !core::utils::is_str_null(str),
            "invalid str parameter (null)");
    CHECK(*str == '0' || *str == '1',
          "invalid str value encountered (not 0|1)");
    return *str == '1';
  }

 private:
  sqlite3 *_db{nullptr};
  std::string _db_name{};

  static std::atomic<std::int32_t> __sh_cnt;

  void check_status(const int c, const int s = SQLITE_OK,
                    const char *err_msg = nullptr) {
    CHECK(c == s, err_msg == nullptr ? sqlite3_errmsg(_db) : err_msg);
  }
};

#endif