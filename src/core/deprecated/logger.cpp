///////////////////////////////////////////////////////////////////////////////
// NOTE:
// This file is only here so that it's source (and logic) is not lost.
// We are currently using the Poco::Logger framework, so this is not being used
///////////////////////////////////////////////////////////////////////////////

// C
#include <sys/time.h>

// C++
#include <chrono>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <string>

// project/local
#include <globals.h>
#include <logger.h>
#include <utils.h>

namespace core::utils {

///////////////////////////////////////////////////////////////////////////////
// class trace

auto trace::stack::rbegin() { return base::c.rbegin(); }
auto trace::stack::rend() { return base::c.rend(); }
auto trace::stack::rbegin() const { return base::c.rbegin(); }
auto trace::stack::rend() const { return base::c.rend(); }

void trace::push(std::string &&str) { __stack.push(std::move(str)); }

void trace::pop() { __stack.pop(); }

std::string trace::dump_stack_trace() {
  return core::globals::sstr_pool.exec<std::string>(
      [](std::stringstream &sstr) {
        for (auto i = __stack.rbegin(); i != __stack.rend(); ++i) {
          sstr << '\t' << *i << '\n';
        }
        sstr.flush();
        return sstr.str();
      });
}

///////////////////////////////////////////////////////////////////////////////
// class tracer

tracer::tracer(const char *file, const char *func, std::size_t line)
    : _file(file), _func(func), _line(line) {
  trace::push(make_source_file_info_string());
}

tracer::~tracer() { trace::pop(); }

const std::string &tracer::file() const { return _file; }

const std::string &tracer::func() const { return _func; }

std::size_t tracer::line() const { return _line; }

std::string tracer::make_source_file_info_string() const {
  return core::globals::sstr_pool.exec<std::string>(
      [this](std::stringstream &sstr) {
        sstr << '[' << _file << ':' << _line << " (" << _func << ")]";
        return sstr.str();
      });
}

///////////////////////////////////////////////////////////////////////////////
// class logger::message

logger::message::message(const bucket bckt, const std::thread::id &thread_id,
                         const char *file, const char *func,
                         const std::size_t line, std::string &&msg)
    : _bucket{bckt},
      _tv{get_tod()},
      _thread_id{thread_id},
      _file{file},
      _func{func},
      _line{line},
      _msg{std::move(msg)},
      _trace{_bucket == core::utils::logger::bucket::SEVERE
                 ? core::utils::trace::dump_stack_trace()
                 : core::globals::empty_string} {}

logger::message::message(message &&o)
    : _bucket{o._bucket},
      _tv{o._tv},
      _thread_id{std::move(o._thread_id)},
      _file{std::move(o._file)},
      _func{std::move(o._func)},
      _line{o._line},
      _msg{std::move(o._msg)},
      _trace{std::move(o._trace)} {
  o.reset();
}

logger::message &logger::message::operator=(message &&o) {
  if (this != &o) {
    ((bucket &)_bucket) = o._bucket;
    ((std::thread::id &)_thread_id) = o._thread_id;
    ((struct timeval &)_tv) = o._tv;
    ((std::string &)_file) = std::move(o._file);
    ((std::string &)_func) = std::move(o._func);
    ((std::string &)_line) = o._line;
    ((std::string &)_msg) = std::move(o._msg);
    ((std::string &)_trace) = std::move(o._trace);
    o.reset();
  }
  return *this;
}

void logger::message::reset() {
  ((bucket &)_bucket) = bucket::NONE;
  std::memset(&((struct timeval &)_tv), 0, sizeof(_tv));
  ((std::string &)_file).clear();
  ((std::string &)_func).clear();
  ((std::size_t &)_line) = -1;
  ((std::string &)_msg).clear();
  ((std::string &)_trace).clear();
}

///////////////////////////////////////////////////////////////////////////////
// class logger
void logger::runner() {
  std::uint16_t cnt{0U};
  while (is_flag_set(flag::RUN)) {
    try {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      // 100ms interval/slices to increase shutdown responsiveness
      if (is_flag_set(flag::RUN) && (cnt += 100) <= 1000) {
        continue;
      }
      cnt = 0U;
      if (__msgs.load()->empty()) {
        // don't do anything if the list of messages is empty
        continue;
      }
      // switch/exchange message list with new one
      std::unique_ptr<std::list<message>> msgs(
          __msgs.exchange(new (std::nothrow) std::list<message>));
      if (__msgs == nullptr) {
        // if we can't allocate a new message list here, then we should not
        // accept/queue log entries any further. But we still need to flush this
        // message batch (hence the std::nothrow new invocation)
        reset_flag(flag::RUN);
        reset_flag(flag::QUEUE_ACCEPT);
      }
      while (!msgs->empty()) {
        const message &msg = msgs->front();
        // ignore msg's which do not belong in any bucket
        if (msg._bucket != bucket::NONE) {
          log_message(msg, get_ostr());
        }
        // remove the message just processed
        msgs->pop_front();
      }
      // flush to file after batch
      get_ostr().flush();
      //
    } catch (const std::exception &e) {
      // if any exception is thrown, then we should not accept/queue
      // any more message and we should dump everything to std::cerr
      //(and also terminate this thread as where will be no messages
      // to log to file)
      reset_flag(flag::RUN);
      reset_flag(flag::QUEUE_ACCEPT);
      std::cerr << "Exception encountered in logger thread: " << e.what()
                << " @ : " << SOURCE_FILE_INFO() << std::endl;
    } catch (...) {
      reset_flag(flag::RUN);
      reset_flag(flag::QUEUE_ACCEPT);
      std::cerr << "Unknown exception encountered in logger thread @: "
                << SOURCE_FILE_INFO() << std::endl;
    }
  }
  // we're exiting this thread, flush the file stream and close it
  //(if applicable)
  if (__ofs.is_open() && __ofs.good()) {
    try {
      __ofs.flush();
      __ofs.close();
      __ofs.clear();
    } catch (const std::exception &ex) {
      std::cerr << "std::exception encountered:" << ex.what()
                << "@ : " << SOURCE_FILE_INFO() << std::endl;
    } catch (...) {
      std::cerr << "unknown exception encountered @: " << SOURCE_FILE_INFO()
                << std::endl;
    }
  }
}

timeval logger::get_tod() {
  struct timeval tv {};
  CHECK(gettimeofday(&tv, nullptr) == 0, get_os_error_string());
  return tv;
}

struct tm logger::to_tm(const std::time_t t) {
  struct tm tt {};
  const auto r{localtime_r(&t, &tt)};
  CHECK(r != nullptr, core::utils::get_os_error_string());
  return tt;
}

std::ostream &logger::get_ostr() noexcept {
  if (is_flag_set(flag::FILES_ERROR)) {
    // if logging to files encountered an error previously,
    // we return std::cerr immediately to log messages to it
    return std::cerr;
  }
  //
  try {
    if (__ofs.is_open()) {
      if (__ofs.tellp() >= __log_file_size) {
        // if the file has exceeded the specified size,
        // close it; another will be opened below
        __ofs.flush();
        __ofs.close();
        __ofs.clear();
      }
    }
    if (!__ofs.is_open()) {
      std::tm t;
      std::memset(&t, 0, sizeof(t));
      char buf[36];
      std::memset(buf, 0, sizeof(buf));
      //
      struct timeval tv {
        get_tod()
      };
      // sleep a very short while to ensure that we get to create
      // a new file and not open an existing one
      std::this_thread::sleep_for(std::chrono::milliseconds(33));
      CHECK(localtime_r(&tv.tv_sec, &t) != nullptr,
            core::utils::get_os_error_string());
      CHECK(std::strftime(buf, sizeof(buf) - 1, "%d%m%Y_%H%M%S", &t) != 0,
            "std::strftime returned 0");
      std::string fname{core::utils::build_string(
          __log_file_dir, '/', __log_file_prefix, buf, '-', std::setfill('0'),
          std::setw(3), (tv.tv_usec / 1000), __log_file_suffix)};
      __ofs.open(fname, std::ios::app);
      CHECK(__ofs.good() && __ofs.is_open(),
            "error opening creating log file: ", fname);
    }
    return __ofs;
    // if any exception occurs logging messages to files, then
    // we switch to logging messages to std::cerr
  } catch (const std::exception &ex) {
    std::cerr << "Exception encountered in logger thread: " << ex.what()
              << " @ : " << SOURCE_FILE_INFO() << std::endl;
  } catch (...) {
    std::cerr << "Unknown exception encountered in logger thread @: "
              << SOURCE_FILE_INFO() << std::endl;
  }
  set_flag(flag::FILES_ERROR);
  return std::cerr;
}

void logger::init(const std::initializer_list<bucket> &buckets,
                  std::size_t log_file_size, const char *log_file_dir,
                  const char *log_file_prefix, const char *log_file_suffix) {
  CHECK_T(std::invalid_argument, log_file_size >= 1024UL,
          "invalid log_file_size parameter (minimum file size is 1024 bytes)");
  CHECK_T(std::invalid_argument, !is_str_null(log_file_dir),
          "invalid log_file_dir parameter");
  CHECK_T(std::invalid_argument, !is_str_null(log_file_prefix),
          "invalid log_file_prefix parameter");
  CHECK_T(std::invalid_argument, !is_str_null(log_file_suffix),
          "invalid log_file_suffix parameter");
  //
  CHECK(!__thrd.joinable(), "logger already initialized");
  __buckets = 0UL;
  for (const auto &b : buckets) {
    __buckets |= static_cast<std::uint64_t>(b);
  }
  __log_file_size = log_file_size;
  __log_file_dir = log_file_dir;
  __log_file_prefix = log_file_prefix;
  __log_file_suffix = log_file_suffix;
  __msgs = new std::list<message>;
  set_flag(flag::RUN);
  set_flag(flag::QUEUE_ACCEPT);
  __thrd = std::thread(runner);
}

void logger::fini() {
  CHECK(__thrd.joinable(), "logger not initialized");
  reset_flag(flag::QUEUE_ACCEPT);
  reset_flag(flag::RUN);
  __thrd.join();
  // delete the message list
  std::unique_ptr<std::list<message>>(__msgs.exchange(nullptr));
  __buckets = 0UL;
  __flags = 0UL;  // reset all flags
  __log_file_size = 4096L;
  __log_file_dir = __log_file_prefix = __log_file_suffix =
      core::globals::empty_string;
}

const char *logger::get_bucket_str(const bucket bckt) {
  CHECK(bckt <= bucket::SEVERE, "invalid logger::bucket value specified");
  std::uint64_t b{static_cast<std::uint64_t>(bckt)};
  std::uint64_t i{0UL};
  while (true) {
    if ((b >>= 1) == 0UL) {
      return __bucket_strings[i + 1];
    }
    ++i;
  }
}

void logger::log_message(const message &msg, std::ostream &ostr) {
  log_message(msg._bucket, msg._thread_id, msg._tv, msg._msg.c_str(),
              msg._file.c_str(), msg._func.c_str(), msg._line,
              msg._trace.c_str(), ostr);
}

void logger::log_message(const bucket bckt, const std::thread::id &thread_id,
                         const struct timeval &tv, const char *msg,
                         const char *file, const char *func, std::size_t line,
                         const char *trace, std::ostream &ostr) {
  static char buf[36];  //(for std::strftime below)
  static std::thread::id init_thread_id{std::this_thread::get_id()};
  //
  CHECK(std::this_thread::get_id() == init_thread_id,
        "illegal logger::log_message invocation from secondary thread");
  const auto tt{to_tm(tv.tv_sec)};
  std::memset(buf, 0, sizeof(buf));
  std::strftime(buf, sizeof(buf) - 1, "%d-%m-%Y %H:%M:%S", &tt);
  const char fill{ostr.fill()};  // store current fill char
  ostr << '[' << std::setw(7) << std::left << get_bucket_str(bckt)
       << "][thread_id: " << thread_id << "][" << buf << '.' << std::setw(3)
       << std::setfill('0') << (tv.tv_usec / 1000) << ' ' << tt.tm_zone
       << "]: " << msg << " (" << file << ':' << line << ". [" << func
       << "])\n";
  if (bckt == bucket::SEVERE && !is_str_null(trace)) {
    ostr << trace;
  }
  ostr.fill(fill);  // restore previous fill char
}

std::string logger::truncate_message(const char *msg) {
  const std::size_t len{std::strlen(msg)};
  std::string res{msg, std::min(len, 1024UL)};
  if (len > res.length()) {
    std::size_t pos{res.length() - 1};
    for (int i = 0; i < 3; ++i) {
      res[pos - i] = '.';
    }
  }
  return res;
}

bool logger::is_flag_set(const flag f) {
  return (__flags.load() & static_cast<std::uint8_t>(f)) ==
         static_cast<std::uint8_t>(f);
}

std::uint8_t logger::set_flag(const flag f) {
  return __flags.exchange(__flags.load() | static_cast<std::uint8_t>(f));
}

std::uint8_t logger::reset_flag(const flag f) {
  return __flags.exchange(__flags.load() & ~static_cast<std::uint8_t>(f));
}

std::string logger::get_stack_trace(const bucket bckt) {
  return bckt == bucket::SEVERE ? core::utils::trace::dump_stack_trace()
                                : core::globals::empty_string;
}

void logger::log(const bucket bckt, const char *file, const char *func,
                 const int line, const char *msg) noexcept {
  const auto thread_id{std::this_thread::get_id()};
  try {
    if ((bckt != bucket::SEVERE && (is_str_null(msg) || !is_enabled(bckt)))) {
      // ignore empty messages or disabled (not logged) buckets
      //(except for cases when the bucket is SEVERE, in which case we log the
      // entry anyway)
      return;
    }

    // truncate any messages > 1024 bytes
    std::string msg_string{truncate_message(msg)};
    if (is_flag_set(flag::QUEUE_ACCEPT) && __msgs.load() != nullptr) {
      // try to add this message to the logging queue
      try {
        std::lock_guard<std::recursive_mutex> lg{__mtx};
        __msgs.load()->push_back(
            message(bckt, thread_id, file, func, line, std::move(msg_string)));
        // any exceptions encountered, start 'logging' all messages to std::cerr
      } catch (const std::bad_alloc &ex) {
        reset_flag(flag::RUN);
        reset_flag(flag::QUEUE_ACCEPT);
        // log this message to std::cerr
        log_message(bckt, thread_id, get_tod(), msg_string.c_str(), file, func,
                    line, get_stack_trace(bckt).c_str(), std::cerr);
        std::cerr << "td::bad_alloc exception encountered (logger::log): "
                  << ex.what() << std::endl;
      } catch (...) {
        reset_flag(flag::RUN);
        reset_flag(flag::QUEUE_ACCEPT);
        // log this message to std::cerr
        log_message(bckt, thread_id, get_tod(), msg_string.c_str(), file, func,
                    line, get_stack_trace(bckt).c_str(), std::cerr);
        std::cerr << "unknown exception encountered: (logger::log)"
                  << std::endl;
      }
    } else {
      // else dump it to std::cerr
      log_message(bckt, thread_id, get_tod(), msg_string.c_str(), file, func,
                  line, get_stack_trace(bckt).c_str(), std::cerr);
    }
  } catch (...) {
    // if any exception occurs, dump the message to std::cerr
    try {
      log_message(bckt, thread_id, get_tod(), msg, file, func, line,
                  get_stack_trace(bckt).c_str(), std::cerr);
    } catch (...) {
      // if any exception is thrown in the call to log_message, then just
      // dump the message straight to std::cerr
      std::cerr << msg << std::endl;
      std::string trace{get_stack_trace(bckt)};
      if (!trace.empty()) {
        std::cerr << trace << std::endl;
      }
      std::cerr.flush();
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// class logger_ctrl
logger_ctrl::logger_ctrl(const std::initializer_list<logger::bucket> &buckets,
                         std::size_t log_file_size, const char *log_file_dir,
                         const char *prefix, const char *suffix) {
  logger::init(buckets, log_file_size, log_file_dir, prefix, suffix);
}

logger_ctrl::~logger_ctrl() { logger::fini(); }

///////////////////////////////////////////////////////////////////////////////
// statics
thread_local core::utils::trace::stack core::utils::trace::__stack{};

// logger
std::recursive_mutex logger::__mtx{};
std::atomic<std::uint8_t> logger::__flags{
    static_cast<std::uint8_t>(logger::flag::RUN) |
    static_cast<std::uint8_t>(logger::flag::QUEUE_ACCEPT)};
std::ofstream logger::__ofs{};
std::atomic<std::list<logger::message> *> logger::__msgs{nullptr};
std::thread logger::__thrd{};
std::uint64_t logger::__buckets{0UL};
std::int64_t logger::__log_file_size{4096L};
std::string logger::__log_file_dir{};
std::string logger::__log_file_prefix{};
std::string logger::__log_file_suffix{};
const char *logger::__bucket_strings[] = {"NONE", "INFO", "DEBUG",
                                          "SQL",  "WARN", "SEVERE"};

}  // namespace core::utils