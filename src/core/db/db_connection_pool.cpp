// C++
#include <future>
#include <list>

// local/project
// #include <core/core.h>
#include <core/db/db_connection_pool.h>
#include <core/db/mysql_connection.h>
#include <core/db/pg_connection.h>
#include <core/exception/exception.h>

namespace core::db {

/////////////////////////////////////////////////////////////////////////////
// class db_connection_pool::conn

db_connection_pool::conn::conn(std::atomic<connection*>* slot, connection* conn)
    : _slot{slot}, _conn{conn}, _released{false} {
  TRACE_INIT();
  //
  check();
}

db_connection_pool::conn::conn(conn&& o)
    : _slot{o._slot}, _conn{o._conn}, _released{o._released.load()} {
  TRACE_INIT();
  //
  check();
  o.reset();
}

db_connection_pool::conn& db_connection_pool::conn::operator=(conn&& o) {
  TRACE_INIT();
  //
  if (this != &o) {
    o.check();
    _slot = o._slot;
    _conn = o._conn;
    _released = o._released.load();
    o.reset();
  }
  return *this;
}

connection* db_connection_pool::conn::conn_obj() const {
  TRACE_INIT();
  //
  return _conn;
}

void db_connection_pool::conn::release() {
  TRACE_INIT();
  //
  if (!_released) {
    connection* held_conn = __held_conn;
    CHECK(_slot->compare_exchange_strong(held_conn, _conn),
          "failed to reinstate held connection object as available");
    reset();
  }
}

void db_connection_pool::conn::reset() {
  TRACE_INIT();
  //
  _slot = nullptr;
  _conn = nullptr;
  _released = true;
}

void db_connection_pool::conn::check() {
  TRACE_INIT();
  //
  CHECK(_conn != nullptr, "_conn is null");
  CHECK(_slot != nullptr, "_slot is null");
  CHECK(_released == false, "_released is false");
}

///////////////////////////////////////////////////////////////////////////////
// class db_connection_pool

db_connection_pool::db_connection_pool(const rapidjson::Value& conf)
    : _conf{conf},
      _dbtype([&] {
        TRACE_INIT();
        //
        const std::string v{
            core::utils::get_json_value<std::string>(conf, "db_type")};
        if (v == "MySQL") {
          return connection::db_type::MySQL;
        }
        if (v == "PgSQL") {
          return connection::db_type::PgSQL;
        }
        CHECK(false, "configuration: invalid db_type value");
        throw 0;  // only here to mute the compiler warning
      }()),
      _name([&]() {
        TRACE_INIT();
        //
        const std::string v{
            core::utils::get_json_value<std::string>(conf, "name")};
        CHECK(!v.empty(), "configuration: invalid name value (empty)");
        return v;
      }()),
      _host([&]() {
        TRACE_INIT();
        //
        const std::string v{
            core::utils::get_json_value<std::string>(conf, "host")};
        CHECK(!v.empty(), "configuration: invalid host value (empty)");
        return v;
      }()),
      _port([&]() {
        TRACE_INIT();
        //
        return core::utils::get_json_value<std::uint16_t>(conf, "port");
      }()),
      _user([&]() {
        TRACE_INIT();
        //
        const auto v{core::utils::get_json_value<std::string>(conf, "user")};
        CHECK(!v.empty(), "configuration: invalid user value (empty)");
        return v;
      }()),
      _password([&]() {
        TRACE_INIT();
        //
        const auto v{
            core::utils::get_json_value<std::string>(conf, "password")};
        CHECK(!v.empty(), "configuration: invalid password value (empty)");
        return v;
      }()),
      _dbname([&]() {
        TRACE_INIT();
        //
        const auto v{core::utils::get_json_value<std::string>(conf, "dbname")};
        CHECK(!v.empty(), "configuration: invalid dbname value (empty)");
        return v;
      }()),
      _trans_isolation_level(connection::get_transaction_isolation_level(conf)),
      _min([&]() {
        TRACE_INIT();
        //
        return core::utils::get_json_value<std::uint16_t>(conf,
                                                          "connection_min");
      }()),
      _max([&]() {
        TRACE_INIT();
        //
        const auto v{
            core::utils::get_json_value<std::uint16_t>(conf, "connection_max")};
        CHECK(v >= _min, "invalid connection_max value (< _connection_min)");
        return v;
      }()),
      _exconn_max_age([&]() {
        TRACE_INIT();
        //
        const auto v{core::utils::get_json_value<std::uint32_t>(
            conf, "extra_connection_max_age")};
        CHECK(v >= 10000, "invalid extra_connection_max_age value (<10000)");
        return v;
      }()),
      _house_keeper_interval([&]() {
        TRACE_INIT();
        //
        const auto v{core::utils::get_json_value<std::uint16_t>(
            conf, "house_keeper_interval")};
        CHECK(v >= 10000, "invalid house_keeper_interval value (<10000)");
        return v;
      }()),
      _objs{_max},
      _exconns_init_times{_max},
      _run{false} {
  TRACE_INIT();
  //
  // ensure that _objs is all initialized to nullptr's
  for (std::size_t i = 0; i < _max; ++i) {
    _objs[i] = nullptr;
  }
}

void db_connection_pool::init() {
  TRACE_INIT();
  //
  CHECK_T(std::logic_error, !_run,
          core::utils::build_string("db_connection_pool [", _name,
                                    "] already initialized"));
  try {
    // only initialize min connections at the start
    for (std::uint16_t i = 0; i < _min; ++i) {
      std::unique_ptr<connection> conn{create_connection(i)};
      _objs[i] = conn.release();
    }
    // set the 'extra' connection slots to nullptr
    for (std::uint16_t i = _min; i < _max; ++i) {
      _objs[i] = nullptr;
      _exconns_init_times[i] = std::chrono::steady_clock::now();
    }
  } catch (const std::exception& ex) {
    disconnect_connections();
    throw;
  } catch (...) {
    disconnect_connections();
    throw;
  }
  // start the house-keeper thread
  _run = true;
  _thread = std::thread(&db_connection_pool::house_keeper, this);
}

void db_connection_pool::fini() {
  TRACE_INIT();
  //
  CHECK_T(std::logic_error, _run,
          core::utils::build_string("db_connection_pool [", _name,
                                    "] not initialized"));
  // stop the house keeper thread
  _run = false;
  _thread.join();
  // close/disconnect the connections
  disconnect_connections();
}

const rapidjson::Value& db_connection_pool::conf() const {
  TRACE_INIT();
  //
  return _conf;
}

connection::db_type db_connection_pool::db_type() const {
  TRACE_INIT();
  //
  return _dbtype;
}

const std::string& db_connection_pool::name() const {
  TRACE_INIT();
  //
  return _name;
}

const std::string& db_connection_pool::host() const {
  TRACE_INIT();
  //
  return _host;
}

std::uint16_t db_connection_pool::port() const {
  TRACE_INIT();
  //
  return _port;
}

const std::string& db_connection_pool::user() const {
  TRACE_INIT();
  //
  return _user;
}

const std::string& db_connection_pool::password() const {
  TRACE_INIT();
  //
  return _password;
}

const std::string& db_connection_pool::dbname() const {
  TRACE_INIT();
  //
  return _dbname;
}

connection::transaction_isolation_level
db_connection_pool::transaction_isolation_level() const {
  TRACE_INIT();
  //
  return _trans_isolation_level;
}

std::uint16_t db_connection_pool::min_conns() const {
  TRACE_INIT();
  //
  return _min;
}

std::uint16_t db_connection_pool::max_conns() const {
  TRACE_INIT();
  //
  return _max;
}

std::uint32_t db_connection_pool::extra_connection_max_age() const {
  TRACE_INIT();
  //
  return _exconn_max_age;
}

db_connection_pool::conn db_connection_pool::acquire(
    const std::int64_t timeout) {
  TRACE_INIT();
  //
  if (timeout != -1LL) {
    CHECK_T(std::invalid_argument, timeout > 0LL, "invalid timeout argument");
  }
  const auto start = std::chrono::steady_clock::now();
  std::atomic<connection*>* slot{nullptr};
  connection* obj{nullptr};
  connection* null_conn{nullptr};
  while (obj == nullptr) {
    // reset variables in every iteration
    slot = nullptr;
    obj = nullptr;
    null_conn = nullptr;
    //
    for (std::uint16_t i = 0; i < _max; ++i) {
      obj = _objs[i];
      if (obj == __dead_conn || obj == __held_conn) {
        // ignore dead or held connections
        obj = nullptr;
        continue;
      }
      if (obj == nullptr) {
        // not allocated yet (this would be an 'extra'connection)
        // try to aqcuire it
        if (_objs[i].compare_exchange_strong(null_conn, __held_conn)) {
          // if acquired
          try {
            obj = create_connection(i);
            slot = &_objs[i];
          } catch (...) {
            // if an exception is throw, reset the connection slot to null_conn
            // leaving it as a held_conn would be incorrect
            _objs[i].exchange(null_conn);
            throw;
          }
        } else {
          // always reset the expected value used on failure
          null_conn = nullptr;
        }
      } else {
        // this would be an existing (and available) connection
        // try to acquire it
        if (_objs[i].compare_exchange_strong(obj, __held_conn)) {
          slot = &_objs[i];
        } else {
          // always reset the expected value used on failure
          obj = nullptr;
        }
      }
      // if a connection has been acquired, check it's validity
      if (obj != nullptr) {
        if (!check_connection(obj)) {
          // if the connections is dead/invalid,
          // mark it as dead/invalid and delete the connection object
          *slot = __dead_conn;
          obj = nullptr;
          slot = nullptr;
          delete obj;
        } else {
          // now that the connection is being successfully acquired,
          // update the connect objects associated init-time
          _exconns_init_times[i] = std::chrono::steady_clock::now();
          break;
        }
      }
    }
    if (obj == nullptr) {
      // check for timeout; if it's elapsed, throw an exception
      if (timeout != -1LL) {
        const std::int64_t elapsed_ms =
            std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now() - start)
                .count();
        CHECK_T(core::exception::time_out, elapsed_ms < timeout,
                static_cast<const std::uint32_t>(elapsed_ms),
                "timeout encountered whilst acquiring "
                "database connection object instance");
      }
      // avoid CPU spin
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
  }
  return conn(slot, obj);
}

connection* db_connection_pool::create_connection(const std::uint16_t index) {
  TRACE_INIT();
  //
  CHECK_T(
      std::range_error, index < _max,
      core::utils::build_string("index out of range (index >= max): ", index)
          .c_str());

  std::unique_ptr<connection> res{nullptr};
  switch (_dbtype) {
    case connection::db_type::MySQL:
      res.reset(new mysql_connection{_conf});
      break;
    case connection::db_type::PgSQL:
      res.reset(new pg_connection{_conf});
      break;
    default:
      CHECK(false,
            "unhandled connection::db_type value encountered in creating "
            "connection object");
      throw 0;  // here just to mute the compiler warning
  }
  res->connect();
  res->ping();
  _exconns_init_times[index] = std::chrono::steady_clock::now();
  return res.release();
}

void db_connection_pool::recreate_dead_connections() noexcept {
  TRACE_INIT();
  //
  std::list<std::future<void>> futures;
  for (std::uint16_t i = 0; _run && i < _max; ++i) {
    connection* obj = _objs[i];
    if (obj != __dead_conn) {
      // only interested in dead connections here
      continue;
    }
    if (i < _min) {  // only replace core (min) connections
      futures.push_back(std::async(std::launch::async, [this, i]() {
        // get a new connection for this slot
        try {
          _objs[i] = create_connection(i);
        } catch (...) {
          // if any exception is thrown,
          // set this slot as a dead/invalid connection
          //(and also absorb the exception (it will be logged))
          _objs[i] = __dead_conn;
        }
      }));
    } else {
      // do not replace any dead 'extra' connections
      _objs[i] = nullptr;
    }
  }
  // wait for all async calls
  for (auto& f : futures) {
    f.get();
  }
}

void db_connection_pool::house_keeper() {
  static std::uint32_t interval{0U};
  //
  TRACE_INIT();
  //
  while (_run) {
    try {
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      if (!_run) {
        // if we're stopping, exit this loop, terminating this thread
        break;
      }
      if ((interval += 500) < _house_keeper_interval) {
        // if the interval hasn't been reached since last run,
        // just regenerate any dead connections and then
        // pause again for 1 second
        recreate_dead_connections();
        continue;
      }
      // since we're going to do a run, reset the interval value
      interval = 0U;

      // check all connections (those that are free/available);
      // if any are not healthy, mark them as dead connections
      for (std::size_t i = 0; _run && i < _max; ++i) {
        connection* obj = _objs[i];
        if (!is_real_conn(obj)) {
          // ignore held, dead and null connections
          continue;
        }
        // if this is a valid/active connection, check it fully to keep it
        // alive. if the check fails, mark it as dead (and release it)
        if (_run) {
          // mark it as held
          if (_objs[i].compare_exchange_strong(obj, __held_conn)) {
            if (check_connection(obj)) {
              // if it's healthy, put it back as available
              _objs[i].exchange(obj);
            } else {
              // if it's not healthy, delete the object and mark the slot as a
              // dead conn; it will be replaced in the recreate_dead_connections
              // call below
              _objs[i] = __dead_conn;
              delete obj;
            }
          }
        }
      }
      // replace dead connections with new ones (asynchronously)
      //(as long as they are within the minimum number of connections
      //(i.e. 'core'))
      if (_run) {
        recreate_dead_connections();
      }

      // remove any 'extra' connections that have aged
      for (std::uint16_t i = _min; _run && i < _max; ++i) {
        connection* obj = _objs[i];
        if (!is_real_conn(obj)) {
          continue;
        }
        // if this 'extra' connection is idle and
        // its age >= _ex_conn_max_age, then remove it
        const std::int64_t elapsed_ms =
            std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::steady_clock::now() - _exconns_init_times[i])
                .count();
        if (elapsed_ms >= _exconn_max_age &&
            _objs[i].compare_exchange_strong(obj, nullptr)) {
          // if 'acquired', release the connection
          obj->disconnect();
          delete obj;
        }
      }
    } catch (const std::exception& ex) {
      LOG_SEVERE(
          core::utils::build_string("exception encountered in "
                                    "db_connection_pool house_keeper_thread: ",
                                    ex.what())
              .c_str());
    } catch (...) {
      LOG_SEVERE(
          "unknown exception encountered in db_connection_pool "
          "house_keeper_thread");
    }
  }
}

bool db_connection_pool::is_real_conn(connection* conn) const {
  TRACE_INIT();
  //
  return conn != nullptr && conn != __dead_conn && conn != __held_conn;
}

bool db_connection_pool::check_connection(connection* conn) noexcept {
  TRACE_INIT();
  //
  try {
    return conn->ping();
  } catch (const std::exception& ex) {
    LOG_WARN(fmt::format("Exception encountered pinging connection {:s}: {:s}.",
                         conn->get_info_str(_name), ex.what()));
  } catch (...) {
    LOG_WARN(
        fmt::format("Unknown exception encountered pinging connection {:s}.",
                    conn->get_info_str(_name)));
  }
  return false;
}

void db_connection_pool::disconnect_connections() noexcept {
  TRACE_INIT();
  //
  for (std::size_t i = 0; i < _max; ++i) {
    if (_objs[i] != nullptr) {
      const auto conn{_objs[i].load()};
      try {
        conn->disconnect();
      } catch (const std::exception& ex) {
        LOG_WARN(
            fmt::format("Exception encountered pinging connection {:s}: {:s}.",
                        conn->get_info_str(_name), ex.what()));
      } catch (...) {
        LOG_WARN(fmt::format(
            "Unknown exception encountered pinging connection {:s}.",
            conn->get_info_str(_name)));
      }
      delete conn;
      _objs[i] = nullptr;
    }
  }
  _exconns_init_times.clear();
  _exconns_init_times.resize(_max);
}

// statics
core::db::connection* core::db::db_connection_pool::__dead_conn{
    ((core::db::connection*)-0x1L)};
core::db::connection* core::db::db_connection_pool::__held_conn{
    ((core::db::connection*)-0x2L)};

}  // namespace core::db
