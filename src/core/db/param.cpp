// local/project
#include <core/db/param.h>

namespace core::db {

///////////////////////////////////////////////////////////////////////////////
// class null
param::null::null() { TRACE_INIT(); }

param::null::null(const null &) { TRACE_INIT(); }

param::null::null(null &&) { TRACE_INIT(); }

param::null &param::null::operator=(const null &) {
  TRACE_INIT();
  return *this;
}

param::null &param::null::operator=(null &&) {
  TRACE_INIT();
  return *this;
}

///////////////////////////////////////////////////////////////////////////////
// class param

param::param() : _value{null{}} { TRACE_INIT(); }

param::param(const bool v) : _value{v} { TRACE_INIT(); }

param::param(const short v) : _value{v} { TRACE_INIT(); }

param::param(const int v) : _value{v} { TRACE_INIT(); }

param::param(const long v) : _value{v} { TRACE_INIT(); }

// FIXME: Check this long long
param::param(const long long v) : _value{v} { TRACE_INIT(); }

param::param(const float &v) : _value{v} { TRACE_INIT(); }

param::param(const double &v) : _value{v} { TRACE_INIT(); }

param::param(const char v) : _value{v} { TRACE_INIT(); }

param::param(const char *v) : _value{std::string{v}} { TRACE_INIT(); }

param::param(const std::string &v) : _value{v} { TRACE_INIT(); }

param::param(const core::utils::byte_array &v) : _value{v} { TRACE_INIT(); }

param::param(const core::datetime::date &v) : _value{v} { TRACE_INIT(); }

param::param(const std::tm &v) : _value{v} { TRACE_INIT(); }

param::param(const param &o) : _value{o._value} { TRACE_INIT(); }

param::param(param &&o) : _value{std::move(o._value)} { TRACE_INIT(); }

const param::value_type &param::value() const {
  TRACE_INIT();
  return _value;
}

bool param::is_null() const {
  TRACE_INIT();
  // the _value variant holds a 'null' structure
  // (which is held at 'index' 0), then this param is null
  return _value.index() == 0;
}

std::string param::to_string() const {
  TRACE_INIT();
  struct {
    std::string operator()(const null &) const {
      TRACE_INIT();
      return std::string("null");
    }
    std::string operator()(const bool v) const {
      static const std::string t{"true"};
      static const std::string f{"false"};
      //
      TRACE_INIT();
      return v ? t : f;
    }
    std::string operator()(const short v) const {
      TRACE_INIT();
      return std::to_string(v);
    }
    std::string operator()(const int v) const {
      TRACE_INIT();
      return std::to_string(v);
    }
    std::string operator()(const long v) const {
      TRACE_INIT();
      return std::to_string(v);
    }
    std::string operator()(const long long v) const {
      TRACE_INIT();
      return std::to_string(v);
    }
    std::string operator()(const float &v) const {
      TRACE_INIT();
      return std::to_string(v);
    }
    std::string operator()(const double &v) const {
      TRACE_INIT();
      return std::to_string(v);
    }
    std::string operator()(const char v) const {
      TRACE_INIT();
      return std::string(1, v);
    }
    std::string operator()(const char *v) const {
      static const std::string empty{};
      TRACE_INIT();
      return core::utils::is_str_null(v) ? empty : std::string{v};
    }
    std::string operator()(const std::string &v) const {
      TRACE_INIT();
      return v;
    }
    std::string operator()(const core::utils::byte_array &) const {
      TRACE_INIT();
      return std::string("byte_array");
    }
    std::string operator()(const core::datetime::date &v) const {
      TRACE_INIT();
      return v.to_string();
    }
    std::string operator()(const std::tm &v) const {
      TRACE_INIT();
      std::string str(36, 0);
      const std::size_t sz(
          strftime(str.data(), str.size() - 1, "%H:%M:%S %d/%m/%Y", &v));
      str.resize(sz);
      return str;
    }
  } visitor;
  return std::visit(visitor, _value);
}

// statics
const param param::__null_v{};

}  // namespace core::db