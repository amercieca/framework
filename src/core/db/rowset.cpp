// C++
#include <climits>

// project/local
#include <core/utils/utils.h>
#include <core/db/rowset.h>

namespace core::db {

///////////////////////////////////////////////////////////////////////////////
// class rowset

rowset::rowset() : _mtx{nullptr} { TRACE_INIT(); }

rowset::~rowset() {
  TRACE_INIT();
  // in cases where this rowset is tied to a prepared statement,
  // the mutex needs to be unlocked here (otherwise the prepared statement
  // will never be available again)
  if (_mtx != nullptr) {
    _mtx->unlock();
  }
}

// the following are helper functions that forward their std::string
// fname.c_str() parameter to the respective funcion in the derived class. So
// there is no need to check the validity of the fname parameter (if it's empty
// or not) because this will be checked in the derived class (forwarded)
// functions
std::optional<bool> rowset::get_bool(const std::string &fname) const {
  TRACE_INIT();
  return get_bool(fname.c_str());
}

std::optional<int> rowset::get_int(const std::string &fname) const {
  TRACE_INIT();
  return get_int(fname.c_str());
}

std::optional<long> rowset::get_long(const std::string &fname) const {
  TRACE_INIT();
  return get_long(fname.c_str());
}

std::optional<float> rowset::get_float(const std::string &fname) const {
  TRACE_INIT();
  return get_float(fname.c_str());
}

std::optional<double> rowset::get_double(const std::string &fname) const {
  TRACE_INIT();
  return get_double(fname.c_str());
}

std::optional<char> rowset::get_char(const std::string &fname) const {
  TRACE_INIT();
  return get_char(fname.c_str());
}

std::optional<std::string> rowset::get_string(const std::string &fname) const {
  TRACE_INIT();
  return get_string(fname.c_str());
}

std::optional<core::datetime::date>
rowset::get_date(const std::string &fname) const {
  TRACE_INIT();
  return get_date(fname.c_str());
}

std::optional<std::tm> rowset::get_timestamp(const std::string &fname) const {
  TRACE_INIT();
  return get_timestamp(fname.c_str());
}

// in the get_v functions below, we need not check the validity of the fname
// parameter as this is checked before their invocation in the get_t function
void rowset::get_v(const char *fname, std::optional<bool> &v) const {
  TRACE_INIT();
  v = get_bool(fname);
}

void rowset::get_v(const char *fname, std::optional<int> &v) const {
  TRACE_INIT();
  v = get_int(fname);
}

void rowset::get_v(const char *fname, std::optional<long> &v) const {
  TRACE_INIT();
  v = get_long(fname);
}

void rowset::get_v(const char *fname, std::optional<float> &v) const {
  TRACE_INIT();
  v = get_float(fname);
}

void rowset::get_v(const char *fname, std::optional<double> &v) const {
  TRACE_INIT();
  v = get_double(fname);
}

void rowset::get_v(const char *fname, std::optional<char> &v) const {
  TRACE_INIT();
  v = get_char(fname);
}

void rowset::get_v(const char *fname, std::optional<std::string> &v) const {
  TRACE_INIT();
  v = get_string(fname);
}

void rowset::get_v(const char *fname,
                   std::optional<core::utils::byte_array> &v) const {
  TRACE_INIT();
  v = get_bytes(fname);
}

void rowset::get_v(const char *fname,
                   std::optional<core::datetime::date> &v) const {
  TRACE_INIT();
  v = get_date(fname);
}

void rowset::get_v(const char *fname, std::optional<std::tm> &v) const {
  TRACE_INIT();
  v = get_timestamp(fname);
}

std::optional<bool> rowset::str_to_opt_bool(const char *str, const char c) {
  TRACE_INIT();
  return core::utils::is_str_null(str) ? null_bool
                                       : std::optional<bool>(*str == c);
}

std::optional<int> rowset::str_to_opt_int(const char *str) {
  TRACE_INIT();
  if (core::utils::is_str_null(str)) {
    return null_int;
  }
  const auto v{std::strtol(str, nullptr, 10)};
  CHECK(v != LONG_MIN && v != LONG_MAX && errno != ERANGE,
        "out of range value in string conversion to int");
  CHECK(v <= INT_MAX, "out of range value in string conversion to int");
  return static_cast<int>(v);
}

std::optional<long> rowset::str_to_opt_long(const char *str) {
  TRACE_INIT();
  if (core::utils::is_str_null(str)) {
    return null_long;
  }
  const auto v{std::strtol(str, nullptr, 10)};
  CHECK(v != LONG_MIN && v != LONG_MAX && errno != ERANGE,
        "out of range value in string conversion to long");
  return static_cast<long>(v);
}

std::optional<float> rowset::str_to_opt_float(const char *str) {
  TRACE_INIT();
  if (core::utils::is_str_null(str)) {
    return null_float;
  }
  const auto &[err_flag, err_msg, fv] = core::utils::string_to_fp<float>(str);
  CHECK(err_flag, err_msg);
  return std::optional<float>(fv);
}

std::optional<double> rowset::str_to_opt_double(const char *str) {
  TRACE_INIT();
  if (core::utils::is_str_null(str)) {
    return null_double;
  }
  const auto &[err_flag, err_msg, dv] = core::utils::string_to_fp<double>(str);
  CHECK(err_flag, err_msg);
  return std::optional<double>(dv);
}

std::optional<char> rowset::str_to_opt_char(const char *str) {
  TRACE_INIT();
  return core::utils::is_str_null(str) ? null_char : std::optional<char>{*str};
}

std::optional<std::string> rowset::str_to_opt_string(const char *str) {
  // the code here differentiates between a nullptr and an empty ("") string.
  // if str == "", then we return an std::optional with an empty value
  // as opposed to a no-value optional
  TRACE_INIT();
  return str == nullptr ? null_string : std::optional<std::string>{str};
}

std::optional<core::datetime::date> rowset::str_to_opt_date(const char *str) {
  TRACE_INIT();
  if (core::utils::is_str_null(str)) {
    return null_date;
  }
  std::tm t{};
  std::memset(&t, 0, sizeof(t));
  
#ifdef _WIN32
  CHECK(core::utils::strptime(str, "%Y-%m-%d", &t) != nullptr, "strptime failed");
#else
  CHECK(strptime(str, "%Y-%m-%d", &t) != nullptr, "strptime failed");
#endif
  return std::optional<core::datetime::date>{core::datetime::date{t}};
}

std::optional<std::tm> rowset::str_to_opt_timestamp(const char *str) {
  TRACE_INIT();
  if (core::utils::is_str_null(str)) {
    return null_timestamp;
  }
  std::tm t{};
  std::memset(&t, 0, sizeof(t));
#ifdef _WIN32
  CHECK(core::utils::strptime(str, "%Y-%m-%d", &t) != nullptr, "strptime failed");
#else
  CHECK(strptime(str, "%Y-%m-%d", &t) != nullptr, "strptime failed");
#endif
  return std::optional<std::tm>{t};
}

// statics
const std::optional<bool> rowset::null_bool{};
const std::optional<int> rowset::null_int{};
const std::optional<long> rowset::null_long{};
const std::optional<float> rowset::null_float{};
const std::optional<double> rowset::null_double{};
const std::optional<char> rowset::null_char{};
const std::optional<std::string> rowset::null_string{};
const std::optional<core::datetime::date> rowset::null_date{};
const std::optional<std::tm> rowset::null_timestamp{};

} // namespace core::db