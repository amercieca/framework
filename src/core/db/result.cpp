// project/local
#include <core/db/result.h>
#include <core/utils/utils.h>

namespace core::db {

result::result(const std::size_t update_count,
               const std::vector<std::size_t>& generated_keys)
    : _update_count{update_count}, _generated_keys{generated_keys} {
  TRACE_INIT();
}

result::result(const std::size_t update_count,
               std::vector<std::size_t>&& generated_keys)
    : _update_count{update_count}, _generated_keys{generated_keys} {
  TRACE_INIT();
}

result::result(const result& o)
    : _update_count{o._update_count}, _generated_keys{o._generated_keys} {
  TRACE_INIT();
}

result::result(result&& o)
    : _update_count{o._update_count},
      _generated_keys{std::move(o._generated_keys)} {
  TRACE_INIT();
  o.reset();
}

result& result::operator=(const result& o) {
  TRACE_INIT();
  if (this != &o) {
    ((std::size_t&)_update_count) = o._update_count;
    ((std::vector<std::size_t>&)_generated_keys) = o._generated_keys;
  }
  return *this;
}

result& result::operator=(result&& o) {
  TRACE_INIT();
  if (this != &o) {
    ((std::size_t&)_update_count) = o._update_count;
    ((std::vector<std::size_t>&)_generated_keys) = std::move(o._generated_keys);
    o.reset();
  }
  return *this;
}

std::size_t result::update_count() const {
  TRACE_INIT();
  return _update_count;
}

const std::vector<std::size_t>& result::generated_keys() const {
  TRACE_INIT();
  return _generated_keys;
}

void result::reset() {
  TRACE_INIT();
  ((std::size_t&)_update_count) = 0UL;
  ((std::vector<std::size_t>&)_generated_keys).clear();
}

}  // namespace core::db