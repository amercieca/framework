// project/local
#include <core/db/mysql_connection.h>

#define CHECK_INITIALIZED(c) \
  CHECK((c) != nullptr, "connection is not initialized (not connected?)")

#define CHECK_STATUS(c, s) CHECK((s) == 0, mysql_error((c)))

#define CHECK_ERRNO(c) CHECK(mysql_errno((c)) == 0, mysql_error((c)))

#define CHECK_NOT_NULL(c, p) CHECK(p != nullptr, mysql_error(c))

#define CHECK_FIELD_INDEX(i, f)                                       \
  CHECK_T(std::range_error,                                           \
          (static_cast<int>((i)) >= 0) &&                             \
              (static_cast<int>((i)) < static_cast<int>((f).size())), \
          "field-index out of range: ", (i))

namespace core::db {

///////////////////////////////////////////////////////////////////////////////
// class mysql_result
mysql_connection::mysql_result::mysql_result(MYSQL_RES *res) : _res{res} {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, _res != nullptr, "invalid res parameter");
}

mysql_connection::mysql_result::mysql_result(mysql_result &&o) : _res{o._res} {
  TRACE_INIT();
  o._res = nullptr;
}
mysql_connection::mysql_result &mysql_connection::mysql_result::operator=(
    mysql_result &&o) {
  TRACE_INIT();
  if (this != &o) {
    _res = o._res;
    o._res = nullptr;
  }
  return *this;
}

mysql_connection::mysql_result::~mysql_result() {
  TRACE_INIT();
  if (_res != nullptr) {
    mysql_free_result(_res);
    _res = nullptr;
  }
}

MYSQL_RES *mysql_connection::mysql_result::get() const {
  TRACE_INIT();
  return _res;
}

MYSQL_RES *mysql_connection::mysql_result::release() {
  TRACE_INIT();
  MYSQL_RES *res = _res;
  _res = nullptr;
  return res;
}

///////////////////////////////////////////////////////////////////////////////
// class r_set
mysql_connection::r_set::r_set(MYSQL *myconn, MYSQL_RES *res)
    : _myconn{myconn}, _res{res} {
  TRACE_INIT();
  CHECK_NOT_NULL(_myconn, _res);
  try {
    init_field_data();
  } catch (...) {
    if (_res != nullptr) {
      mysql_free_result(_res);
      _res = nullptr;
    }
    throw;
  }
}

mysql_connection::r_set::~r_set() {
  TRACE_INIT();
  if (_res != nullptr) {
    mysql_free_result(_res);
    _res = nullptr;
  }
}

field_type mysql_connection::r_set::get_field_type(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  switch (_fields[fi]->type) {
    case MYSQL_TYPE_TINY:
      return field_type::BOOL;

    case MYSQL_TYPE_INT24:
    case MYSQL_TYPE_LONG:
      return field_type::INT;

    case MYSQL_TYPE_LONGLONG:
      return field_type::LONG;

    case MYSQL_TYPE_FLOAT:
      return field_type::FLOAT;

    case MYSQL_TYPE_DOUBLE:
    case MYSQL_TYPE_NEWDECIMAL:
      return field_type::FLOAT;

    case MYSQL_TYPE_VAR_STRING:
      return field_type::STRING;

    case MYSQL_TYPE_STRING:
      return field_type::CHAR;

    case MYSQL_TYPE_DATE:
      return field_type::DATE;

    case MYSQL_TYPE_DATETIME:
    case MYSQL_TYPE_TIMESTAMP:
      return field_type::TIMESTAMP;

    case MYSQL_TYPE_TINY_BLOB:
    case MYSQL_TYPE_MEDIUM_BLOB:
    case MYSQL_TYPE_BLOB:
    case MYSQL_TYPE_LONG_BLOB:
      return field_type::BYTE_ARRAY;

    default:
      // throw an exception when an unhandled type is encountered
      CHECK(false, "unhandled field_type value encountered for field @ [", fi,
            "] encountered");
      throw 0;  // only here to mute compiler warning
  }
}

field_type mysql_connection::r_set::get_field_type(const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_field_type(get_field_no(fname));
}

void mysql_connection::r_set::init_field_data() {
  TRACE_INIT();
  _fields.clear();
  _fieldmap.clear();
  // populate the fields map for this result set
  std::uint16_t i = 0U;
  const std::size_t num_fields{mysql_num_fields(_res)};
  CHECK(num_fields > 0UL, "mysql_num_fields returned: ", num_fields);
  _fields.reserve(num_fields);
  for (MYSQL_FIELD *field = mysql_fetch_field(_res); field != nullptr;
       field = mysql_fetch_field(_res), ++i) {
    _fields.emplace_back(field);
    CHECK(_fieldmap.try_emplace(field->name, i).second,
          "error adding field to fieldmap: ", field->name);
  }
}

int mysql_connection::r_set::get_field_no(const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  const auto i = _fieldmap.find(fname);
  CHECK(i != _fieldmap.end(), "no such field: ", fname);
  return i->second;
}

///////////////////////////////////////////////////////////////////////////////
// class mysql_connection dr_set
mysql_connection::dr_set::dr_set(MYSQL *conn, MYSQL_RES *res)
    : r_set{conn, res}, _row{nullptr}, _lengths{nullptr} {
  TRACE_INIT();
}

// interface
bool mysql_connection::dr_set::next() const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  _row = mysql_fetch_row(_res);
  if (_row == nullptr) {
    // if null has been returned, check that there is no error
    CHECK_ERRNO(_myconn);
    // otherwise return false (no more rows)
    return false;
  }
  return true;
}

std::uint16_t mysql_connection::dr_set::num_fields() const {
  TRACE_INIT();
  return mysql_num_fields(_res);
}

std::optional<bool> mysql_connection::dr_set::get_bool(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_TINY),
        "invalid value-type (bool) for field: ", fi);
  return str_to_opt_bool(_row[fi], '1');
}

std::optional<bool> mysql_connection::dr_set::get_bool(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_bool(get_field_no(fname));
}

std::optional<int> mysql_connection::dr_set::get_int(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_LONG),
        "invalid value-type (int) for field: ", fi);
  return str_to_opt_int(_row[fi]);
}

std::optional<int> mysql_connection::dr_set::get_int(const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_int(get_field_no(fname));
}

std::optional<long> mysql_connection::dr_set::get_long(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_LONGLONG),
        "invalid value-type (long) for field: ", fi);
  return str_to_opt_long(_row[fi]);
}

std::optional<long> mysql_connection::dr_set::get_long(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_long(get_field_no(fname));
}

std::optional<float> mysql_connection::dr_set::get_float(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(
            *_fields[fi],
            {MYSQL_TYPE_FLOAT, MYSQL_TYPE_DECIMAL, MYSQL_TYPE_NEWDECIMAL}),
        "invalid value-type (float) for field: ", fi);
  return str_to_opt_float(_row[fi]);
}

std::optional<float> mysql_connection::dr_set::get_float(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_float(get_field_no(fname));
}

std::optional<double> mysql_connection::dr_set::get_double(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(
            *_fields[fi],
            {MYSQL_TYPE_DOUBLE, MYSQL_TYPE_DECIMAL, MYSQL_TYPE_NEWDECIMAL}),
        "invalid value-type (double) for field: ", fi);
  return str_to_opt_double(_row[fi]);
}

std::optional<double> mysql_connection::dr_set::get_double(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_double(get_field_no(fname));
}

std::optional<std::string> mysql_connection::dr_set::get_string(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_VAR_STRING),
        "invalid value-type (string) for field: ", fi);
  return str_to_opt_string(_row[fi]);
}

std::optional<std::string> mysql_connection::dr_set::get_string(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_string(get_field_no(fname));
}

std::optional<char> mysql_connection::dr_set::get_char(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_STRING),
        "invalid value-type (char) for field: ", fi);
  return str_to_opt_char(_row[fi]);
}

std::optional<char> mysql_connection::dr_set::get_char(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_char(get_field_no(fname));
}

std::optional<core::datetime::date> mysql_connection::dr_set::get_date(
    int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_DATE),
        "invalid value-type (date) for field: ", fi);
  return str_to_opt_date(_row[fi]);
}

std::optional<core::datetime::date> mysql_connection::dr_set::get_date(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_date(get_field_no(fname));
}

std::optional<std::tm> mysql_connection::dr_set::get_timestamp(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(
            *_fields[fi], {MYSQL_TYPE_DATETIME, MYSQL_TYPE_TIMESTAMP}),
        "invalid value-type (timestamp) for field: ", fi);
  return str_to_opt_timestamp(_row[fi]);
}

std::optional<std::tm> mysql_connection::dr_set::get_timestamp(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_timestamp(get_field_no(fname));
}

core::utils::byte_array mysql_connection::dr_set::get_bytes(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(
            *_fields[fi], {MYSQL_TYPE_TINY_BLOB, MYSQL_TYPE_MEDIUM_BLOB,
                           MYSQL_TYPE_BLOB, MYSQL_TYPE_LONG_BLOB}),
        "invalid value-type (byte_array) for field: ", fi);
  return core::utils::byte_array{(const unsigned char *)_row[fi],
                                 get_field_length(fi)};
}

core::utils::byte_array mysql_connection::dr_set::get_bytes(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_bytes(get_field_no(fname));
}

std::size_t mysql_connection::dr_set::get_field_length(const int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  if (_lengths == nullptr) {
    _lengths = (size_t *)(mysql_fetch_lengths(_res));
    CHECK_NOT_NULL(_myconn, _lengths);
  }
  return _lengths[fi];
}

///////////////////////////////////////////////////////////////////////////////
// class mysql_connection pr_set
mysql_connection::pr_set::pr_set(MYSQL *conn, MYSQL_STMT *stmt)
    : r_set{conn, mysql_stmt_result_metadata(stmt)},
      _stmt{stmt},
      _rbinds{num_fields()},
      _rdata{num_fields()} {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, _stmt != nullptr, "invalid stmt parameter");
  CHECK(_fields.size() == _rbinds.size(),
        "mismatch in _fields size and _rbinds size: ", _fields.size(), " : ",
        _rbinds.size());
  // setup the binds
  std::size_t i{0UL};
  for (const auto field : _fields) {
    setup_bind(*field, i++);
  }
  // activate the binds
  CHECK(mysql_stmt_bind_result(_stmt, std::addressof(*_rbinds.begin())) == 0,
        mysql_stmt_error(_stmt));
}

void mysql_connection::pr_set::setup_bind(MYSQL_FIELD &field,
                                          const std::size_t fi) {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  MYSQL_BIND &bind = _rbinds[fi];
  result_data &data = _rdata[fi];

  std::memset(&bind, 0, sizeof(bind));
  // set defaults
  bind.buffer_type = field.type;
  const auto len{field.max_length};
  //
  switch (field.type) {
    case MYSQL_TYPE_TINY:  // bool
      data.bind_bool(bind);
      break;

    case MYSQL_TYPE_SHORT:  // short
      data.bind_short(bind);
      break;

    case MYSQL_TYPE_INT24:  // int
    case MYSQL_TYPE_LONG:   // this is an int
      data.bind_int(bind);
      break;

    case MYSQL_TYPE_LONGLONG:  // long
      data.bind_long(bind);
      break;

    case MYSQL_TYPE_FLOAT:  // float
      data.bind_float(bind);
      break;

    case MYSQL_TYPE_DOUBLE:  // double
      data.bind_double(bind);
      break;

    case MYSQL_TYPE_NEWDECIMAL:
      data.bind_decimal(bind, len);
      break;

    case MYSQL_TYPE_DATE:  // date
      data.bind_date(bind);
      break;

    case MYSQL_TYPE_TIME:  // time/datetime/timestamp
    case MYSQL_TYPE_DATETIME:
    case MYSQL_TYPE_TIMESTAMP:
      data.bind_timestamp(bind);
      break;

    case MYSQL_TYPE_STRING:
      if (field.length == 1) {  // char
        data.bind_char(bind);
      } else {  // string
        data.bind_string(bind, len);
      }
      break;

    case MYSQL_TYPE_VAR_STRING:  // string
      data.bind_string(bind, len);
      break;

    case MYSQL_TYPE_BLOB:  // blob
    case MYSQL_TYPE_TINY_BLOB:
    case MYSQL_TYPE_MEDIUM_BLOB:
    case MYSQL_TYPE_LONG_BLOB:
      data.bind_blob(bind, len);
      break;

    default:
      CHECK(false, "unhandled MYSQL_TYPE value encountered: ", field.type);
      throw 0;  // here just to mute compiler warning
  }
}

// interface
bool mysql_connection::pr_set::next() const {
  TRACE_INIT();
  bool res{false};
  const int fres{mysql_stmt_fetch(_stmt)};
  switch (fres) {
    case 0:  // data retrieved OK
      res = true;
      break;

    case MYSQL_NO_DATA:  // no more data (this is fine/OK)
      res = false;
      break;

    case 1:  // Error occurred
             // throw exception with MySQL specific error message
      CHECK(false, mysql_error(_myconn));
      break;

    case MYSQL_DATA_TRUNCATED:
      // this should never happen because we're using
      // max_length strategy when fetching data
      CHECK(false, "'mysql_stmt_fetch' data truncated");
      break;

    default:
      CHECK(false, "unhandled value returned from 'mysql_stmt_fetch': ", fres);
      break;
  }
  return res;
}

std::uint16_t mysql_connection::pr_set::num_fields() const {
  TRACE_INIT();
  return mysql_stmt_field_count(_stmt);
}

std::optional<bool> mysql_connection::pr_set::get_bool(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_TINY),
        "invalid value-type (bool) for field: ", fi);
  return _rbinds[fi].is_null_value ? null_bool
                                   : std::optional<bool>{_rdata[fi].v_bool()};
}

std::optional<bool> mysql_connection::pr_set::get_bool(
    const char *fname) const {
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_bool(get_field_no(fname));
}

std::optional<int> mysql_connection::pr_set::get_int(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi],
                                     {MYSQL_TYPE_INT24, MYSQL_TYPE_LONG}),
        "invalid value-type (int) for field: ", fi);
  return _rbinds[fi].is_null_value ? null_int
                                   : std::optional<int>{_rdata[fi].v_int()};
}

std::optional<int> mysql_connection::pr_set::get_int(const char *fname) const {
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_int(get_field_no(fname));
}

std::optional<long> mysql_connection::pr_set::get_long(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_LONGLONG),
        "invalid value-type (long) for field: ", fi);
  return _rbinds[fi].is_null_value ? null_long
                                   : std::optional<long>{_rdata[fi].v_long()};
}

std::optional<long> mysql_connection::pr_set::get_long(
    const char *fname) const {
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_long(get_field_no(fname));
}

std::optional<float> mysql_connection::pr_set::get_float(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi],
                                     {MYSQL_TYPE_FLOAT, MYSQL_TYPE_NEWDECIMAL}),
        "invalid value-type (float) for field: ", fi);
  return _rbinds[fi].is_null_value ? null_float
                                   : std::optional<float>{_rdata[fi].v_float()};
}

std::optional<float> mysql_connection::pr_set::get_float(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_float(get_field_no(fname));
}

std::optional<double> mysql_connection::pr_set::get_double(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(
            *_fields[fi], {MYSQL_TYPE_DOUBLE, MYSQL_TYPE_NEWDECIMAL}),
        "invalid value-type (double) for field: ", fi);
  return _rbinds[fi].is_null_value
             ? null_double
             : std::optional<double>{_rdata[fi].v_double()};
}

std::optional<double> mysql_connection::pr_set::get_double(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_double(get_field_no(fname));
}

std::optional<std::string> mysql_connection::pr_set::get_string(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_VAR_STRING),
        "invalid value-type (string) for field: ", fi);
  return _rbinds[fi].is_null_value
             ? null_string
             : std::optional<std::string>{_rdata[fi].v_string()};
}

std::optional<std::string> mysql_connection::pr_set::get_string(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_string(get_field_no(fname));
}

std::optional<char> mysql_connection::pr_set::get_char(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_STRING),
        "invalid value-type (char) for field: ", fi);
  return _rbinds[fi].is_null_value ? null_char
                                   : std::optional<char>{_rdata[fi].v_char()};
}

std::optional<char> mysql_connection::pr_set::get_char(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_char(get_field_no(fname));
}

std::optional<core::datetime::date> mysql_connection::pr_set::get_date(
    int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(*_fields[fi], MYSQL_TYPE_DATE),
        "invalid value-type (date) for field: ", fi);
  return _rbinds[fi].is_null_value
             ? null_date
             : std::optional<core::datetime::date>{_rdata[fi].v_tm()};
}

std::optional<core::datetime::date> mysql_connection::pr_set::get_date(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_date(get_field_no(fname));
}

std::optional<std::tm> mysql_connection::pr_set::get_timestamp(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  return _rbinds[fi].is_null_value ? null_timestamp
                                   : std::optional<std::tm>{_rdata[fi].v_tm()};
}

std::optional<std::tm> mysql_connection::pr_set::get_timestamp(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_timestamp(get_field_no(fname));
}

core::utils::byte_array mysql_connection::pr_set::get_bytes(int fi) const {
  TRACE_INIT();
  CHECK_FIELD_INDEX(fi, _fields);
  CHECK(mysql_connection::is_of_type(
            *_fields[fi], {MYSQL_TYPE_TINY_BLOB, MYSQL_TYPE_MEDIUM_BLOB,
                           MYSQL_TYPE_BLOB, MYSQL_TYPE_LONG_BLOB}),
        "invalid value-type (byte_array) for field: ", fi);
  return _rbinds[fi].is_null_value ? core::utils::byte_array{}
                                   : _rdata[fi].v_byte_array();
}

core::utils::byte_array mysql_connection::pr_set::get_bytes(
    const char *fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_bytes(get_field_no(fname));
}

///////////////////////////////////////////////////////////////////////////////
// class result_data
mysql_connection::result_data::result_data() {
  TRACE_INIT();
  clear(false);
}

mysql_connection::result_data::result_data(result_data &&o)
    : _data{o._data}, _field_type{o._field_type}, _bind{o._bind} {
  TRACE_INIT();
  o.clear(false);
}

mysql_connection::result_data::~result_data() {
  TRACE_INIT();
  clear(true);
}

mysql_connection::result_data &mysql_connection::result_data::operator=(
    result_data &&o) {
  TRACE_INIT();
  if (this != &o) {
    _data = o._data;
    _field_type = o._field_type;
    _bind = o._bind;
    o.clear(false);
  }
  return *this;
}

void mysql_connection::result_data::clear(const bool full) {
  TRACE_INIT();
  if (full) {
    if (is_string()) {
      delete[] _data.v_string;
    } else if (is_byte_array()) {
      delete[] _data.v_byte_array;
    } else if (is_decimal()) {
      delete[] _data.v_decimal_string;
    }
  }
  std::memset(&_data, 0, sizeof(_data));
  _field_type = field_type::NONE;
  _bind = nullptr;
}

void mysql_connection::result_data::bind_bool(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::BOOL;
  bind.buffer = &_data.v_bool;
  bind.buffer_length = sizeof(_data.v_bool);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_short(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::SHORT;
  bind.buffer = &_data.v_short;
  bind.buffer_length = sizeof(_data.v_short);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_int(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::INT;
  bind.buffer = &_data.v_int;
  bind.buffer_length = sizeof(_data.v_int);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_long(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::LONG;
  bind.buffer = &_data.v_long;
  bind.buffer_length = sizeof(_data.v_long);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_float(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::FLOAT;
  bind.buffer = &_data.v_float;
  bind.buffer_length = sizeof(_data.v_float);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_double(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::DOUBLE;
  bind.buffer = &_data.v_double;
  bind.buffer_length = sizeof(_data.v_double);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_decimal(MYSQL_BIND &bind,
                                                 const std::size_t len) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::DOUBLE;
  _data.v_decimal_string = new char[len + 1];
  std::memset(_data.v_decimal_string, 0, len + 1);
  bind.buffer = _data.v_decimal_string;
  bind.buffer_length = len;
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_date(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::DATE;
  bind.buffer = &_data.v_tm;
  bind.buffer_length = sizeof(_data.v_tm);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_timestamp(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::TIMESTAMP;
  bind.buffer = &_data.v_tm;
  bind.buffer_length = sizeof(_data.v_tm);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_char(MYSQL_BIND &bind) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::CHAR;
  bind.buffer = &_data.v_char;
  bind.buffer_length = sizeof(_data.v_char);
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_string(MYSQL_BIND &bind,
                                                const std::size_t len) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::STRING;
  _data.v_string = new char[len + 1];
  std::memset(_data.v_string, 0, len + 1);
  bind.buffer = _data.v_string;
  bind.buffer_length = len;
  bind.is_unsigned = 0;
}

void mysql_connection::result_data::bind_blob(MYSQL_BIND &bind,
                                              const std::size_t len) {
  TRACE_INIT();
  _bind = &bind;
  _field_type = field_type::BYTE_ARRAY;
  _data.v_byte_array = new char[len + 1];
  std::memset(_data.v_byte_array, 0, len + 1);
  bind.buffer = _data.v_byte_array;
  bind.buffer_length = len;
  bind.is_unsigned = 0;
}

bool mysql_connection::result_data::v_bool() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::BOOL, "not a bool value");
  return _data.v_bool;
}

short mysql_connection::result_data::v_short() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::SHORT, "not a short value");
  return _data.v_short;
}

int mysql_connection::result_data::v_int() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::INT, "not an int  value");
  return _data.v_int;
}

long mysql_connection::result_data::v_long() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::LONG, "not a long value");
  return _data.v_long;
}

float mysql_connection::result_data::v_float() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::FLOAT, "not a float value");
  if (is_decimal()) {
    const auto &[res, msg, val] =
        core::utils::string_to_fp<float>(_data.v_decimal_string);
    CHECK(res, msg);
    return val;
  }
  return _data.v_float;
}

double mysql_connection::result_data::v_double() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::DOUBLE, "not a double value");
  if (is_decimal()) {
    const auto &[res, msg, val] =
        core::utils::string_to_fp<double>(_data.v_decimal_string);
    CHECK(res, msg);
    return val;
  }
  return _data.v_double;
}

char mysql_connection::result_data::v_char() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::CHAR, "not a char value");
  return _data.v_char;
}

std::string mysql_connection::result_data::v_string() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::STRING, "not a string value");
  return std::string{core::utils::is_str_null(_data.v_string) ? ""
                                                              : _data.v_string};
}
core::utils::byte_array mysql_connection::result_data::v_byte_array() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::BYTE_ARRAY, "not a byte_array value");
  CHECK(_bind != nullptr, "_bind is uninitialized (null)");
  if (*(_bind->length) < 1UL) {
    return core::utils::byte_array{};
  }
  CHECK(_data.v_byte_array != nullptr,
        "_data.v_byte_array is uninitialized (null)");
  return core::utils::byte_array{(const unsigned char *)_data.v_byte_array,
                                 *(_bind->length)};
}

core::datetime::date mysql_connection::result_data::v_date() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::DATE, "not a date value");
  return core::datetime::date{(unsigned short)_data.v_tm.day,
                              (unsigned short)_data.v_tm.month,
                              (short)_data.v_tm.year};
}

std::tm mysql_connection::result_data::v_tm() const {
  TRACE_INIT();
  CHECK(_field_type == field_type::TIMESTAMP, "not a timestamp value");
  std::tm res{};
  res.tm_year = _data.v_tm.year - 1900;
  res.tm_mon = _data.v_tm.month - 1;
  res.tm_mday = _data.v_tm.day;
  res.tm_hour = _data.v_tm.hour;
  res.tm_min = _data.v_tm.minute;
  res.tm_sec = _data.v_tm.second;
  return res;
}

bool mysql_connection::result_data::is_string() const {
  TRACE_INIT();
  return _field_type == field_type::STRING && _data.v_string != nullptr;
}

bool mysql_connection::result_data::is_byte_array() const {
  TRACE_INIT();
  return _field_type == field_type::BYTE_ARRAY && _data.v_byte_array != nullptr;
}

bool mysql_connection::result_data::is_decimal() const {
  TRACE_INIT();
  return _field_type == field_type::DOUBLE && _data.v_decimal_string != nullptr;
}

///////////////////////////////////////////////////////////////////////////////
// class param_bind
mysql_connection::param_bind::param_bind() : _bind{nullptr} {
  TRACE_INIT();
  clear();
}

mysql_connection::param_bind::param_bind(const param_bind &o)
    : _data{o._data}, _bind{o._bind} {
  TRACE_INIT();
}

mysql_connection::param_bind &mysql_connection::param_bind::operator=(
    const param_bind &o) {
  TRACE_INIT();
  if (this != &o) {
    _data = o._data;
    _bind = o._bind;
  }
  return *this;
}

void mysql_connection::param_bind::clear(const bool full) {
  TRACE_INIT();
  if (full && _bind != nullptr) {
    std::memset(&_bind, 0, sizeof(_bind));
  }
  std::memset(&_data, 0, sizeof(_data));
}

void mysql_connection::param_bind::bind(const param &p, MYSQL_BIND *bind) {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, bind != nullptr, "invalid bind parameter");
  _bind = bind;
  std::memset(bind, 0, sizeof(MYSQL_BIND));
  std::visit(*this, p.value());
}

// null value
mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const param::null &) const {
  TRACE_INIT();
  return std::make_tuple(true, MYSQL_TYPE_NULL, nullptr, 0);
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const bool &v) {
  TRACE_INIT();
  _data.v_bool = static_cast<char>(v);
  return std::make_tuple(false, MYSQL_TYPE_TINY, &_data.v_bool,
                         sizeof(_data.v_bool));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const short &v) {
  TRACE_INIT();
  _data.v_short = v;
  return std::make_tuple(false, MYSQL_TYPE_SHORT, &_data.v_short,
                         sizeof(_data.v_short));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const int &v) {
  TRACE_INIT();
  _data.v_int = v;
  return std::make_tuple(false, MYSQL_TYPE_LONG, &_data.v_int,
                         sizeof(_data.v_int));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const long &v) {
  TRACE_INIT();
  _data.v_long = v;
  return std::make_tuple(false, MYSQL_TYPE_LONGLONG, &_data.v_long,
                         sizeof(_data.v_long));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const long long &v) {
  TRACE_INIT();
  _data.v_long = v;
  return std::make_tuple(false, MYSQL_TYPE_LONGLONG, &_data.v_long,
                         sizeof(_data.v_long));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const float &v) {
  TRACE_INIT();
  _data.v_float = v;
  return std::make_tuple(false, MYSQL_TYPE_FLOAT, &_data.v_float,
                         sizeof(_data.v_float));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const double &v) {
  TRACE_INIT();
  _data.v_double = v;
  return std::make_tuple(false, MYSQL_TYPE_DOUBLE, &_data.v_double,
                         sizeof(_data.v_double));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const char &v) {
  TRACE_INIT();
  _data.v_char = v;
  return std::make_tuple(false, MYSQL_TYPE_STRING, &_data.v_char,
                         sizeof(_data.v_char));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const std::string &v) {
  TRACE_INIT();
  _data.v_string = v.empty() ? nullptr : v.c_str();
  return std::make_tuple(false, MYSQL_TYPE_VAR_STRING, (void *)_data.v_string,
                         v.empty() ? 0UL : v.length());
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const core::utils::byte_array &v) {
  TRACE_INIT();
  _data.v_byte_array = v.get();
  return std::make_tuple(false, MYSQL_TYPE_BLOB, (void *)_data.v_byte_array,
                         v.length());
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const core::datetime::date &v) {
  TRACE_INIT();
  _data.v_tm.year = v.year();
  _data.v_tm.month = v.month();
  _data.v_tm.day = v.day();
  return std::make_tuple(false, MYSQL_TYPE_DATE, (void *)&_data.v_tm,
                         sizeof(_data.v_tm));
}

mysql_connection::param_bind::ext_res mysql_connection::param_bind::extract(
    const std::tm &v) {
  TRACE_INIT();
  _data.v_tm.year = 1900 + v.tm_year;
  _data.v_tm.month = v.tm_mon + 1;
  _data.v_tm.day = v.tm_mday;
  _data.v_tm.hour = v.tm_hour;
  _data.v_tm.minute = v.tm_min;
  _data.v_tm.second = v.tm_sec;
  return std::make_tuple(false, MYSQL_TYPE_TIMESTAMP, (void *)&_data.v_tm,
                         sizeof(_data.v_tm));
}

///////////////////////////////////////////////////////////////////////////////
// class mysql_statement_handle
mysql_connection::mysql_statement_handle::mysql_statement_handle(
    MYSQL *myconn, MYSQL_STMT *stmt)
    : statement_handle{}, _myconn{myconn}, _stmt(stmt) {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, _stmt != nullptr, "invalid stmt parameter");
}

mysql_connection::mysql_statement_handle::~mysql_statement_handle() {
  TRACE_INIT();
  try {
    close();
  } catch (...) {
    // catch anything in dtor
  }
}

void mysql_connection::mysql_statement_handle::close() {
  TRACE_INIT();
  if (_stmt != nullptr) {
    // ensure that _stmt is set to nullptr on exit
    core::utils::raii_func r{[this]() { _stmt = nullptr; }};
    CHECK_STATUS(_myconn, mysql_stmt_free_result(_stmt));
    CHECK_STATUS(_myconn, mysql_stmt_close(_stmt));
  }
}

MYSQL_STMT *mysql_connection::mysql_statement_handle::get() const {
  TRACE_INIT();
  return _stmt;
}

// with MySQL, there is no way to get prepared statement parameter
// metadata. Currently [18.12.2018], the mysql_stmt_param_metadata
// function does nothing)
// https://dev.mysql.com/doc/refman/5.7/en/mysql-stmt-param-metadata.html
// so, there is no way to validate parameter data types prior to
// statement execution
void mysql_connection::mysql_statement_handle::execute(
    const std::initializer_list<param> &params, const bool store_result) const {
  TRACE_INIT();
  CHECK(_stmt != nullptr, "_stmt is uninitialized (null)");
  CHECK(param_count() == params.size(), "incorrect number of params specified");
  std::vector<param_bind> param_binds{};
  std::vector<MYSQL_BIND> mysql_binds{};
  if (params.size() > 0UL) {
    const std::size_t pcnt = params.size();
    param_binds.resize(pcnt);
    mysql_binds.resize(pcnt);
    std::size_t i{0UL};
    for (const auto &param : params) {
      auto &param_bind{param_binds[i]};
      auto mysql_bind{std::addressof(mysql_binds[i])};
      param_bind.bind(param, mysql_bind);
      ++i;
    }
    CHECK_STATUS(_myconn, mysql_stmt_bind_param(
                              _stmt, std::addressof(*mysql_binds.begin())));
  }
  CHECK(mysql_stmt_execute(_stmt) == 0, mysql_stmt_error(_stmt));
  if (store_result) {
    CHECK(mysql_stmt_store_result(_stmt) == 0, mysql_stmt_error(_stmt));
  }
}

std::size_t mysql_connection::mysql_statement_handle::param_count() const {
  TRACE_INIT();
  CHECK(_stmt != nullptr, "_stmt is not initialized");
  return mysql_stmt_param_count(_stmt);
}

///////////////////////////////////////////////////////////////////////////////
// class mysql_connection

mysql_connection::mysql_connection(const rapidjson::Value &json)
    : connection{json, connection::db_type::MySQL}, _myconn{nullptr} {
  TRACE_INIT();
}

mysql_connection::~mysql_connection() {
  TRACE_INIT();
  try {
    disconnect();
  } catch (const std::exception &ex) {
    LOG_SEVERE(
        core::utils::build_string(
            "exception encountered in mysql_connection dtor: ", ex.what())
            .c_str());
  } catch (...) {
    LOG_SEVERE("uknown exception encountered in mysql_connection dtor");
  }
}

std::string mysql_connection::byte_array_to_str(
    const core::utils::byte_array &ba) const {
  TRACE_INIT();
  if (ba.is_null()) {
    return core::globals::empty_string;
  }
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  const std::size_t size{(ba.length() * 2) + 4};
  std::string str(size, 0);
  const std::size_t len(
      mysql_real_escape_string(_myconn, str.data(), ba.get(), ba.length()));
  str.resize(len);
  return str;
}

// interface
void mysql_connection::_connect() {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  _myconn = mysql_init(nullptr);
  CHECK(_myconn != nullptr, mysql_error(_myconn));
  try {
    const int flags{[this]() {
      const auto &v{conf()["use_compression"]};
      return v.IsNull() ? 0 : CLIENT_COMPRESS;
    }()};
    MYSQL *c{mysql_real_connect(_myconn, host().c_str(), user().c_str(),
                                password().c_str(), database().c_str(), port(),
                                nullptr, flags)};
    CHECK(c != nullptr && c == _myconn, mysql_error(_myconn));
    // set transaction isolation level
    exec_sql(core::utils::build_string(
        "SET SESSION TRANSACTION ISOLATION LEVEL ",
        transaction_isolation_level_to_string(trans_isolation_level()),
        ", READ WRITE"));
    // set autocommit off
    CHECK_STATUS(_myconn, mysql_autocommit(_myconn, 0));
  } catch (...) {
    _myconn = nullptr;
    throw;
  }
}

bool mysql_connection::active() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  return _myconn != nullptr && ping();
}

bool mysql_connection::ping() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  return mysql_ping(_myconn) == 0;
}

void mysql_connection::_start_transaction() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  CHECK_STATUS(_myconn, mysql_query(_myconn, "START TRANSACTION"));
}

void mysql_connection::_commit() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  CHECK_STATUS(_myconn, mysql_commit(_myconn));
}

void mysql_connection::_rollback() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  CHECK_STATUS(_myconn, mysql_rollback(_myconn));
}

std::string mysql_connection::escape_string(const std::string &sql) const {
  // https://dev.mysql.com/doc/refman/5.5/en/mysql-real-escape-string.html
  // The string pointed to by from must be length bytes long. You must
  // allocate the to buffer to be at least length*2+1 bytes long.
  TRACE_INIT();
  if (sql.empty()) {
    return sql;
  }
  const std::size_t sz{(sql.length() * 2) + 1};
  std::string res{};
  res.reserve(sz);
  res.resize(sz, 0);
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  const std::size_t len(
      mysql_real_escape_string(_myconn, res.data(), sql.c_str(), sql.length()));
  res.resize(len);
  return res;
}

void mysql_connection::_disconnect() noexcept {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  if (_myconn != nullptr) {
    mysql_close(_myconn);
    _myconn = nullptr;
  }
}

void mysql_connection::_preprocess_insert_statement(std::string &,
                                                    const std::string &) const {
  // empty - nothing in particular to be done in the case of a
  // mysql_connection
  TRACE_INIT();
}

std::unique_ptr<rowset> mysql_connection::_exec_select(
    const statement_handle &s,
    const std::initializer_list<param> &params) const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  const auto &stmt{dynamic_cast<const mysql_statement_handle &>(s)};
  stmt.execute(params, true);
  return std::make_unique<pr_set>(_myconn, stmt.get());
}

result mysql_connection::_exec_insert(
    const statement_handle &s, const std::initializer_list<param> &params,
    const std::string &key_field_name) const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  const auto &stmt{dynamic_cast<const mysql_statement_handle &>(s)};
  stmt.execute(params);
  const std::size_t update_count{
      static_cast<std::size_t>(mysql_stmt_affected_rows(stmt.get()))};
  std::vector<std::size_t> generated_keys{};
  if (!key_field_name.empty() && update_count > 0UL) {
    generated_keys.reserve(1);
    const std::size_t id{
        static_cast<std::size_t>(mysql_stmt_insert_id(stmt.get()))};
    CHECK_ERRNO(_myconn);
    if (id > 0) {
      generated_keys.emplace_back(id);
    }
  }
  return result{update_count, generated_keys};
}

std::size_t mysql_connection::_exec_update(
    const statement_handle &s,
    const std::initializer_list<param> &params) const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  const auto &stmt{dynamic_cast<const mysql_statement_handle &>(s)};
  stmt.execute(params);
  return mysql_stmt_affected_rows(stmt.get());
}

std::unique_ptr<connection::statement_handle>
mysql_connection::_create_statement_handle(const std::string &sql,
                                           const std::string &) const {
  TRACE_INIT();
  // we don't have to check the sql and md5 parameters; they are checked at the
  // base class level
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  const auto sh{mysql_stmt_init(_myconn)};
  CHECK_NOT_NULL(_myconn, sh);
  std::unique_ptr<mysql_statement_handle> stmt{
      std::make_unique<mysql_statement_handle>(_myconn, sh)};
  CHECK_STATUS(_myconn,
               mysql_stmt_prepare(stmt->get(), sql.c_str(), sql.length()));
  {
    // cursors are to be designated as read-only
    unsigned long v{CURSOR_TYPE_READ_ONLY};
    CHECK_STATUS(_myconn,
                 mysql_stmt_attr_set(stmt->get(), STMT_ATTR_CURSOR_TYPE, &v));
  }
  {
    // set the prefetch row count (default to 4096 if not specified)
    const auto v{
        conf().HasMember("prefetch_row_count")
            ? static_cast<std::size_t>(conf()["prefetch_row_count"].GetUint64())
            : 4096UL};
    CHECK_STATUS(_myconn,
                 mysql_stmt_attr_set(stmt->get(), STMT_ATTR_PREFETCH_ROWS, &v));
  }
  {
    // get mysql to always return the maximum length field for fields returned
    const my_bool v = 1;
    CHECK_STATUS(_myconn, mysql_stmt_attr_set(stmt->get(),
                                              STMT_ATTR_UPDATE_MAX_LENGTH, &v));
  }
  return stmt;
}  // namespace core::db

std::unique_ptr<rowset> mysql_connection::_exec_select_direct(
    const std::string &sql) const {
  TRACE_INIT();
  // we don't have to check the sql parameter;
  // it is checked at the base class level
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  exec_sql(sql);
  mysql_result res{mysql_store_result(_myconn)};
  CHECK_NOT_NULL(_myconn, res.get());
  return std::make_unique<dr_set>(_myconn, res.release());
}

result mysql_connection::_exec_insert_direct(
    const std::string &sql, const std::string &key_field_name) const {
  TRACE_INIT();
  // we don't have to check the sql parameter;
  // it is checked at the base class level
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  exec_sql(sql);
  const std::size_t update_count = mysql_affected_rows(_myconn);
  CHECK(update_count != static_cast<std::size_t>(-1), mysql_error(_myconn));
  std::vector<std::size_t> generated_keys{};
  generated_keys.reserve(1);
  if (!key_field_name.empty()) {
    const std::size_t id{static_cast<std::size_t>(mysql_insert_id(_myconn))};
    CHECK_ERRNO(_myconn);
    if (id > 0) {
      generated_keys.emplace_back(id);
    }
  }
  return result{update_count, generated_keys};
}

std::size_t mysql_connection::_exec_update_direct(
    const std::string &sql) const {
  TRACE_INIT();
  // we don't have to check the sql parameter;
  // it is checked at the base class level
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_myconn);
  exec_sql(sql);
  const std::size_t update_count = mysql_affected_rows(_myconn);
  CHECK(update_count != static_cast<std::size_t>(-1), mysql_error(_myconn));
  return update_count;
}

void mysql_connection::exec_sql(const std::string &sql) const {
  TRACE_INIT();
  CHECK_STATUS(_myconn, mysql_real_query(_myconn, sql.c_str(), sql.length()));
}

bool mysql_connection::is_of_type(const MYSQL_FIELD &field,
                                  const enum_field_types &type) {
  return field.type == type;
}

bool mysql_connection::is_of_type(
    const MYSQL_FIELD &field,
    const std::initializer_list<enum_field_types> &types) {
  for (const auto &type : types) {
    if (field.type == type) {
      return true;
    }
  }
  return false;
}

// struct that takes care of loading/unloading of mysql library
static struct mysql_connection_initializer {
  mysql_connection_initializer() { mysql_library_init(0, nullptr, nullptr); }
  ~mysql_connection_initializer() { mysql_library_end(); }
} mysql_connection_initializer_instance;

}  // namespace core::db
