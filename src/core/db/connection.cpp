// C++
#include <set>

// project/local
#include <core/db/connection.h>
#include <core/utils/md5.h>

namespace core::db {

///////////////////////////////////////////////////////////////////////////////
// class connection::statement_handle
connection::statement_handle::statement_handle() { TRACE_INIT(); }

connection::statement_handle::~statement_handle() { TRACE_INIT(); }

///////////////////////////////////////////////////////////////////////////////
// class connection::prepared_statement_map

connection::prepared_statement_map::prepared_statement_map(
    const connection &conn)
    : base{}, _conn{conn} {
  TRACE_INIT();
  //
}

connection::prepared_statement_map::~prepared_statement_map() {
  TRACE_INIT();
  //
  try {
    clear();
  } catch (...) {
    LOG_WARN("exception encountered in prepared_statement_map dtor");
  }
}

const connection::statement_handle &connection::prepared_statement_map::get(
    const std::string &sql) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !sql.empty(), "sql parameter cannot be empty");

  const auto md5{core::utils::md5::compute(sql.c_str(), sql.length())};
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  const auto i = base::find(md5);
  if (i != base::end()) {
    return *(i->second);
  }
  auto sh{_conn._create_statement_handle(sql, md5)};
  CHECK(sh.get() != nullptr, "_conn._create_statement_handle returned nullptr");
  CHECK(base::insert(std::make_pair(md5, sh.get())).second,
        "error adding statement to prepared statement cache");
  LOG_SQL(fmt::format("Prepared statement [md5: {:s}]: {:s}", md5, sql));
  return *sh.release();
}

void connection::prepared_statement_map::clear() {
  TRACE_INIT();
  //
  for (const auto &[k, v] : *this) {
    (void)k;
    try {
      v->close();
    } catch (...) {
      // catch anything here to ensure that the deletion below happens
      LOG_WARN("exception encountered when calling statement_handle::close");
    }
    delete v;
  }
  base::clear();
}

///////////////////////////////////////////////////////////////////////////////
// class connection
connection::connection(const rapidjson::Value &conf, const db_type type)
    : _conf{conf},
      _dbtype{type},
      _host{core::utils::get_json_value<std::string>(_conf, "host")},
      _port{core::utils::get_json_value<std::uint16_t>(_conf, "port")},
      _user{core::utils::get_json_value<std::string>(_conf, "user")},
      _password{core::utils::get_json_value<std::string>(_conf, "password")},
      _database{core::utils::get_json_value<std::string>(_conf, "dbname")},
      _trans_isolation_level{get_transaction_isolation_level(_conf)},
      _in_transaction{false},
      _prepared_statement_map{*this} {
  TRACE_INIT();
  //
}

connection::~connection() noexcept {
  TRACE_INIT();
  //
}

connection::db_type connection::type() const {
  TRACE_INIT();
  //
  return _dbtype;
}

connection::transaction_isolation_level connection::trans_isolation_level()
    const {
  TRACE_INIT();
  //
  return _trans_isolation_level;
}
const std::string &connection::host() const {
  TRACE_INIT();
  //
  return _host;
}
std::uint16_t connection::port() const {
  TRACE_INIT();
  //
  return _port;
}
const std::string &connection::user() const {
  TRACE_INIT();
  //
  return _user;
}
const std::string &connection::password() const {
  TRACE_INIT();
  //
  return _password;
}
const std::string &connection::database() const {
  TRACE_INIT();
  //
  return _database;
}

std::string connection::get_info_str(const std::string &name) const {
  TRACE_INIT();
  //
  return fmt::format("name: {:s}; db_type: {:s}. [host: {:s}]", name,
                     connection::db_type_to_str(_dbtype), _host);
}

std::string connection::params_to_string(
    const std::initializer_list<param> &params,
    const std::set<std::uint16_t> &mask_indices) {
  //
  static std::string _no_params_{"[]"};
  //
  TRACE_INIT();
  //
  if (params.size() == 0) {
    return _no_params_;
  }
  std::stringstream sstr{};
  sstr << '[';
  std::size_t i{0UL};
  for (const auto &p : params) {
    std::string v{p.to_string()};
    if (mask_indices.find(i) != mask_indices.end()) {
      sstr << '[' << i << "]: " << std::string(v.length(), '*') << "; ";
    } else {
      sstr << '[' << i << "]: " << v << "; ";
    }
    ++i;
  }
  std::string res{sstr.str()};
  if (res.length() > 2) {
    res.erase(res.length() - 2);
  }
  res.append("]");
  return res;
}

void connection::connect() {
  TRACE_INIT();
  //
  CHECK(!active(), "connection already established");
  CHECK(_prepared_statement_map.empty(),
        "prepared statement map is not empty!");
  _connect();
}

void connection::disconnect() {
  TRACE_INIT();
  //
  if (active()) {
    _disconnect();
  }
  _prepared_statement_map.clear();
}

void connection::start_transaction() const {
  TRACE_INIT();
  //
  CHECK(!_in_transaction, "another transaction is already active");
  try {
    _start_transaction();
    _in_transaction = true;
  } catch (...) {
    _in_transaction = false;
    throw;
  }
}

void connection::commit() const {
  TRACE_INIT();
  //
  CHECK(_in_transaction, "no current transaction active (commit)");
  try {
    _commit();
    _in_transaction = false;
  } catch (...) {
    _in_transaction = true;
    throw;
  }
}

void connection::rollback() const {
  TRACE_INIT();
  //
  CHECK(_in_transaction, "no current transaction active (rollback)");
  try {
    _rollback();
    _in_transaction = false;
  } catch (...) {
    _in_transaction = true;
    throw;
  }
}

bool connection::in_transaction() const {
  TRACE_INIT();
  //
  return _in_transaction;
}

std::unique_ptr<rowset> connection::exec_select(
    const std::string &sql, const std::initializer_list<param> &params,
    const std::set<std::uint16_t> &mask_indices) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !sql.empty(), "invalid sql param (empty)");
  CHECK(is_select_statement(sql), "not a select statement");
  LOG_SQL(fmt::format("{:s}. Params: {:s}", sql,
                      params_to_string(params, mask_indices)));
  const auto &stmt{_prepared_statement_map.get(process_statement(sql, true))};
  std::lock_guard lg{stmt._mtx};
  std::unique_ptr<rowset> rs{_exec_select(stmt, params)};
  rs->_mtx = std::addressof(stmt._mtx);
  stmt._mtx.lock();  // lock this again (it will be released in the rowset's
                     // destructor)
  return rs;
}

result connection::exec_insert(const std::string &sql,
                               const std::initializer_list<param> &params,
                               const std::string &key_field_name,
                               const std::set<std::uint16_t> &mask_indices) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !sql.empty(), "invalid sql param (empty)");
  CHECK(is_insert_statement(sql), "not an insert statement");
  LOG_SQL(fmt::format("{:s}. Params: {:s}", sql,
                      params_to_string(params, mask_indices)));
  const auto &stmt{_prepared_statement_map.get(
      process_statement(sql, true, key_field_name))};
  std::lock_guard<std::recursive_mutex> lg{stmt._mtx};
  return _exec_insert(stmt, params, key_field_name);
}

std::size_t connection::exec_update(
    const std::string &sql, const std::initializer_list<param> &params,
    const std::set<std::uint16_t> &mask_indices) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !sql.empty(), "invalid sql param (empty)");
  CHECK(is_update_statement(sql), "not an update statement");
  LOG_SQL(fmt::format("{:s}. Params: {:s}", sql,
                      params_to_string(params, mask_indices)));
  const auto &stmt{_prepared_statement_map.get(process_statement(sql, true))};
  std::lock_guard<std::recursive_mutex> lg{stmt._mtx};
  return _exec_update(stmt, params);
}

std::unique_ptr<rowset> connection::exec_select_direct(
    const std::string &sql, const bool escape_stmt) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !sql.empty(), "invalid sql param (empty)");
  CHECK(is_select_statement(sql), "not a select statement");
  LOG_SQL(sql.c_str());
  return _exec_select_direct(process_statement(sql, escape_stmt));
}

result connection::exec_insert_direct(const std::string &sql,
                                      const std::string &key_field_name,
                                      const bool escape_stmt) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !sql.empty(), "invalid sql param (empty)");
  CHECK(is_insert_statement(sql), "not an insert statement");
  LOG_SQL(sql.c_str());
  return _exec_insert_direct(process_statement(sql, escape_stmt),
                             key_field_name);
}

std::size_t connection::exec_update_direct(const std::string &sql,
                                           const bool escape_stmt) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !sql.empty(), "invalid sql param (empty)");
  CHECK(is_update_statement(sql), "not an update statement");
  LOG_SQL(sql.c_str());
  return _exec_update_direct(process_statement(sql, escape_stmt));
}

const rapidjson::Value &connection::conf() const {
  TRACE_INIT();
  //
  return _conf;
}

std::string connection::process_statement(
    const std::string &sql, const bool escape_stmt,
    const std::string &key_field_name) const {
  TRACE_INIT();
  //
  if (!key_field_name.empty() && is_insert_statement(sql)) {
    std::string res{sql};
    _preprocess_insert_statement(res, key_field_name);
    return escape_stmt ? escape_string(res) : res;
  }
  return escape_stmt ? escape_string(sql) : sql;
}

bool connection::is_select_statement(const std::string &sql) {
  TRACE_INIT();
  //
  return match_statement(__rx_select, sql);
}

bool connection::is_insert_statement(const std::string &sql) {
  TRACE_INIT();
  //
  return match_statement(__rx_insert, sql);
}

bool connection::is_update_statement(const std::string &sql) {
  TRACE_INIT();
  //
  return match_statement(__rx_update, sql);
}

connection::transaction_isolation_level
connection::string_to_transaction_isolation_level(const std::string &til_str) {
  static std::map<const std::string, transaction_isolation_level> map{
      {"READ UNCOMMITTED", transaction_isolation_level::READ_UNCOMMITTED},
      {"READ COMMITTED", transaction_isolation_level::READ_COMMITTED},
      {"REPEATABLE READ", transaction_isolation_level::REPEATABLE_READ},
      {"SERIALIZABLE", transaction_isolation_level::SERIALIZEABLE}};
  //
  TRACE_INIT();
  //
  const auto i{map.find(til_str)};
  CHECK(i != map.end(), "invalid transaction_isolation_level string");
  return i->second;
}

std::string connection::transaction_isolation_level_to_string(
    const transaction_isolation_level til) {
  TRACE_INIT();
  //
  static std::map<transaction_isolation_level, const std::string> map{
      {transaction_isolation_level::READ_UNCOMMITTED, "READ UNCOMMITTED"},
      {transaction_isolation_level::READ_COMMITTED, "READ COMMITTED"},
      {transaction_isolation_level::REPEATABLE_READ, "REPEATABLE READ"},
      {transaction_isolation_level::SERIALIZEABLE, "SERIALIZABLE"}};
  //
  const auto i{map.find(til)};
  CHECK(i != map.end(), "invalid transaction_isolation_level");
  return i->second;
}

connection::transaction_isolation_level
connection::get_transaction_isolation_level(const rapidjson::Value &json) {
  TRACE_INIT();
  //
  if (!json.HasMember("transaction_isolation_level")) {
    return transaction_isolation_level::READ_COMMITTED;
  }
  const auto &v{json["transaction_isolation_level"]};
  CHECK(v.IsString(),
        "transaction_isolation_level value is not of type string");
  const std::string til{json["transaction_isolation_level"].GetString()};
  return (til.empty()) ? transaction_isolation_level::READ_COMMITTED
                       : string_to_transaction_isolation_level(til);
}

const char *connection::db_type_to_str(const db_type dbtype) {
  TRACE_INIT();
  //
  switch (dbtype) {
    case db_type::MySQL:
      return "MySQL (db_type::MySQL)";
    case db_type::PgSQL:
      return "Postgres (db_type::PgSQL)";
    default:
      CHECK(false,
            core::utils::build_string("unhandled db_type encountered: ",
                                      static_cast<std::int64_t>(dbtype)));
      return nullptr;  // here just to satisfy the compiler
  }
}

bool connection::match_statement(const std::regex &rx, const std::string &sql) {
  static std::recursive_mutex mtx;
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !sql.empty(), "invalid sql parameter");
  std::lock_guard<std::recursive_mutex> lg{mtx};
  // only check first 6 characters
  return std::regex_match(sql.length() < 6 ? sql : sql.substr(0, 6UL), rx);
}

// statics
const std::regex connection::__rx_insert("^insert$",
                                         std::regex_constants::icase);
const std::regex connection::__rx_select("^select$",
                                         std::regex_constants::icase);
const std::regex connection::__rx_update("^(update|delete)$",
                                         std::regex_constants::icase);

}  // namespace core::db
