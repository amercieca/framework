// local/project
#include <core/db/date.h>
#include <core/utils/utils.h>

namespace core::datetime {

date::date(const unsigned short day, const unsigned short month,
           const short year)
    : _day{day}, _month{month}, _year{year} {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, is_valid(), "invalid date parameters");
}

date::date(const std::tm &v, const std::optional<int> &offset) {
  TRACE_INIT();

  // calculate the OS's offset from GMT/UTC
  const int os_offset{[](const bool is_dst) {
    const int fac{is_dst ? 3600 : 0};
#ifdef WIN32
    // https://support.zabbix.com/browse/ZBX-9839
    return -_timezone - fac;
#else
    const time_t t{time(nullptr)};
    struct std::tm lt {};
    localtime_r(&t, &lt);
    return static_cast<int>(lt.tm_gmtoff) - fac - (lt.tm_isdst ? 3600 : 0);
#endif
  }(v.tm_isdst != 0)};

  // if not time zone offset is not specified, then assume that
  // the date/time parameter received is in local date/time
  // of the machine on which this code is running
  const auto ofs{(bool)offset ? *offset : os_offset};

  if (os_offset != 0 || ofs != 0) {
    const auto t{((mktime((std::tm *)&v) + os_offset)) - ofs};
    CHECK(t != (time_t)-1, core::utils::get_os_error_string());
    struct std::tm g {};
#ifdef WIN32
    CHECK(gmtime_s(&g, &t) == 0, core::utils::get_os_error_string());
#else
    CHECK(gmtime_r(&t, &g) == &g, core::utils::get_os_error_string());
#endif
    init(g);
  } else {
    init(v);
  }
  CHECK_T(std::invalid_argument, is_valid(), "invalid std::tm parameter");
}

date::date(const std::time_t &v, const bool is_utc) {
  TRACE_INIT();
  const auto t{is_utc ? std::gmtime(&v) : std::localtime(&v)};
  CHECK(t != nullptr, core::utils::get_os_error_string());
  init(*t);
  CHECK_T(std::invalid_argument, is_valid(), "invalid std::time_t parameter");
}

date::date(const date &o) : _day{o._day}, _month{o._month}, _year{o._year} {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, is_valid(), "invalid date parameters");
}

date::date(date &&o) : _day{o._day}, _month{o._month}, _year{o._year} {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, is_valid(), "invalid date parameters");
}

date &date::operator=(const date &o) {
  TRACE_INIT();
  if (this != &o) {
    // casts to remove constness
    ((unsigned short &)_day) = o._day;
    ((unsigned short &)_month) = o._month;
    ((short &)_year) = o._year;
    CHECK_T(std::invalid_argument, is_valid(), "invalid date parameter");
  }
  return *this;
}

date &date::operator=(date &&o) {
  TRACE_INIT();
  if (this != &o) {
    // casts to remove constness
    ((unsigned short &)_day) = o._day;
    ((unsigned short &)_month) = o._month;
    ((short &)_year) = o._year;
    CHECK_T(std::invalid_argument, is_valid(), "invalid date parameter");
  }
  return *this;
}

unsigned short date::day() const {
  TRACE_INIT();
  return _day;
}

unsigned short date::month() const {
  TRACE_INIT();
  return _month;
}

short date::year() const {
  TRACE_INIT();
  return _year;
}

bool date::is_valid() const {
  TRACE_INIT();
  if (_month < 1 || _month > 12) {
    return false;
  }
  if (_day < 1) {
    return false;
  }
  switch (_month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return _day <= 31;
    case 4:
    case 6:
    case 9:
    case 11:
      return _day <= 30;
    case 2:
      return is_leap_year(_year) ? _day <= 29 : _day <= 28;
  }
  CHECK(false, core::utils::build_string("invalid _month value: ", _month));
  return false;  // only here to satisfy the compiler
}

std::string date::to_string() const {
  TRACE_INIT();
  std::string str(12, 0);
  const int sz{
      snprintf(str.data(), str.size(), "%02d-%02d-%4d", _day, _month, _year)};
  str.resize(sz);
  return str;
}

void date::init(const std::tm &v) {
  TRACE_INIT();
  ((unsigned short &)_day) = v.tm_mday;
  ((unsigned short &)_month) = v.tm_mon + 1;
  ((short &)_year) = 1900 + v.tm_year;
}

std::time_t date::to_time_t() const {
  TRACE_INIT();
  std::tm v;
  std::memset(&v, 0, sizeof(v));
  v.tm_mday = _day;
  v.tm_mon = _month - 1;
  v.tm_year = _year - 1900;
  v.tm_isdst = 0;
#ifndef WIN32
  v.tm_gmtoff = 0;
  v.tm_zone = __UTC;
#endif
  const auto r{std::mktime(&v)};
  CHECK(r != (time_t)-1, core::utils::get_os_error_string());
  return r;
}

std::tm date::to_tm() const {
  TRACE_INIT();
  const auto t{to_time_t()};
  const auto r{std::gmtime(&t)};
  CHECK(r != nullptr, core::utils::get_os_error_string());
  return *r;
}

bool date::is_leap_year(const short year) {
  TRACE_INIT();
  return (year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0);
}

// statics
const char *const date::__UTC{"UTC"};
}  // namespace core::datetime
