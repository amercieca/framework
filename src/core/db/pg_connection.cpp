// C++
#include <functional>
#include <memory>
#include <sstream>

#include <core/db/pg_connection.h>

#define CHECK_FIELD_INDEX(r, fi)                              \
  CHECK_T(std::range_error, (fi) >= 0 && fi < PQnfields((r)), \
          "invalid field-index parameter")

#define CHECK_INITIALIZED(c)                                                 \
  {                                                                          \
    CHECK((c) != nullptr, "connection is not initialized (not connected?)"); \
    CHECK(PQstatus((c)) == CONNECTION_OK, PQerrorMessage((c)));              \
  }

#define CHECK_RESULT_STATUS(r, s) \
  CHECK(PQresultStatus((r)) == ((s)), PQresultErrorMessage((r)))

#define EXEC_CMD(c, q)                                   \
  [&]() {                                                \
    pg_result res{PQexec((c), (q))};                     \
    CHECK(PQresultStatus(res.get()) == PGRES_COMMAND_OK, \
          PQresultErrorMessage((res.get())));            \
  }()

#define EXEC_CMD_UC(c, q)                                \
  [&]() {                                                \
    pg_result res{PQexec((c), (q))};                     \
    CHECK(PQresultStatus(res.get()) == PGRES_COMMAND_OK, \
          PQresultErrorMessage((res.get())));            \
    return result_update_count(res.get());               \
  }()

// use this function to get the error number (if ever needed)
// PQresultErrorField(res, PG_DIAG_SQLSTATE);

namespace core::db {
///////////////////////////////////////////////////////////////////////////////
// class pg_result
pg_connection::pg_result::pg_result(PGresult* res) : _res(res) {
  CHECK_T(std::invalid_argument, _res != nullptr, "invalid res parameter");
  TRACE_INIT();
}

pg_connection::pg_result::pg_result(pg_result&& o) : _res(o._res) {
  TRACE_INIT();
  o._res = nullptr;
}
pg_connection::pg_result& pg_connection::pg_result::operator=(pg_result&& o) {
  TRACE_INIT();
  if (this != &o) {
    _res = o._res;
    o._res = nullptr;
  }
  return *this;
}

pg_connection::pg_result::~pg_result() {
  TRACE_INIT();
  if (_res != nullptr) {
    PQclear(_res);
    _res = nullptr;
  }
}

PGresult* pg_connection::pg_result::get() const {
  TRACE_INIT();
  return _res;
}

PGresult* pg_connection::pg_result::release() {
  TRACE_INIT();
  PGresult* res = _res;
  _res = nullptr;
  return res;
}

///////////////////////////////////////////////////////////////////////////////
// class pg_connection::r_set
pg_connection::r_set::r_set(const pg_connection& conn, PGresult* res)
    : _conn{conn}, _res{res} {
  TRACE_INIT();
}

pg_connection::r_set::~r_set() {
  TRACE_INIT();
  clear_result();
}

field_type pg_connection::r_set::get_field_type(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  switch (static_cast<fp_type>(PQftype(_res, fi))) {
    case fp_type::INT2:
      return field_type::SHORT;

    case fp_type::INT4:
      return field_type::INT;

    case fp_type::INT8:
      return field_type::LONG;

    case fp_type::TEXT:
    case fp_type::VARCHAR:
      return field_type::STRING;
      return field_type::STRING;

    case fp_type::FLOAT8:
    case fp_type::NUMERIC:
      return field_type::DOUBLE;

    case fp_type::FLOAT4:
      return field_type::FLOAT;

    case fp_type::BOOL:
      return field_type::BOOL;

    case fp_type::DATE:
      return field_type::DATE;

    case fp_type::TIME:
    case fp_type::TIMETZ:
    case fp_type::TIMESTAMP:
    case fp_type::TIMESTAMPTZ:
      return field_type::TIMESTAMP;

    case fp_type::BYTEA:
      return field_type::BYTE_ARRAY;

    case fp_type::CHAR:
    case fp_type::BPCHAR:
      return field_type::CHAR;

    default:
      CHECK(false, core::utils::build_string(
                       "unhandled fp_type value encountered for field @ [", fi,
                       "] encountered"));
      throw 0;  // only here to mute the compiler warning
  }
}

field_type pg_connection::r_set::get_field_type(const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_field_type(get_field_no(fname));
}

std::optional<bool> pg_connection::r_set::get_bool(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(oid, fp_type::BOOL), "field @ [", fi,
        "] is not of type (bool)");
  return str_to_opt_bool(get_field_str(fi), 't');
}

std::optional<bool> pg_connection::r_set::get_bool(const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_bool(get_field_no(fname));
}

std::optional<int> pg_connection::r_set::get_int(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(oid, {fp_type::INT4, fp_type::INT8}),
        "field @ [", fi, "] is not of type (int)");
  return str_to_opt_int(get_field_str(fi));
}

std::optional<int> pg_connection::r_set::get_int(const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_int(get_field_no(fname));
}

std::optional<long> pg_connection::r_set::get_long(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(oid, fp_type::INT8), "field @ [", fi,
        "] is not of type (long)");
  return str_to_opt_long(get_field_str(fi));
}

std::optional<long> pg_connection::r_set::get_long(const char* fname) const {
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_long(get_field_no(fname));
}

std::optional<float> pg_connection::r_set::get_float(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(
            oid, {fp_type::FLOAT4, fp_type::FLOAT8, fp_type::NUMERIC}),
        "field @ [", fi, "] is not of type (float)");
  return str_to_opt_float(get_field_str(fi));
}

std::optional<float> pg_connection::r_set::get_float(const char* fname) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_float(get_field_no(fname));
}

std::optional<double> pg_connection::r_set::get_double(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(
            oid, {fp_type::FLOAT4, fp_type::FLOAT8, fp_type::NUMERIC}),
        "field @ [", fi, "] is not of type (double)");
  return str_to_opt_double(get_field_str(fi));
}

std::optional<double> pg_connection::r_set::get_double(
    const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_double(get_field_no(fname));
}

std::optional<std::string> pg_connection::r_set::get_string(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(
            oid, {fp_type::BPCHAR, fp_type::VARCHAR, fp_type::TEXT}),
        "field @ [", fi, "] is not of type (string)");
  return str_to_opt_string(get_field_str(fi));
}

std::optional<std::string> pg_connection::r_set::get_string(
    const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_string(get_field_no(fname));
}

std::optional<char> pg_connection::r_set::get_char(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(oid, {fp_type::CHAR}), "field @ [", fi,
        "] is not of type (char)");
  return str_to_opt_char(get_field_str(fi));
}

std::optional<char> pg_connection::r_set::get_char(const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_char(get_field_no(fname));
}

std::optional<core::datetime::date> pg_connection::r_set::get_date(
    int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(oid, fp_type::DATE), "field @ [", fi,
        "] is not of type (date)");
  return str_to_opt_date(get_field_str(fi));
}

std::optional<core::datetime::date> pg_connection::r_set::get_date(
    const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_date(get_field_no(fname));
}

std::optional<std::tm> pg_connection::r_set::get_timestamp(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(
            oid, {fp_type::DATE, fp_type::TIME, fp_type::TIMETZ,
                  fp_type::TIMESTAMP, fp_type::TIMESTAMPTZ}),
        "field @ [", fi, "] is not of type (timestamp)");
  return str_to_opt_timestamp(get_field_str(fi));
}

std::optional<std::tm> pg_connection::r_set::get_timestamp(
    const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_timestamp(get_field_no(fname));
}

core::utils::byte_array pg_connection::r_set::get_bytes(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const auto oid{PQftype(_res, fi)};
  CHECK(pg_connection::is_of_type(oid, fp_type::BYTEA), "field @ [", fi,
        "] is not of type (byte_array)");
  return hex_string_to_byte_array(get_field_str(fi));
}

core::utils::byte_array pg_connection::r_set::get_bytes(
    const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  return get_bytes(get_field_no(fname));
}

int pg_connection::r_set::get_field_no(const char* fname) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(fname),
          "invalid fname parameter");
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  const int fi{PQfnumber(_res, fname)};
  CHECK_T(std::invalid_argument, fi != -1, "no such field: ", fname);
  return fi;
}

const char* pg_connection::r_set::get_field_str(int fi) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  CHECK_FIELD_INDEX(_res, fi);
  const int row_index{get_row_index()};
  CHECK(row_index > -1 && row_index < PQntuples(_res),
        "invalid value returned from get_row_index");
  return PQgetisnull(_res, row_index, fi) == 0 ? PQgetvalue(_res, row_index, fi)
                                               : nullptr;
}

void pg_connection::r_set::clear_result() const {
  TRACE_INIT();
  if (_res != nullptr) {
    PQclear(_res);
    _res = nullptr;
  }
}

std::uint16_t pg_connection::r_set::num_fields() const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  return PQnfields(_res);
}

///////////////////////////////////////////////////////////////////////////////
// class pg_connection::dr_set
pg_connection::dr_set::dr_set(const pg_connection& conn, PGresult* res)
    : r_set{conn, res},
      _index{-1},
      _row_count{PQntuples(res)} {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, res != nullptr, "invalid res parameter");
}

bool pg_connection::dr_set::next() const {
  TRACE_INIT();
  return ++_index < _row_count;
}

inline int pg_connection::dr_set::get_row_index() const {
  TRACE_INIT();
  return _index;
}

///////////////////////////////////////////////////////////////////////////////
// class pg_connection::pr_set
pg_connection::pr_set::pr_set(const pg_connection& conn) : r_set{conn} {
  // immediately switch to SingleRowMode
  TRACE_INIT();
  CHECK(conn.enter_single_row_mode(),
        "Connection already executing SingleRowMode query");
}

pg_connection::pr_set::~pr_set() {
  TRACE_INIT();
  // cannot throw in destructor; better to disconnect the underlying
  // connection if this happens (which is extremely unlikely)
  if (!_conn.leave_single_row_mode()) {
    LOG_SEVERE("Error resetting Connection SingleRowMode; disconnecting...");
    // disconnnect the connection
    try {
      ((core::db::connection&)_conn).disconnect();
    } catch (const std::exception& ex) {
      LOG_SEVERE(
          core::utils::build_string(
              "exception encountered disconnecting from database server: ",
              ex.what())
              .c_str());
    } catch (...) {
      LOG_SEVERE("exception encountered disconnecting from database server");
    }
  }
}

bool pg_connection::pr_set::next() const {
  TRACE_INIT();
  clear_result();
  _res = PQgetResult(_conn._pgconn);
  if (_res == nullptr) {
    return false;
  }
  const auto status = PQresultStatus(_res);
  if (status == PGRES_TUPLES_OK) {
    // according to the documentation:
    // https://www.postgresql.org/docs/9.2/libpq-single-row-mode.html
    // "After the last row, or immediately if the query returns zero rows, a
    // zero-row object with status PGRES_TUPLES_OK is returned; this is the
    // signal that no more rows will arrive."
    clear_result();
    return false;
  }
  CHECK_RESULT_STATUS(_res, PGRES_SINGLE_TUPLE);
  return true;
}

inline int pg_connection::pr_set::get_row_index() const {
  TRACE_INIT();
  // in single-row-mode, we will always have one row (tuple);
  // so we always return it's index (which would be 0)
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// class pg_statement_handle
pg_connection::pg_statement_handle::pg_statement_handle(
    const pg_connection& conn, const std::string& md5)
    : _conn{conn},
      _md5{[&]() {
        CHECK_T(std::invalid_argument, !md5.empty(), "invalid md5 parameter");
        return md5;
      }()},
      _res{PQdescribePrepared(_conn._pgconn, _md5.c_str())} {
  TRACE_INIT();
  CHECK(_res != nullptr, "PQdescribePrepared return null");
  CHECK_RESULT_STATUS(_res, PGRES_COMMAND_OK);
}

pg_connection::pg_statement_handle::~pg_statement_handle() {
  TRACE_INIT();
  close();
}

std::size_t pg_connection::pg_statement_handle::param_count() const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  return static_cast<std::size_t>(PQnparams(_res));
}

pg_connection::pg_result pg_connection::pg_statement_handle::execute(
    const std::initializer_list<param>& params) const {
  TRACE_INIT();
  CHECK(_res != nullptr, "_res is uninitialized (null)");
  const std::size_t pcnt{params.size()};
  std::vector<std::string> values{};
  std::vector<const char*> param_values{};
  values.reserve(pcnt);
  param_values.reserve(pcnt);
  std::variant<Oid> p_oid;
  std::variant<int> p_num;
  std::size_t i{0};
  for (const auto& p : params) {
    const Oid oid{PQparamtype(_res, i)};
    CHECK(oid != 0U, "PQparamtype returned 0");
    p_oid = oid;
    p_num = static_cast<int>(i);
    // put the values in 'values' so that they remain available (in scope)
    // and in 'param_values' to pass a C style const char** array to the
    // PQexecPrepared function below
    if (p.is_null()) {
      param_values.emplace_back(nullptr);
    } else {
      param_values.emplace_back(
          values.emplace_back(std::visit(*this, p.value(), p_oid, p_num))
              .c_str());
    }
    ++i;
  }
  CHECK(!_md5.empty(), "_md5 is not set");

  pg_result r{PQexecPrepared(
      _conn._pgconn, _md5.c_str(), pcnt,
      param_values.empty() ? nullptr : std::addressof(*param_values.begin()),
      nullptr, nullptr, 0)};
  const auto rs{PQresultStatus(r.get())};
  CHECK(rs == PGRES_COMMAND_OK || rs == PGRES_TUPLES_OK ||
            rs == PGRES_SINGLE_TUPLE,
        PQresultErrorMessage(r.get()));
  return r;
}

// the null value (only here to satisfy the compiler)
std::string pg_connection::pg_statement_handle::operator()(const param::null&,
                                                           const Oid&,
                                                           const int&) const {
  TRACE_INIT();
  CHECK(false, "operator should never be invoked");
  throw 0;  // only here to mute the compiler warning
}

std::string pg_connection::pg_statement_handle::operator()(
    const bool& v, const Oid& oid, const int& pnum) const {
  static std::string t{"t"};
  static std::string f{"f"};
  //
  TRACE_INIT();
  CHECK_T(std::invalid_argument, pg_connection::is_of_type(oid, fp_type::BOOL),
          "invalid value-type (bool) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return v ? t : f;
}

std::string pg_connection::pg_statement_handle::operator()(
    const short& v, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, pg_connection::is_of_type(oid, fp_type::INT2),
          "invalid value-type (short) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return std::to_string(v);
}

std::string pg_connection::pg_statement_handle::operator()(
    const int& v, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument,
          pg_connection::is_of_type(oid, {fp_type::INT4, fp_type::INT8}),
          "invalid value-type (int) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return std::to_string(v);
}

std::string pg_connection::pg_statement_handle::operator()(
    const long& v, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, pg_connection::is_of_type(oid, fp_type::INT8),
          "invalid value-type (long) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return std::to_string(v);
}

std::string pg_connection::pg_statement_handle::operator()(
    const long long& v, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, pg_connection::is_of_type(oid, fp_type::INT8),
          "invalid value-type (long) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return std::to_string(v);
}

std::string pg_connection::pg_statement_handle::operator()(
    const float& v, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument,
          pg_connection::is_of_type(oid, {fp_type::FLOAT4, fp_type::NUMERIC}),
          "invalid value-type (float) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return fp_to_string(v);
}

std::string pg_connection::pg_statement_handle::operator()(
    const double& v, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument,
          pg_connection::is_of_type(oid, {fp_type::FLOAT8, fp_type::NUMERIC}),
          "invalid value-type (double) for parameter: ", pnum, " [oid: ", oid,
          ']');

  return fp_to_string(v);
}

std::string pg_connection::pg_statement_handle::operator()(
    const char& v, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument,
          pg_connection::is_of_type(oid, {fp_type::CHAR, fp_type::BPCHAR}),
          "invalid value-type (char) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return std::string{v};
}

std::string pg_connection::pg_statement_handle::operator()(
    const std::string& v, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument,
          pg_connection::is_of_type(
              oid, {fp_type::BPCHAR, fp_type::VARCHAR, fp_type::TEXT}),
          "invalid value-type (string) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return v;
}

std::string pg_connection::pg_statement_handle::operator()(
    const core::utils::byte_array& ba, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, pg_connection::is_of_type(oid, fp_type::BYTEA),
          "invalid value-type (byte_array) for parameter: ", pnum,
          " [oid: ", oid, ']');
  return pg_connection::byte_array_to_hex_string(ba);
}

std::string pg_connection::pg_statement_handle::operator()(
    const core::datetime::date& d, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, pg_connection::is_of_type(oid, fp_type::DATE),
          "invalid value-type (date) for parameter: ", pnum, " [oid: ", oid,
          ']');
  return d.to_string();
}

std::string pg_connection::pg_statement_handle::operator()(
    const std::tm& t, const Oid& oid, const int& pnum) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument,
          pg_connection::is_of_type(
              oid, {fp_type::DATE, fp_type::TIME, fp_type::TIMETZ,
                    fp_type::TIMESTAMP, fp_type::TIMESTAMPTZ}),
          "invalid value-type (struct tm) for parameter: ", pnum,
          " [oid: ", oid, ']');
  std::string str(36, 0);
  const std::size_t sz(strftime(
      str.data(), str.length() - 1,
      is_of_type(oid, fp_type::DATE) ? "%Y-%m-%d" : "%Y-%m-%d %H:%M:%S", &t));
  str.resize(sz);
  return str;
}

void pg_connection::pg_statement_handle::close() {
  TRACE_INIT();
  if (_res != nullptr) {
    PQclear(_res);
    _res = nullptr;
  }
}

///////////////////////////////////////////////////////////////////////////////
// class pg_connection
pg_connection::pg_connection(const rapidjson::Value& json)
    : connection{json, connection::db_type::PgSQL}, _pgconn{nullptr} {
  TRACE_INIT();
}

pg_connection::~pg_connection() {
  TRACE_INIT();
  try {
    disconnect();
  } catch (const std::exception& ex) {
    LOG_SEVERE(
        core::utils::build_string(
            "exception encountered in mysql_connection dtor: ", ex.what())
            .c_str());
  } catch (...) {
    LOG_SEVERE("uknown exception encountered in mysql_connection dtor");
  }
}

bool pg_connection::active() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  return _pgconn != nullptr && ping();
}

bool pg_connection::ping() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  pg_result r{PQexec(_pgconn, "select 1")};
  return PQstatus(_pgconn) == CONNECTION_OK;
}

void pg_connection::_start_transaction() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  EXEC_CMD(_pgconn, "begin transaction");
}

void pg_connection::_commit() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  EXEC_CMD(_pgconn, "commit");
}

void pg_connection::_rollback() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  EXEC_CMD(_pgconn, "rollback");
}

std::string pg_connection::escape_string(const std::string& sql) const {
  TRACE_INIT();
  if (sql.empty()) {
    return sql;
  }
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  const std::size_t sz{(sql.length() * 2) + 1};
  std::string res{};
  res.reserve(sz);
  res.resize(sz, 0);
  int error{-1};
  const std::size_t len{PQescapeStringConn(_pgconn, res.data(), sql.c_str(),
                                           sql.length(), &error)};
  CHECK(error == 0, PQerrorMessage(_pgconn));
  res.resize(len);
  return res;
}

std::string pg_connection::byte_array_to_str(
    const core::utils::byte_array& ba) const {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !ba.is_null(), "invalid ba argument (null)");
  // we do not need to lock or check if _pgconn is initialized
  // as byte_array_to_hex_string does not use _pgconn and works on
  // local variables
  return byte_array_to_hex_string(ba);
}

bool pg_connection::enter_single_row_mode() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  bool ex{false};
  if (_in_single_row_mode.compare_exchange_strong(ex, true)) {
    // try setting Postgres connection
    if (PQsetSingleRowMode(_pgconn) != 1) {
      // reset the flag if that (PQsetSingleRowMode) failed
      _in_single_row_mode.exchange(false);
      CHECK(false, PQerrorMessage(_pgconn));
    }
    return true;
  }
  return false;
}

bool pg_connection::leave_single_row_mode() const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  bool ex{true};
  return _in_single_row_mode.compare_exchange_strong(ex, false);
}

void pg_connection::_connect() {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  _pgconn = PQconnectdb(conn_str().c_str());
  CHECK_INITIALIZED(_pgconn);
  // set the transaction isolation level
  std::string cmd{core::utils::build_string(
      "SET SESSION CHARACTERISTICS AS TRANSACTION ISOLATION LEVEL ",
      transaction_isolation_level_to_string(trans_isolation_level()),
      " READ WRITE")};
  EXEC_CMD(_pgconn, cmd.c_str());
}

void pg_connection::_disconnect() noexcept {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  if (_pgconn != nullptr) {
    PQfinish(_pgconn);
    _pgconn = nullptr;
  }
}

void pg_connection::_preprocess_insert_statement(
    std::string& sql, const std::string& key_field_name) const {
  TRACE_INIT();
  if (key_field_name.empty()) {
    return;
  }
  CHECK_T(std::invalid_argument, !sql.empty(), "invalid sql parameter (empty)");
  // remove any trailing ';' characters if present
  const auto p{sql.find_last_of(';')};
  if (p != sql.npos) {
    sql.erase(p);
  }
  sql.append(" RETURNING ").append(key_field_name);
}

std::unique_ptr<rowset> pg_connection::_exec_select(
    const statement_handle& s,
    const std::initializer_list<param>& params) const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  const auto& stmt{dynamic_cast<const pg_statement_handle&>(s)};
  CHECK(stmt.param_count() == params.size(),
        "incorrect number of params specified");
  pg_result res{stmt.execute(params)};
  CHECK_RESULT_STATUS(res.get(), PGRES_TUPLES_OK);
  return std::make_unique<dr_set>(*this, res.release());
}

result pg_connection::_exec_insert(const statement_handle& s,
                                   const std::initializer_list<param>& params,
                                   const std::string& key_field_name) const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  const auto& stmt{dynamic_cast<const pg_statement_handle&>(s)};
  std::vector<std::size_t> generated_keys{};
  pg_result res{stmt.execute(params)};
  CHECK_RESULT_STATUS(
      res.get(), key_field_name.empty() ? PGRES_COMMAND_OK : PGRES_TUPLES_OK);
  std::size_t update_count{0UL};
  if (!key_field_name.empty()) {
    const int uc{PQntuples(res.get())};
    CHECK(uc > 0, "PQntuples returned (< 1) for insert statement");
    update_count = static_cast<std::size_t>(uc);
    generated_keys.reserve(update_count);
    for (std::size_t i = 0; i < update_count; ++i) {
      generated_keys.emplace_back(std::stoi(PQgetvalue(res.get(), i, 0)));
    }
  } else {
    update_count = static_cast<std::size_t>(result_update_count(res.get()));
    CHECK(update_count > 0,
          "result_update_count returned (< 1) for insert statement");
  }
  return result{update_count, generated_keys};
}

std::size_t pg_connection::_exec_update(
    const statement_handle& s,
    const std::initializer_list<param>& params) const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  const auto& stmt{dynamic_cast<const pg_statement_handle&>(s)};
  pg_result res{stmt.execute(params)};
  CHECK_RESULT_STATUS(res.get(), PGRES_COMMAND_OK);
  return static_cast<std::size_t>(result_update_count(res.get()));
}

std::unique_ptr<connection::statement_handle>
pg_connection::_create_statement_handle(const std::string& sql,
                                        const std::string& md5) const {
  TRACE_INIT();
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  // we don't have to check the sql and md5 parameters;
  // they are checked at the base class level
  pg_result res{PQprepare(_pgconn, md5.c_str(), sql.c_str(), 0, nullptr)};
  CHECK_RESULT_STATUS(res.get(), PGRES_COMMAND_OK);
  return std::make_unique<pg_statement_handle>(*this, md5);
}

std::unique_ptr<rowset> pg_connection::_exec_select_direct(
    const std::string& sql) const {
  TRACE_INIT();
  // we don't have to check the sql parameter;
  // it is checked at the base class level
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  pg_result res{PQexec(_pgconn, sql.c_str())};
  CHECK_RESULT_STATUS(res.get(), PGRES_TUPLES_OK);
  return std::make_unique<dr_set>(*this, res.release());
}

result pg_connection::_exec_insert_direct(
    const std::string& sql, const std::string& key_field_name) const {
  TRACE_INIT();
  // we don't have to check the sql parameter;
  // it is checked at the base class level
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  std::size_t update_count{0UL};
  std::vector<std::size_t> generated_keys{};
  if (!key_field_name.empty()) {
    std::string dsql(sql);
    // remove any trailing ';' characters if present
    const auto p{dsql.find_last_of(';')};
    if (p != dsql.npos) {
      dsql.erase(p);
    }
    dsql.append(" RETURNING ").append(key_field_name);
    pg_result res{PQexec(_pgconn, dsql.c_str())};
    CHECK_RESULT_STATUS(res.get(), PGRES_TUPLES_OK);
    update_count = static_cast<std::size_t>(result_update_count(res.get()));
    CHECK(update_count > 0,
          "result_update_count returned (< 1) for insert statement");
    generated_keys.reserve(update_count);
    for (std::size_t i = 0; i < update_count; ++i) {
      generated_keys.emplace_back(std::stoi(PQgetvalue(res.get(), i, 0)));
    }
  } else {
    update_count = EXEC_CMD_UC(_pgconn, sql.c_str());
    CHECK(update_count > 0,
          "result_update_count returned (< 1) for insert statement");
  }
  return result{update_count, generated_keys};
}

std::size_t pg_connection::_exec_update_direct(const std::string& sql) const {
  TRACE_INIT();
  // we don't have to check the sql parameter;
  // it is checked at the base class level
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  CHECK_INITIALIZED(_pgconn);
  return EXEC_CMD_UC(_pgconn, sql.c_str());
}

const std::string& pg_connection::conn_str() const {
  TRACE_INIT();
  if (_conn_str.empty()) {
    const std::string sslmode{
        conf().HasMember("sslmode") ? conf()["sslmode"].GetString() : ""};
    _conn_str = fmt::format(
        "host={:s} port={:d} user={:s} password={:s} dbname={:s} sslmode={:s}",
        host(), port(), user(), password(), database(),
        sslmode.empty() ? "disable" : sslmode);
  }
  return _conn_str;
}

int pg_connection::result_update_count(PGresult* res) {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, res != nullptr, "invalid res argument");
  std::string update_count{PQcmdTuples(res)};
  const int uc{update_count.empty() ? 0 : std::stoi(update_count)};
  CHECK(uc >= 0, "PQcmdTuples(res) returned (< 0)");
  return uc;
}

core::utils::byte_array pg_connection::hex_string_to_byte_array(
    const std::string& str) {
  TRACE_INIT();
  if (str.empty()) {
    return core::utils::byte_array{};
  }
  const std::size_t len = str.length();
  const std::size_t size = (str.length() - 2) >> 1;
  core::utils::byte_array res{size};
  char* d = (char*)res.get();
  for (std::size_t i = 2; i < len; i += 2) {
    *d++ = ((((strchr(__hex_chars, str[i]) - __hex_chars) << 0x4) & 0xF0) |
            ((strchr(__hex_chars, str[i + 1]) - __hex_chars) & 0x0F));
  }
  return res;
}

std::string pg_connection::byte_array_to_hex_string(
    const core::utils::byte_array& ba) {
  TRACE_INIT();
  if (ba.is_null()) {
    return core::globals::empty_string;
  }
  const std::size_t size = ba.length();
  const std::size_t sz = 2 + (size * 2);
  const char* data = ba.get();
  std::string str;
  str.reserve(sz);
  str.resize(sz);
  str[0] = '\\';
  str[1] = 'x';
  char* d = str.data() + 2;
  for (std::size_t i = 0; i < size; ++i) {
    *d++ = __hex_chars[(data[i] & 0xF0) >> 0x4];
    *d++ = __hex_chars[data[i] & 0x0F];
  }
  return str;
}

bool pg_connection::is_of_type(const Oid& oid,
                               const pg_connection::fp_type& type) {
  TRACE_INIT();
  return oid == static_cast<Oid>(type);
}

bool pg_connection::is_of_type(
    const Oid& oid,
    const std::initializer_list<pg_connection::fp_type>& types) {
  TRACE_INIT();
  for (const auto& t : types) {
    if (static_cast<Oid>(t) == oid) {
      return true;
    }
  }

  return false;
}

// statics
const char pg_connection::__hex_chars[] = "0123456789abcdef";

}  // namespace core::db