// C++
#include <ctime>
#include <fstream>
#include <future>
#include <regex>

// Poco
#include <Poco/AsyncChannel.h>
#include <Poco/Crypto/Cipher.h>
#include <Poco/Crypto/CipherFactory.h>
#include <Poco/Crypto/CipherKey.h>
#include <Poco/Crypto/CipherKeyImpl.h>
#include <Poco/Crypto/Crypto.h>
#include <Poco/Crypto/DigestEngine.h>
#include <Poco/DigestStream.h>
#include <Poco/FileChannel.h>
#include <Poco/FormattingChannel.h>
#include <Poco/HMACEngine.h>
#include <Poco/Net/HTMLForm.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/NetException.h>
#include <Poco/Net/PartHandler.h>
#include <Poco/PatternFormatter.h>
#include <Poco/StreamCopier.h>
#include <Poco/Util/Application.h>

// OpenSSL
#include <openssl/conf.h>
#include <openssl/crypto.h>
#include <openssl/engine.h>
#include <openssl/err.h>
#include <openssl/ssl.h>

// project/local
#include <core/app/serverapp.h>
#include <core/auth/auth.h>
#include <core/http/result.h>
#include <core/utils/base64_url.h>
#include <core/utils/md5.h>
#include <core/utils/utils.h>

namespace core::app {

/////////////////////////////////////////////////////////////////////////////
// request_logger:
// (private) class that logs each request to the db and files
// (and also provides extraction of form data, acting as part-handler)
class request_logger final : public Poco::Net::PartHandler {
 public:
  request_logger() = delete;

  request_logger(const request_logger &) = delete;

  request_logger(request_logger &&) = delete;

  request_logger &operator=(const request_logger &) = delete;

  request_logger &operator=(request_logger &&) = delete;

  request_logger(const std::string &request_id,
                 const Poco::Net::HTTPServerRequest &request,
                 const Poco::Net::HTTPServerResponse &response,
                 const std::int32_t route_id, const std::string &route_path)
      : _request_id{request_id},
        _request{request},
        _response{response},
        _route_id{route_id},
        _route_path{route_path} {
    TRACE_INIT();
    //
    CHECK(route_id >= 0, "invalid route_id parameter");
    CHECK(!route_path.empty(), "invalid route_path parameter (blank/empty)");
  }

  ~request_logger() noexcept {
    TRACE_INIT();
    //
    // log this request + response data to the database
    try {  // we catch everything here as we cannot have an exception be thrown
      // in a destructor
      std::async(std::launch::async, log, _request_id, _instant, _route_id,
                 duration(), _response.getStatus(), _response.getReason(),
                 // compile request headers
                 [this]() {
                   std::map<std::string, std::string> headers{};
                   for (const auto &[k, v] : _request) {
                     headers.insert(std::make_pair(k, v));
                   }
                   return headers;
                 }(),
                 // compile params
                 [this]() {
                   std::map<std::string, std::string> params{};
                   if (_form != nullptr) {
                     for (const auto &[k, v] : *_form) {
                       params.insert(std::make_pair(k, v));
                     }
                   }
                   return params;
                 }(),
                 // response headers
                 [this]() {
                   std::map<std::string, std::string> headers{};
                   for (const auto &[k, v] : _response) {
                     headers.insert(std::make_pair(k, v));
                   }
                   return headers;
                 }(),
                 // file data list
                 std::move(_file_data_list));
    } catch (const std::exception &ex) {
      LOG_SEVERE(ex.what());
    } catch (...) {
      LOG_SEVERE("Exception encountered logging request data to database");
    }
  }

  void init(const bool do_file_uploads,
            const std::uint32_t file_upload_size_limit) const {
    //
    TRACE_INIT();
    //
    const std::string method{_request.getMethod()};
    auto &request{(Poco::Net::HTTPServerRequest &)_request};
    // set the _file_upload_size_limit data member
    //(which will be used (if applicable) in the handlePart function)
    _file_upload_size_limit = file_upload_size_limit;

    _form.reset(nullptr);
    // GET request
    if (method == Poco::Net::HTTPRequest::HTTP_GET) {
      // no files to handle in a GET request
      _form.reset(new Poco::Net::HTMLForm{request});
    } else {
      // POST or PUT request
      CHECK((method == Poco::Net::HTTPRequest::HTTP_POST ||
             method == Poco::Net::HTTPRequest::HTTP_PUT),
            fmt::format("Unhandled HTTP Method encountered: {:s}", method));
      if (!do_file_uploads) {
        // discard any file uploads if they are not required
        _form.reset(new Poco::Net::HTMLForm{request, request.stream()});
      } else {
        // otherwise, read the file data, save the uploaded files (if any) and
        // update file_data_list in the process (so the caller will have the
        // necessary file upload information when handling the request)
        _form.reset(new Poco::Net::HTMLForm{request, request.stream(),
                                            (Poco::Net::PartHandler &)*this});
      }
    }
    // if the form is empty, then set it to null
    if (_form.get() != nullptr && _form->empty()) {
      _form.reset(nullptr);
    }
  }

  // parameters to this function are passed by value (or moved) as this
  // function is invoked via std::async and hence all values have to
  // be available during this call whenever it happens
  static void log(const std::string request_id, const std::int64_t instant,
                  const std::int32_t route_id, const std::int64_t duration,
                  const Poco::Net::HTTPResponse::HTTPStatus status,
                  const std::string reason,
                  std::map<std::string, std::string> &&req_headers,
                  std::map<std::string, std::string> &&params,
                  std::map<std::string, std::string> &&resp_headers,
                  std::list<core::http::route::file_data> &&file_data_list) {
    //
    static std::regex _obfs_rx_{
        Poco::Util::Application::instance().config().getString(
            "security.obfuscation_variable_regex"),
        std::regex_constants::icase};
    //
    TRACE_INIT();
    try {
      //
      // it is also quite important here that no exceptions are thrown,
      // as this function is invoked asynchronously
      //
      if (request_id.empty()) {
        LOG_SEVERE("request_id parameter is blank/empty");
        // we cannot log an entry with a blank request_id, so we exit the
        // function
        return;
      }
      // write warnings to the logs if some values are missing/incorrect
      if (reason.empty()) {
        LOG_WARN(fmt::format("reason parameter for request_id: {:s} is empty",
                             request_id));
      }
      if (req_headers.empty()) {
        LOG_WARN(fmt::format(
            "req_headers parameter for request_id: {:s} is empty", request_id));
      }
      if (resp_headers.empty()) {
        LOG_WARN(
            fmt::format("resp_headers parameter for request_id: {:s} is empty",
                        request_id));
      }
      //
      {
        // sanitize/mask any sensitive (such as passwords) data
        for (auto &[k, v] : req_headers) {
          if (std::regex_match(k, _obfs_rx_)) {
            v.assign(v.length(), '*');
          }
        }
        for (auto &[k, v] : resp_headers) {
          if (std::regex_match(k, _obfs_rx_)) {
            v.assign(v.length(), '*');
          }
        }
        for (auto &[k, v] : params) {
          if (std::regex_match(k, _obfs_rx_)) {
            v.assign(v.length(), '*');
          }
        }
      }
      LOG_DEBUG(fmt::format("Logging data for request: {:s}...", request_id));
      core::app::web_server_app::get_db_conn("CoreDB").exec<void>(
          [&](core::db::connection &conn, bool &) {
            // write the request record
            conn.exec_insert(
                "insert into request.request(id, instant, route_id, duration, "
                "status_code, status_msg) values($1, $2, $3, $4, $5, $6)",
                {request_id,
                 // instant (as an std::tm) in seconds
                 [&]() {
                   std::time_t t{instant / 1000};
                   std::tm ts{};
#ifdef _WIN32
                   gmtime_s(&ts, &t);
#else
                   gmtime_r(&t, &ts);
#endif
                   return ts;
                 }(),
                 route_id, duration, status, reason});

            // log the request headers for this record
            for (const auto &[k, v] : req_headers) {
              conn.exec_insert(
                  "insert into request.request_data(request_id, data_type, "
                  "name, value) values($1, $2, $3, $4)",
                  {request_id, 'H', k, v});  // H == header value
            }
            // dump the request parameters
            for (const auto &[k, v] : params) {
              conn.exec_insert(
                  "insert into request.request_data(request_id, data_type, "
                  "name, value) values($1, $2, $3, $4)",
                  {request_id, 'P', k, v});  // P == param value
            }
            // dump the response headers
            for (const auto &[k, v] : resp_headers) {
              conn.exec_insert(
                  "insert into request.request_data(request_id, data_type, "
                  "name, value) values($1, $2, $3, $4)",
                  {request_id, 'X', k, v});  // X == response value
            }
            // dump the upload file data (and delete the
            // physical files in the process (if not retained))
            for (const auto &f : file_data_list) {
              conn.exec_insert(
                  "insert into request.request_file(request_id, param_name, "
                  "content_type, file_name, file_size) "
                  "values($1, $2, $3, $4, $5)",
                  {request_id, f.param_name, f.content_type, f.file_name,
                   f.file_size});
              //
              if (!f.retain) {
                // delete the physical file (if it's not being retained)
                if (std::remove(f.file_path.c_str()) != 0) {
                  LOG_SEVERE(fmt::format(
                      "Error deleting file [{:s}] for request [{:s}]: {:s}",
                      f.file_path, request_id,
                      core::utils::get_os_error_string()));
                }
              }
            }
          });
      LOG_DEBUG(
          fmt::format("Logging data for request: {:s}; done.", request_id));
    } catch (const std::exception &ex) {
      LOG_SEVERE(
          fmt::format("Exception encountered logging request [{:s}]: {:s}",
                      request_id, ex.what()));
    } catch (...) {
      LOG_SEVERE(fmt::format(
          "Uknown exception encountered logging request: {:s}", request_id));
    }
  }

  int64_t instant() const {
    TRACE_INIT();
    //
    return _instant;
  }

  const std::string &id() const {
    TRACE_INIT();
    //
    return _request_id;
  }

  int32_t route_id() const {
    TRACE_INIT();
    //
    return _route_id;
  }

  const std::string &path() const {
    TRACE_INIT();
    //
    return _route_path;
  }

  const Poco::Net::HTMLForm *form() const {
    TRACE_INIT();
    //
    return _form.get();
  }

  const std::list<core::http::route::file_data> &file_data_list() const {
    TRACE_INIT();
    //
    return _file_data_list;
  }

  int64_t duration() const {
    TRACE_INIT();
    //
    return core::utils::now<std::chrono::milliseconds>().count() - _instant;
  }

 private:
  const std::string _request_id;
  const Poco::Net::HTTPServerRequest &_request;
  const Poco::Net::HTTPServerResponse &_response;
  const std::int32_t _route_id;
  const std::string _route_path;
  const int64_t _instant{core::utils::now<std::chrono::milliseconds>().count()};
  mutable std::uint32_t _file_upload_size_limit{0U};
  mutable std::unique_ptr<Poco::Net::HTMLForm> _form{nullptr};
  mutable std::list<core::http::route::file_data> _file_data_list{};

  static const std::regex __rx;
  static std::mutex __mtx;

  void handlePart(const Poco::Net::MessageHeader &header, std::istream &strm) {
    //
    static const std::string _path_{
        Poco::Util::Application::instance().config().getString(
            "httpd.requests.file_upload_path")};
    static const std::uint32_t _sys_file_upload_limit_{
        static_cast<std::uint32_t>(
            Poco::Util::Application::instance().config().getInt(
                "httpd.requests.file_upload_size_limit_kb"))};
    //
    auto copy_stream = [&](std::istream &istr, std::ostream &ostr,
                           const std::string &param_name,
                           const std::string &file_name) {
      TRACE_INIT();
      //
      // calculate the file upload size limit (system)
      const std::uint32_t file_upload_size_limit{[&]() {
        if (_file_upload_size_limit == 0U) {
          return _sys_file_upload_limit_;
        }
        return std::min(_sys_file_upload_limit_, _file_upload_size_limit);
      }()};
      CHECK(file_upload_size_limit >= 1024,
            "invalid file_upload_size_limit value set to less than 1K");

      char buf[8192];
      std::streamsize len = 0;
      istr.read(buf, sizeof(buf));
      std::streamsize n = istr.gcount();
      while (n > 0) {
        len += n;
        CHECK(static_cast<std::uint32_t>(len) <= file_upload_size_limit,
              fmt::format("file upload size limit exceeded for request "
                          "[{:s}] (param: {:s}; file_name: {:s})",
                          _request_id, param_name, file_name));
        ostr.write(buf, n);
        if (istr && ostr) {
          istr.read(buf, sizeof(buf));
          n = istr.gcount();
        } else
          n = 0;
      }
      return len;
    };
    //
    TRACE_INIT();
    //
    long file_size{0L};
    std::string param_name{};
    std::string file_name{};
    std::string content_type{};
    // extract the above values from the header
    for (const auto &v : header) {
      std::smatch sm;
      if (v.first == "Content-Disposition" && [&]() {
            std::lock_guard<std::mutex> lg{__mtx};
            return std::regex_match(v.second, sm, __rx);
          }()) {
        param_name = sm[1];
        file_name = sm[2];
      } else if (v.first == "Content-Type") {
        content_type = v.second;
      }
    }
    CHECK(!param_name.empty(),
          "could not extract param_name value from file upload header");
    CHECK(!file_name.empty(),
          "could not extract file_name value from file upload header");
    CHECK(!content_type.empty(),
          "could not extract content_type value from file upload header");
    // compute file name (including full path)
    const std::string file_path{fmt::format(
        "{:s}/{:s}-{:s}-{:s}", _path_, _request_id, param_name, file_name)};
    std::ofstream ofs(file_path, std::ios_base::binary);
    CHECK(ofs.good(), fmt::format("Error creating file: {:s}", file_path));
    file_size = copy_stream(strm, ofs, param_name, file_name);
    ofs.flush();
    ofs.close();
    _file_data_list.push_back(core::http::route::file_data{
        param_name, content_type, file_name, file_path, file_size});
  }
};

///////////////////////////////////////////////////////////////////////////////
// class logging
web_server_app::logging::logging() {
  TRACE_INIT();
  //
}

const char *web_server_app::logging::name() const {
  TRACE_INIT();
  //
  return _name.c_str();
}

void web_server_app::logging::initialize(Poco::Util::Application &app) {
  TRACE_INIT();
  //
  const auto &cfg{app.config()};
  ((std::string &)_name) =
      fmt::format("{:s} - logging", cfg.getString("app.name"));
  // formatter
  Poco::AutoPtr<Poco::PatternFormatter> pf(new Poco::PatternFormatter{});
  pf->setProperty("pattern", cfg.getString("logging.pattern"));
  // file channel
  Poco::AutoPtr<Poco::FileChannel> ffc(new Poco::FileChannel{});
  ffc->setProperty("path", cfg.getString("logging.path"));
  ffc->setProperty("rotation", cfg.getString("logging.rotation"));
  ffc->setProperty("archive", cfg.getString("logging.archive"));
  ffc->setProperty("times", cfg.getString("logging.times"));
  ffc->setProperty("compress", cfg.getString("logging.compress"));
  ffc->setProperty("purgeAge", cfg.getString("logging.purge_age"));
  ffc->setProperty("purgeCount", cfg.getString("logging.purge_count"));
  // formatting channel
  Poco::AutoPtr<Poco::FormattingChannel> fc(
      new Poco::FormattingChannel(pf, ffc));
  // async channel (log asynchronously for speed)
  Poco::AutoPtr<Poco::AsyncChannel> ac(new Poco::AsyncChannel(fc));
  Poco::Logger::root().setChannel(ac);
  Poco::Logger::root().setLevel(cfg.getString("logging.level"));
}

void web_server_app::logging::uninitialize() {
  TRACE_INIT();
  //
}

///////////////////////////////////////////////////////////////////////////////
// class db
web_server_app::db::db() {
  TRACE_INIT();
  //
}

const char *web_server_app::db::name() const {
  TRACE_INIT();
  //
  return _name.c_str();
}

void web_server_app::db::initialize(Poco::Util::Application &app) {
  TRACE_INIT();
  //
  std::lock_guard lg{_mtx};
  ((std::string &)_name) =
      fmt::format("{:s} - db", app.config().getString("app.name"));
  try {
    const std::string cfg_str{app.config().getString("db_connections")};
    CHECK(!cfg_str.empty(),
          "db_connections configuration parameter is not empty");
    _conf.SetNull();
    _conf.Parse(cfg_str);
    //
    CHECK(_conf.IsArray(),
          "db: json configuration value (db_connections) is not of Array type");
    for (const auto &i : _conf.GetArray()) {
      CHECK(i.IsObject(), "db_connections value is not of Object type");
      if (!core::utils::get_json_value<bool>(i, "active")) {
        // ignore inactive connections
        continue;
      }
      const std::string name{
          core::utils::get_json_value<std::string>(i, "name")};
      CHECK(_conn_pool_map.find(name) == _conn_pool_map.end(),
            fmt::format("duplicate db connection specified: {:s}", name));
      std::unique_ptr<core::db::db_connection_pool> db_cp{
          new core::db::db_connection_pool(i)};
      db_cp->init();
      _conn_pool_map[name].reset(db_cp.release());
    }
    // check that a 'CoreDB' db connection pool has been specified
    CHECK(_conn_pool_map.find("CoreDB") != _conn_pool_map.end(),
          "no 'CoreDB' db connection specified");
  } catch (const std::exception &ex) {
    _conn_pool_map.clear();
    LOG_SEVERE(ex.what());
    throw;
  } catch (...) {
    _conn_pool_map.clear();
    LOG_SEVERE("Exception encountered initialising DB subsystem");
    throw;
  }
}

void web_server_app::db::uninitialize() {
  TRACE_INIT();
  //
  std::lock_guard lg{_mtx};
  for (auto &[n, c] : _conn_pool_map) {
    LOG_INFO(fmt::format("Disconnecting db: {:s}", n));
    try {
      c->fini();
      c.reset(nullptr);
    } catch (const std::exception &ex) {
      // since we're uninitialising, we just absorb exceptions here
      LOG_SEVERE(ex.what());
    } catch (...) {
      // since we're uninitialising, we just absorb exceptions here
      LOG_SEVERE("Exception encountered initialising DB subsystem");
    }
  }
  _conn_pool_map.clear();
}

core::db::db_connection_pool &web_server_app::db::get_db_conn(
    const char *name) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !core::utils::is_str_null(name),
          "invalid name parameter (null/empty)");
  std::lock_guard lg{_mtx};
  const auto i{_conn_pool_map.find(name)};
  CHECK(i != _conn_pool_map.end(),
        fmt::format("No such db Connection: {:s}", name));
  return *(i->second.get());
}

core::db::db_connection_pool &web_server_app::db::get_db_conn(
    const std::string &name) const {
  TRACE_INIT();
  //
  return get_db_conn(name.c_str());
}

///////////////////////////////////////////////////////////////////////////////
// class web_server::request_handler_factory
web_server_app::web_server::request_handler_factory::request_handler_factory(
    web_server &ws_obj)
    : _ws_obj{ws_obj} {
  TRACE_INIT();
  //
}

inline Poco::Net::HTTPRequestHandler *
web_server_app::web_server::request_handler_factory::createRequestHandler(
    const Poco::Net::HTTPServerRequest &request,
    Poco::Net::HTTPServerRequest::PRIORITY &priority, std::uint32_t &timeout) {
  TRACE_INIT();
  //
  // just forward the request to the web_server instance
  return _ws_obj.create_request_handler(request, priority, timeout);
}

///////////////////////////////////////////////////////////////////////////////
// class web_server::request_handler
web_server_app::web_server::request_handler::request_handler(
    web_server &ws_obj, const bool is_login_request,
    const core::http::route *route)
    : _ws_obj{ws_obj}, _is_login_request{is_login_request}, _route{route} {
  TRACE_INIT();
  //
}

inline void web_server_app::web_server::request_handler::handleRequest(
    Poco::Net::HTTPServerRequest &request,
    Poco::Net::HTTPServerResponse &response) {
  TRACE_INIT();
  //
  // just forward the request to the web_server instance
  _ws_obj.handle_request(request, response, _is_login_request, _route);
}

///////////////////////////////////////////////////////////////////////////////
// class web_server
web_server_app::web_server::web_server(
    const Poco::Util::LayeredConfiguration &config, const auth &auth,
    core::http::route::route_factory_func rff)
    : _server{new request_handler_factory{*this},
              Poco::Net::ServerSocket(config.getInt("httpd.port", 9090)),
              [&]() {  // httpd server parameters
                Poco::AutoPtr<Poco::Net::HTTPServerParams> params{
                    new Poco::Net::HTTPServerParams{}};
                params->setKeepAlive(config.getBool("httpd.keep_alive"));
                params->setKeepAliveTimeout([&config]() {
                  const auto v{config.getInt("httpd.keep_alive_timeout")};
                  CHECK(v >= 0,
                        "Invalid httpd.keep_alive_timeout configuration "
                        "parameter");
                  return v;
                }());
                params->setMaxKeepAliveRequests([&config]() {
                  const auto v{config.getInt("httpd.max_keep_alive_requests")};
                  CHECK(v >= 0,
                        "Invalid 'httpd.max_keep_alive_requests' configuration "
                        "parameter");
                  return v;
                }());
                params->setMaxQueued([&]() {
                  const auto v{config.getInt("httpd.max_queued")};
                  CHECK(v >= 0,
                        "Invalid 'httpd.max_queued' configuration parameter");
                  return v;
                }());
                params->setMaxThreads([&]() {
                  const auto v{config.getInt("httpd.max_threads")};
                  CHECK(v >= 0,
                        "Invalid 'httpd.max_threads' configuration parameter");
                  return v;
                }());
                params->setTimeout([&]() {
                  const auto a{config.getInt("httpd.timeout_secs")};
                  const auto b{config.getInt("httpd.timeout_usecs")};
                  CHECK(a >= 0,
                        "Invalid 'httpd.timeout_secs' configuration parameter");
                  CHECK(
                      b >= 0,
                      "Invalid 'httpd.timeout_usecs' configuration parameter");
                  return Poco::Timespan{a, b};
                }());
                params->setServerName(config.getString("httpd.server_name"));
                return params;
              }()},
      _name{config.getString("httpd.server_name")},
      _login_route_path{config.getString("httpd.requests.login_route_path")},
      _auth{auth},
      _rff{[&rff]() {
        CHECK(rff != nullptr, "invalid rff parameter");
        return rff;
      }()} {
  TRACE_INIT();
  //
}

const char *web_server_app::web_server::name() const {
  TRACE_INIT();
  //
  return _name.c_str();
}

void web_server_app::web_server::initialize(Poco::Util::Application &) {
  TRACE_INIT();
  //
  // load the routes from the database
  core::app::web_server_app::get_db_conn("CoreDB").exec<void>(
      [&](core::db::connection &conn, bool &) {
        core::http::route::load(conn, _rff);
      });
  _server.start();
}

void web_server_app::web_server::uninitialize() {
  TRACE_INIT();
  //
  _server.stopAll();
}

std::string web_server_app::web_server::get_request_path(
    const Poco::Net::HTTPServerRequest &request) const {
  TRACE_INIT();
  //
  std::string uri{request.getURI()};
  if (request.getMethod() == Poco::Net::HTTPRequest::HTTP_GET) {
    const auto i{uri.find_first_of("?")};
    if (i != uri.npos) {
      uri.erase(i);
    }
  }
  return uri;
}

Poco::Net::HTTPRequestHandler *
web_server_app::web_server::create_request_handler(
    const Poco::Net::HTTPServerRequest &request,
    Poco::Net::HTTPServerRequest::PRIORITY &priority, std::uint32_t &timeout) {
  TRACE_INIT();
  //
  if (web_server_app::terminated()) {
    // if the application is being shut down, then return a nullptr,
    // as we don't to be servicing any more requests at this point
    return nullptr;
  }
  //
  // attempt to locate the route from the requests path
  const std::string path{get_request_path(request)};
  const auto route{core::http::route::get_route(path)};
  if (route != nullptr) {
    priority = route->priority();
    // timeout values are in milliseconds
    timeout = route->timeout();
  }
  // always return an instance of request_handler
  //(which will forward calls to this class (web_server))
  return new request_handler{*this, path == _login_route_path, route};
}

void web_server_app::web_server::log_request(
    const std::string &request_id, Poco::Net::HTTPServerRequest &request) {
  //
  static std::regex _obfs_rx_{
      Poco::Util::Application::instance().config().getString(
          "security.obfuscation_variable_regex"),
      std::regex_constants::icase};
  //
  TRACE_INIT();
  //
  std::stringstream sstr{};
  sstr << "request_id: " << request_id << "; ";
  sstr << "method: " << request.getMethod() << "; ";
  sstr << "URI: " << request.getURI() << "; ";
  {
    // sanitize/mask any sensitive (such as passwords) data
    for (const auto &[k, v] : request) {
      if (std::regex_match(v, _obfs_rx_)) {
        sstr << k << ": " << std::string(v.length(), '*') << "; ";
      } else {
        sstr << k << ": " << v << "; ";
      }
    }
  }
  std::string res{sstr.str()};
  auto i{res.find_last_of(";")};
  if (i != res.npos) {
    res.erase(i);
  }
  LOG_INFO(res);
}

void web_server_app::web_server::handle_login_request(
    Poco::Net::HTTPServerRequest &request,
    Poco::Net::HTTPServerResponse &response, const std::string &request_id,
    const Poco::Net::HTMLForm *form) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !request.empty(),
          "invalid request_id parameter");
  //
  if (request.getMethod() != Poco::Net::HTTPRequest::HTTP_POST) {
    core::http::route::send_error_response(
        request, response, Poco::Net::HTTPResponse::HTTP_METHOD_NOT_ALLOWED,
        request_id, "'POST' method required for Login request");
    return;
  }

  if (form == nullptr) {
    core::http::route::send_error_response(
        request, response, Poco::Net::HTTPResponse::HTTP_BAD_REQUEST,
        request_id,
        "form data in 'application/x-www-form-urlencoded' format required.");
    return;
  }

  if (!(form->has("username") && form->has("password"))) {
    core::http::route::send_error_response(
        request, response, Poco::Net::HTTPResponse::HTTP_BAD_REQUEST,
        request_id, "Incomplete credentials supplied");
    return;
  }
  std::optional<core::auth::user_info> ui{
      _auth.authenticate((*form)["username"], (*form)["password"])};
  if (!ui) {
    core::http::route::send_error_response(
        request, response, Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED,
        request_id, "Login Incorrect");
    return;
  }
  //
  const auto jwt{_auth.gen_jwt(*ui)};
  core::http::result result{request_id};
  result.set("jwt", jwt);
  core::http::route::send_ok_response(request, response, result);
}

void web_server_app::web_server::handle_request(
    Poco::Net::HTTPServerRequest &request,
    Poco::Net::HTTPServerResponse &response, const bool is_login_route,
    const core::http::route *route) {
  //
  TRACE_INIT();
  //
  // generate a unique id for this request
  const std::string request_id{core::utils::gen_uuid()};
  try {
    // immediately log the request to the log files
    log_request(request_id, request);

    // initialize the request logger
    const auto [route_id, route_path] = [&]() {
      if (route == nullptr) {
        return std::make_pair(core::http::route::__ROUTE_ID_NOT_FOUND,
                              core::http::route::__ROUTE_NOT_FOUND);
      }
      if (is_login_route) {
        return std::make_pair(core::http::route::__ROUTE_ID_LOGIN,
                              core::http::route::__ROUTE_LOGIN);
      }
      return std::make_pair(route->id(), route->path());
    }();
    //
    const request_logger rl{request_id, request, response, route_id,
                            route_path};
    // read the form data (including file upload data if any)
    rl.init(!is_login_route && route != nullptr && route->has_file_uploads(),
            (route != nullptr && route->has_file_uploads())
                ? route->file_upload_size_limit()
                : 0U);

    // check if it's a 'login' request; if so handle it here
    if (is_login_route) {
      // when extracting the form here, discard file uploads
      handle_login_request(request, response, rl.id(), rl.form());
      return;
    }

    // check if this route exists (i.e. handled);
    // if not, return 404
    if (route == nullptr) {
      core::http::route::send_error_response(
          request, response, Poco::Net::HTTPResponse::HTTP_NOT_FOUND, rl.id());
      return;
    }

    // check if the route supports this HTTP method
    const std::string method{request.getMethod()};
    if (!route->is_method_allowed(method)) {
      //
      core::http::route::send_error_response(
          request, response, Poco::Net::HTTPResponse::HTTP_METHOD_NOT_ALLOWED,
          request_id,
          fmt::format("'{:s}' method not allowed for this route ({:s})", method,
                      route_path));
      return;
    }

    // check if there have been too many request for this route
    // (within its own specified time span)
    if (route->has_too_many_requests()) {
      core::http::route::send_error_response(
          request, response, Poco::Net::HTTPResponse::HTTP_TOO_MANY_REQUESTS,
          rl.id());
      return;
    }

    // handle request (after full credentials check)
    // check user credential (via JWT)
    std::string scheme, jwt;
    try {
      request.getCredentials(scheme, jwt);
    } catch (const Poco::Net::NotAuthenticatedException &ex) {
      // if credentials were not supplied, set require authentication on the
      // the response object and send an unauthorized response
      response.requireAuthentication("Bearer");
      core::http::route::send_error_response(
          request, response, Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED,
          rl.id());
      return;
    }
    //
    // check that the credentials supplied follow the 'Bearer' scheme
    if (scheme != "Bearer") {
      response.requireAuthentication("Bearer");
      core::http::route::send_error_response(
          request, response, Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED,
          rl.id());
      return;
    }

    // check credentials
    const auto ai{_auth.check_credentials(rl.instant(), jwt)};
    if (ai.has_error) {
      core::http::route::send_error_response(
          request, response,
          Poco::Net::HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED, rl.id(),
          ai.error_msg);
      return;
    }

    // this (below, ai.login_required == true ) would be the case when
    // the JWT issued for this session has expired
    if (ai.login_required) {
      core::http::route::send_error_response(
          request, response,
          Poco::Net::HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED, rl.id(),
          "Login Required");
      return;
    }

    // check route access for this user
    if (!ai.user_info->can_exec(route->id())) {
      core::http::route::send_error_response(
          request, response,
          Poco::Net::HTTPResponse::HTTPStatus::HTTP_FORBIDDEN, rl.id(),
          "Access Denied");
      return;
    }

    // if route instance found and user has the required access rights for it,
    // then invoke its handler
    route->handle_request(request, response, *ai.user_info, rl.id(), rl.form(),
                          rl.file_data_list());
    //
  } catch (const std::exception &ex) {
    LOG_SEVERE(fmt::format("[{:s}]: Exception encountered: {:s}", request_id,
                           ex.what()));
    core::http::route::send_error_response(
        request, response, Poco::Net::HTTPResponse::HTTP_INTERNAL_SERVER_ERROR,
        request_id, "Please contact Support");
  } catch (...) {
    LOG_SEVERE(
        fmt::format("[{:s}]: Unknown Exception Encountered", request_id));
    core::http::route::send_error_response(
        request, response, Poco::Net::HTTPResponse::HTTP_INTERNAL_SERVER_ERROR,
        request_id, "Please contact Support");
  }
}

///////////////////////////////////////////////////////////////////////////////
// class auth::auth_info

web_server_app::auth::auth_info::auth_info() {
  TRACE_INIT();
  //
}

web_server_app::auth::auth_info::auth_info(const auth_info &o)
    : not_before{o.not_before},
      issued_at{o.issued_at},
      expiry{o.expiry},
      token{o.token},
      user_info{o.user_info},
      has_error{o.has_error},
      error_msg{o.error_msg},
      login_required{o.login_required} {
  TRACE_INIT();
  //
}

web_server_app::auth::auth_info::auth_info(auth_info &&o)
    : not_before{o.not_before},
      issued_at{o.issued_at},
      expiry{o.expiry},
      token{o.token},
      user_info{o.user_info},
      has_error{o.has_error},
      error_msg{o.error_msg},
      login_required{o.login_required} {
  TRACE_INIT();
  //
}

web_server_app::auth::auth_info &web_server_app::auth::auth_info::operator=(
    const web_server_app::auth::auth_info &o) {
  TRACE_INIT();
  //
  if (this != &o) {
    not_before = o.not_before;
    issued_at = o.issued_at;
    expiry = o.expiry;
    token = o.token;
    user_info = o.user_info;
    has_error = o.has_error;
    error_msg = o.error_msg;
    login_required = o.login_required;
  }
  return *this;
}

web_server_app::auth::auth_info &web_server_app::auth::auth_info::operator=(
    web_server_app::auth::auth_info &&o) {
  TRACE_INIT();
  //
  if (this != &o) {
    not_before = o.not_before;
    issued_at = o.issued_at;
    expiry = o.expiry;
    token = o.token;
    user_info = o.user_info;
    has_error = o.has_error;
    error_msg = o.error_msg;
    login_required = o.login_required;
  }
  return *this;
}

///////////////////////////////////////////////////////////////////////////////
// class auth

web_server_app::auth::auth(const Poco::Util::LayeredConfiguration &config)
    : _db{nullptr},
      _jwt_header{core::utils::base64_url::encode(
          R"({"alg":"HS256","typ":"JWT"})")},
      _jwt_issuer{config.getString("auth.jwt.content.issuer")},
      _jwt_subject{config.getString("auth.jwt.content.subject")},
      _jwt_audience{config.getString("auth.jwt.content.audience")},
      _jwt_lifetime{config.getInt("auth.jwt.lifetime-seconds")},
      _refresh_interval{config.getInt("auth.refresh-interval-seconds")},
      _cipher{[&config]() {
        const std::string key{config.getString("auth.encryption.key")};
        const std::string salt{config.getString("auth.encryption.salt")};
        const std::string cipher{config.getString("auth.encryption.cipher")};
        //
        CHECK(!key.empty(),
              "configuration parameter auth.encryption.key is empty");
        CHECK(!salt.empty(),
              "configuration parameter auth.encryption.salt is empty");
        CHECK(!cipher.empty(),
              "configuration parameter auth.encryption.cipher is empty");
        //
        const Poco::Crypto::Cipher::ByteVec bv_key{key.begin(), key.end()};
        const Poco::Crypto::Cipher::ByteVec bv_salt{salt.begin(), salt.end()};
        return Poco::Crypto::CipherFactory::defaultFactory().createCipher(
            Poco::Crypto::CipherKey{cipher, bv_key, bv_salt});
      }()},
      _hmac{config.getString("auth.jwt.hmac-key")} {
  TRACE_INIT();
  //
  CHECK(!_jwt_issuer.empty(),
        "configuration parameter auth.jwt.content.issuer is empty");
  CHECK(!_jwt_subject.empty(),
        "configuration parameter auth.jwt.content.subject is empty");
  CHECK(!_jwt_audience.empty(),
        "configuration parameter auth.jwt.content.audience is empty");
  CHECK(_jwt_lifetime > 0,
        "configuration parameter auth.jwt.lifetime-seconds is invalid (<= 0");
  CHECK(_refresh_interval > 0,
        "configuration parameter auth.refresh-interval-seconds is invalid (<= "
        "0)");
}

const char *web_server_app::auth::name() const {
  TRACE_INIT();
  //
  return _name.c_str();
}

void web_server_app::auth::initialize(Poco::Util::Application &app) {
  TRACE_INIT();
  // initialize the _db pointer
  _db = &core::app::web_server_app::get_db_conn(
      app.config().getString("auth.db_connection_name").c_str());
  CHECK(_db != nullptr,
        "error retrieving DB connection pool as specified by "
        "configuration parameter auth.db_connection_name");
}

void web_server_app::auth::uninitialize() {
  TRACE_INIT();
  //
  _db = nullptr;
}

const core::auth::user_info web_server_app::auth::load_user_info(
    const std::int32_t id, core::db::connection &conn) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, id > 0, "invalid id (user) parameter");
  core::auth::user_info ui{id};
  {
    // load the access rights (group/role based first)
    auto rs{conn.exec_select(
        "select rr.route_id, rr.f_create, rr.f_read, rr.f_update, "
        "rr.f_delete, rr.f_execute from auth.role_right rr inner join "
        "auth.user_role ur on rr.role_id = ur.role_id inner join "
        "auth.user u on ur.user_id = u.id inner join auth.route rt on "
        "rr.route_id = rt.id inner join auth.role r on rr.role_id = r.id "
        "where rr.active = true and rt.active = true and u.active = true "
        "and r.active = true and ur.active = true and u.id = $1;",
        {id})};
    while (rs->next()) {
      const auto [rid, c, r, u, d, x] =
          rs->get<std::int32_t, bool, bool, bool, bool, bool>(
              "route_id", "f_create", "f_read", "f_update", "f_delete",
              "f_execute");
      //
      CHECK((bool)rid, "null value retrieved for 'route_id' field");
      CHECK((bool)c, "null value retrieved for 'f_create' field");
      CHECK((bool)r, "null value retrieved for 'f_read' field");
      CHECK((bool)u, "null value retrieved for 'f_update' field");
      CHECK((bool)d, "null value retrieved for 'f_delete' field");
      CHECK((bool)x, "null value retrieved for 'f_execute' field");
      //
      //'or' with the previous flags (if any) so that
      // a user always get the highest access rights
      // for each route
      ui.add_route_access(*rid,
                          core::auth::compile_auth_flags(*c, *r, *u, *d, *x));
    }
  }
  {
    // load the user specific route access rights now
    auto rs{conn.exec_select(
        "select ur.route_id, ur.f_create, ur.f_read, ur.f_update, "
        "ur.f_delete, ur.f_execute from auth.user_right ur inner join "
        "auth.user u on ur.user_id = u.id inner join auth.route r on "
        "ur.route_id = r.id where ur.active = true and r.active = true "
        "and u.active = true and u.id = $1",
        {id})};
    while (rs->next()) {
      const auto [rid, c, r, u, d, x] =
          rs->get<std::int32_t, bool, bool, bool, bool, bool>(
              "route_id", "f_create", "f_read", "f_update", "f_delete",
              "f_execute");
      //
      CHECK((bool)rid, "null value retrieved for 'route_id' field");
      CHECK((bool)c, "null value retrieved for 'f_create' field");
      CHECK((bool)r, "null value retrieved for 'f_read' field");
      CHECK((bool)u, "null value retrieved for 'f_update' field");
      CHECK((bool)d, "null value retrieved for 'f_delete' field");
      CHECK((bool)x, "null value retrieved for 'f_execute' field");
      //
      //'hard' set specific user route access flags
      //(as user flags overrule role flags)
      ui.set_route_access(*rid,
                          core::auth::compile_auth_flags(*c, *r, *u, *d, *x));
    }
  }
  {
    // update any superfluous route access flags.
    //(these would be the cases where a user (directly or through a role)
    // would for example have 'create' access when this action is actually
    // not applicable for the route in question)
    for (auto &[did, flags] : ui.route_access()) {
      auto rs{conn.exec_select(
          "select f_create, f_read, f_update, f_delete, f_execute from "
          "auth.route where id = $1",
          {did})};
      while (rs->next()) {
        const auto [c, r, u, d, x] = rs->get<bool, bool, bool, bool, bool>(
            "f_create", "f_read", "f_update", "f_delete", "f_execute");
        //
        CHECK((bool)c, "null value retrieved for 'f_create' field");
        CHECK((bool)r, "null value retrieved for 'f_read' field");
        CHECK((bool)u, "null value retrieved for 'f_update' field");
        CHECK((bool)d, "null value retrieved for 'f_delete' field");
        CHECK((bool)x, "null value retrieved for 'f_execute' field");
        //
        ui.set_route_access(
            did, flags & core::auth::compile_auth_flags(*c, *r, *u, *d, *x));
      }
    }
  }
  {
    // load the user's roles
    auto rs{conn.exec_select(
        "select role_id from auth.user_role where active = true "
        "and user_id = $1",
        {id})};
    while (rs->next()) {
      const auto role_id{rs->get_int("role_id")};
      CHECK((bool)role_id, "null value retrieved for 'role_id' field");
      ui.add_role(*role_id);
    }
  }
  // ensure that any superfluous route entries for this user are removed
  ui.prune_route_access();
  //
  return ui;
}

const core::auth::user_info web_server_app::auth::load_user_info(
    const std::int32_t id) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, id > 0, "invalid id (user) parameter");
  CHECK(_db != nullptr, "db connection pool reference not initialised");
  //
  return _db->exec<core::auth::user_info>(
      [&](core::db::connection &conn, bool &) {
        return load_user_info(id, conn);
      });
}

const core::auth::user_info &web_server_app::auth::get_user_info(
    const std::int64_t now, const std::int32_t id) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, id > 0, "invalid id (user) parameter");
  //
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  auto i{_user_info_map.find(id)};
  if (i != _user_info_map.end()) {
    const auto &issued_at = i->second.second;
    // if we have an existing entry in cache,
    // check if it's stale; if it is, remove it, otherwise return it
    if (now <= (issued_at + _refresh_interval)) {
      return i->second.first;
    }
    // else, erase existing stale entry
    _user_info_map.erase(i);
  }
  // load update user info data from the database
  // and add it to the cache
  const auto p{_user_info_map.insert(
      std::make_pair(id, std::make_pair(load_user_info(id), now)))};
  CHECK(
      p.second,
      fmt::format(
          "error inserting user_info data into user_info_map for user id: {:d}",
          id));
  return p.first->second.first;
}

void web_server_app::auth::set_user_info(
    const std::int64_t now, const core::auth::user_info &ui) const {
  TRACE_INIT();
  //
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  const auto id{ui.id()};
  erase_user_info(id);
  CHECK(
      _user_info_map.insert(std::make_pair(id, std::make_pair(ui, now))).second,
      fmt::format(
          "Error inserting user_info data into user_info_map for user id: {:d}",
          id));
}

void web_server_app::auth::erase_user_info(const std::int32_t uid) const {
  TRACE_INIT();
  //
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  auto i{_user_info_map.find(uid)};
  if (i != _user_info_map.end()) {
    _user_info_map.erase(i);
  }
}

std::optional<core::auth::user_info> web_server_app::auth::authenticate(
    const std::string &username, const std::string &password) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !username.empty(),
          "invalid username parameter (null/empty)");
  CHECK_T(std::invalid_argument, !password.empty(),
          "invalid password parameter (null/empty)");
  CHECK(_db != nullptr, "db connection pool reference not initialised");
  //
  return _db->exec<std::optional<core::auth::user_info>>(
      [&](core::db::connection &conn, bool &) {
        auto rs{
            conn.exec_select("select id from auth.user where active = true and "
                             "username = $1 and password = $2",
                             {username, core::utils::md5::compute(password)},
                             {1})};  // mask password parameter (in logs)
        while (rs->next()) {
          const auto id{rs->get_int("id")};
          CHECK((bool)id, "null value retrieved for 'id' field");
          // create the user_info result object
          //(now that authentication was successful)
          // and return it
          return std::optional<core::auth::user_info>{
              core::auth::user_info{load_user_info(*id, conn)}};
        }
        // in case of authentication failure, return a no-value
        return std::optional<core::auth::user_info>{};
      });
}

std::string web_server_app::auth::gen_jwt(
    const core::auth::user_info &ui) const {
  //
  TRACE_INIT();
  //
  const std::int64_t now{core::utils::now<std::chrono::seconds>().count()};
  rapidjson::Document d{rapidjson::kObjectType};
  auto &a{d.GetAllocator()};
  const auto nbf{now};
  const auto iat{now};
  const auto exp{now + _jwt_lifetime};
  const auto jti{core::utils::gen_uuid()};
  //
  d.AddMember("iss", _jwt_issuer, a);
  d.AddMember("sub", _jwt_subject, a);
  // FIXME: at some stage, have a look at this (it's painful!)
  d.AddMember("aud",
              rapidjson::Value{rapidjson::kArrayType}.CopyFrom(
                  core::utils::string_to_json(_jwt_audience), a),
              a);
  d.AddMember("nbf", nbf, a);
  d.AddMember("iat", iat, a);
  d.AddMember("exp", exp, a);
  d.AddMember("jti", jti, a);
  d.AddMember("data",
              encrypt(fmt::format("{:d}:{:d}:{:d}:{:s}:{:d}", nbf, iat, exp,
                                  jti, ui.id())),
              a);
  //
  const std::string payload{
      core::utils::base64_url::encode(core::utils::json_to_string(d))};
  //
  const std::string signature{[&]() {
    std::lock_guard<std::recursive_mutex> lg{_mtx};
    Poco::DigestOutputStream dos{_hmac};
    dos << _jwt_header << '.' << payload;
    dos.close();
    return core::utils::base64_url::encode(_hmac.digest());
  }()};
  // update the user info map
  set_user_info(now, ui);
  //
  return fmt::format("{:s}.{:s}.{:s}", _jwt_header, payload, signature);
}

web_server_app::auth::auth_info web_server_app::auth::check_credentials(
    const int64_t instant, const std::string &jwt) const {
  //
  static std::regex _rx_jwt_{"(.*)\\.(.*)\\.(.*)"};
  static std::regex _rx_data_{
      "(\\d+)\\:(\\d+)\\:(\\d+)\\:([a-f0-9]{8}\\-[a-f0-9]{4}\\-[a-f0-9]{4}\\-"
      "["
      "a-f0-9]{4}\\-[a-f0-9]{12})\\:(\\d+)"};
  //
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !jwt.empty(), "invalid jwt parameter (empty)");
  //
  const auto now{instant / 1000};  // value in seconds
  auth_info ai{};
  try {
    // first check validity of the JWT received
    std::smatch sm;
    {
      std::lock_guard<std::recursive_mutex> lg{_mtx};
      CHECK(std::regex_match(jwt, sm, _rx_jwt_), "Invalid JWT Token");
    }
    CHECK(sm.size() == 4, "Invalid JWT Token");
    CHECK(sm[1] == _jwt_header, "Invalid JWT Token");
    {
      // check the signature (by regenerating the signature again
      // from the required data and checking it against the one received)
      std::lock_guard<std::recursive_mutex> lg{_mtx};
      Poco::DigestOutputStream dos{_hmac};
      dos << sm[1] << '.' << sm[2];
      dos.close();
      CHECK(sm[3].str() == core::utils::base64_url::encode(_hmac.digest()),
            "Invalid JWT token");
    }
    // now extract data from JWT
    const auto payload{core::utils::string_to_json((char *)std::addressof(
        *core::utils::base64_url::decode(sm[2]).begin()))};
    ai.not_before = core::utils::get_json_value<std::int64_t>(payload, "nbf");
    ai.issued_at = core::utils::get_json_value<std::int64_t>(payload, "iat");
    ai.expiry = core::utils::get_json_value<std::int64_t>(payload, "exp");
    ai.token = core::utils::get_json_value<std::string>(payload, "jti");
    //
    const auto &[nbf, iat, exp, jti, uid] = [&]() {
      // read the required data from the 'data' entry of the JWT
      const std::string ui_data{
          decrypt(core::utils::get_json_value<std::string>(payload, "data"))};
      {
        std::lock_guard<std::recursive_mutex> lg{_mtx};
        CHECK(std::regex_match(ui_data, sm, _rx_data_), "Invalid JWT Token");
      }
      return std::make_tuple(static_cast<std::int32_t>(std::stoi(sm[1])),
                             static_cast<std::int32_t>(std::stoi(sm[2])),
                             static_cast<std::int32_t>(std::stoi(sm[3])),
                             sm[4].str(),
                             static_cast<std::int32_t>(std::stoi(sm[5])));
    }();
    // check that the jti and other fields extracted match
    // (this is the 2-factor encryption check)
    CHECK(ai.not_before == nbf && ai.issued_at == iat && ai.expiry == exp &&
              jti == ai.token,
          "Invalid JWT token");

    // check for early use of JWT
    CHECK(now >= ai.not_before, "Inactive JWT token");

    // check for expiration of JWT; if it's fully expired,
    // then the user has to re-login
    if (now > ai.expiry) {
      erase_user_info(uid);
      ai.login_required = true;
      return ai;
    }

    // get_user_info will itself check if the user_info data
    // will need to be refreshed from the database
    ai.user_info = get_user_info(now, uid);
    return ai;
  } catch (const std::runtime_error &ex) {
    ai.has_error = true;
    ai.error_msg = ex.what();
    LOG_WARN(ex.what());
    return ai;
  } catch (...) {
    ai.has_error = true;
    ai.error_msg = "Unknown Exception encountered processing JWT token";
    LOG_WARN(ai.error_msg);
    return ai;
  }
}

std::string web_server_app::auth::encrypt(const std::string &data) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !data.empty(),
          "invalid data parameter (empty)");
  CHECK(_cipher.get() != nullptr, "_cipher reference not initialized");
  //
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  return _cipher->encryptString(data, Poco::Crypto::Cipher::ENC_BINHEX_NO_LF);
}

std::string web_server_app::auth::decrypt(const std::string &data) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !data.empty(),
          "invalid data parameter (empty)");
  CHECK(_cipher.get() != nullptr, "_cipher reference not initialized");
  //
  std::lock_guard<std::recursive_mutex> lg{_mtx};
  return _cipher->decryptString(data, Poco::Crypto::Cipher::ENC_BINHEX_NO_LF);
}

///////////////////////////////////////////////////////////////////////////////
// class web_server_app
web_server_app::web_server_app(core::http::route::route_factory_func rff)
    : _rff{[&]() {
        CHECK(rff != nullptr, "invalid rff (route_factory_func) parameter");
        return rff;
      }()} {
  TRACE_INIT();
  //
}

web_server_app::~web_server_app() {
  TRACE_INIT();
  //
}

void web_server_app::initialize(Poco::Util::Application &app) {
  TRACE_INIT();
  //
  // load configuration (fixing it with env var values as specified)
  const auto file_name{fix_config(app)};
  loadConfiguration(file_name);
  // delete the temporary (fixed/generate file after use)
  CHECK(std::remove(file_name.c_str()) == 0,
        core::utils::get_os_error_string());

  // add sub-systems

  // logging
  addSubsystem(_logging = new logging{});

  // db
  const std::string db_cfg{app.config().getString("db_connections")};
  CHECK(!db_cfg.empty(),
        "invalid db_connections configuration parameter (empty)");
  addSubsystem(_db = new db{});

  // auth
  addSubsystem(_auth = new auth{app.config()});

  // web server
  addSubsystem(_ws = new web_server{app.config(), *_auth, _rff});

  // call base class
  Poco::Util::ServerApplication::initialize(app);
}  // namespace core::app

std::string web_server_app::fix_config(Poco::Util::Application &app) {
  //
  auto regex_escape{[](const std::string &str) {
    TRACE_INIT();
    //
    CHECK_T(std::invalid_argument, !str.empty(),
            "invalid str parameter (empty)");
    std::stringstream sstr{};
    for (const auto &v : str) {
      if (std::strchr("{}$:-", v) != nullptr) {
        sstr << '\\';
      }
      sstr << v;
    }
    return sstr.str();
  }};
  //
  auto remove_file_ext{[](const std::string &str) {
    TRACE_INIT();
    //
    CHECK_T(std::invalid_argument, !str.empty(),
            "invalid str parameter (empty)");
    std::string res{str};
    const auto pos{res.find_last_of('.')};
    if (pos != res.npos) {
      res = res.substr(0, pos);
    }
    return res;
  }};
  //
  TRACE_INIT();
  //

  const auto file_name{
      fmt::format("{:s}.json", remove_file_ext(app.commandPath()))};
  std::ifstream ifs{file_name};
  CHECK(ifs.good(), fmt::format("Error reading configuration file: {:s} [{:s}]",
                                file_name, core::utils::get_os_error_string()));
  // get file size
  ifs.seekg(0, std::ios_base::end);
  const auto sz{ifs.tellg()};
  if (sz < 1) {
    ifs.close();
    CHECK(false,
          fmt::format("configuration file [{:s}] is of zero size", file_name));
  }
  ifs.seekg(0, std::ios_base::beg);
  // read file contents into string (buf)
  std::string buf{};
  buf.resize(sz);
  ifs.read(buf.data(), sz);
  if (ifs.fail()) {
    ifs.close();
    CHECK(false,
          fmt::format("Error reading configuration file contents: {:s} [{:s}]",
                      file_name, core::utils::get_os_error_string()));
  }
  ifs.close();
  {
    // look for any ${ENV_VAR:-actual_value} entries and replace
    // them with either (1) the value as specified in the corresponding
    // environment variable or (2) the actual value specified if no
    // corresponding environment variable exists or is set

    // work (i.e. update) a copy of buf
    // as changing the subject itself (buf) in the loop
    // might give undefined/undesired behaviour
    std::string nbuf{buf};
    const std::regex rx{R"__(\$\{(.*)\:\-(.*)\})__"};
    const std::sregex_iterator e{};
    // do the search on buf (which does not change in the loop)
    for (std::sregex_iterator i{
             std::sregex_iterator{buf.begin(), buf.end(), rx}};
         i != e; ++i) {
      const auto str_val{(*i)[0].str()};  // this is the whole match (i.e.
                                          // ${ENV_VAR:-actual_value})
      const auto env_key{
          (*i)[1].str()};  // this is the environment variable name to locate

      // build a regular expression from str_val above (escaping any special
      // regex chars beforehand)
      const auto irx{std::regex{regex_escape(str_val)}};
      // attempt to get the corresponding environment variable
      const auto env_val{std::getenv(env_key.c_str())};
      if (!core::utils::is_str_null(env_val)) {
        // if found, replace the ${ENV_VAR:-actual_value} with the
        // environment variable's value
        nbuf = std::regex_replace(nbuf, irx, env_val,
                                  std::regex_constants::match_any);
      } else {
        // otherwise, replace the ${ENV_VAR:-actual_value} with the
        // actual values specified (i.e. the part after ':-' in
        // ${ENV_VAR:-actual_value})
        const auto act_val{(*i)[2].str()};
        nbuf = std::regex_replace(nbuf, irx, act_val,
                                  std::regex_constants::match_any);
      }
    }
    // now set buf equal to nbuf
    buf = nbuf;
  }
  //
  {
    // now replace any entries with numeric values in quotes (such as: "port":
    // "9090") with 	"port": 9090 (i.e. superfluous quotes removed)

    std::string nbuf{buf};  // again, work on a copy of buf (so as not to change
                            // the subject of the regex in the loop)
    const std::regex rx{R"__((".*"\s{0,1}\:\s{0,1})\"(\d+)\")__"};
    const std::sregex_iterator s{buf.begin(), buf.end(), rx};
    const std::sregex_iterator e{};
    for (std::sregex_iterator i{s}; i != e; ++i) {
      const auto rep{fmt::format("{:s} {:s}", (*i)[1].str(), (*i)[2].str())};
      nbuf = std::regex_replace(nbuf, std::regex{regex_escape((*i)[0].str())},
                                rep, std::regex_constants::match_any);
    }
    // now set buf equal to nbuf
    buf = nbuf;
  }
  //
  CHECK(!buf.empty(), "JSON configuration string is empty after update/fixing");
  // write the fixed/updated json configuration string to file
  const std::string nfile_name{fmt::format("{:s}-tmp.json", app.commandPath())};
  std::ofstream ofs{nfile_name, std::ios_base::trunc};
  CHECK(
      ofs.good(),
      fmt::format("Error opening/creating new configuration file: {:s} [{:s}]",
                  nfile_name, core::utils::get_os_error_string()));
  ofs << buf;
  ofs.flush();
  if (ofs.fail()) {
    ofs.close();
    CHECK(false,
          fmt::format(
              "Error writing new configuration file contents: {:s} [{:s}]",
              nfile_name, core::utils::get_os_error_string()));
  }
  ofs.close();
  return nfile_name;
}

void web_server_app::uninitialize() {
  TRACE_INIT();
  //
  _logging = nullptr;
  _db = nullptr;
  _auth = nullptr;
  _ws = nullptr;
  Poco::Util::ServerApplication::uninitialize();

  {
    // FIXME: look at this
    // OpenSSL finalisation
    Poco::Crypto::uninitializeCrypto();
    //
    FIPS_mode_set(0);
    CRYPTO_set_locking_callback(nullptr);
    CRYPTO_set_id_callback(nullptr);

    ERR_remove_state(0);

    SSL_COMP_free_compression_methods();

    ENGINE_cleanup();

    CONF_modules_free();
    CONF_modules_unload(1);

    COMP_zlib_cleanup();

    ERR_free_strings();
    EVP_cleanup();

    CRYPTO_cleanup_all_ex_data();
  }
}

int web_server_app::main(const std::vector<std::string> &) {
  TRACE_INIT();
  //
  std::cout << "Server ready; awaiting requests..." << std::endl;
  waitForTerminationRequest();  // wait for CTRL-C or kill (SIGINT)
  //
  __terminated.store(true);
  __initialised.store(false);
  //
  return Application::EXIT_OK;
}

// static
int web_server_app::run(int argc, char **argv,
                        core::http::route::route_factory_func rff) {
  TRACE_INIT();
  //
  try {
    CHECK_T(std::invalid_argument, rff != nullptr,
            "invalid rff (route_factory_func) parameter");
    // check that instance is not already initialized
    bool init{false};
    CHECK(__initialised.compare_exchange_strong(init, true) &&
              __instance.get() == nullptr,
          "web_server_app instance already initialised");
    __instance.reset(new web_server_app{rff});
    __terminated.store(false);
    return ((Poco::Util::ServerApplication *)__instance)->run(argc, argv);
    //
  } catch (const std::exception &ex) {
    const auto msg{fmt::format("Exception encountered: {:s}", ex.what())};
    LOG_SEVERE(msg);
    return -1;
  } catch (...) {
    const auto msg{"Unknown exception encountered"};
    LOG_SEVERE(msg);
    return -1;
  }
}

core::db::db_connection_pool &web_server_app::get_db_conn(const char *name) {
  TRACE_INIT();
  CHECK(__initialised.load() && __instance.get() != nullptr,
        "web_server_app: instance not initialized");
  CHECK(__instance->_db != nullptr,
        "web_server_app: db Subsystem not initialized");
  return __instance->_db->get_db_conn(name);
}

core::db::db_connection_pool &web_server_app::get_db_conn(
    const std::string &name) {
  TRACE_INIT();
  //
  return get_db_conn(name.c_str());
}

bool web_server_app::terminated() {
  TRACE_INIT();
  //
  return __terminated.load();
}

///////////////////////////////////////////////////////////////////////////////
// statics
const std::regex request_logger::__rx{
    R"__(^form-data;\s{1}name="(.*)";\s{1}filename="(.*)"$)__"};
std::mutex request_logger::__mtx{};

Poco::SharedPtr<core::app::web_server_app>
    core::app::web_server_app::__instance{nullptr};
std::atomic<bool> core::app::web_server_app::__initialised{false};
std::atomic<bool> core::app::web_server_app::__terminated{true};

}  // namespace core::app
