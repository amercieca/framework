// C++
#include <cstring>
#include <random>
#include <set>
#include <stdexcept>

// local/project
#include <core/utils/simple_encrypt.h>
#include <core/utils/trace.h>
#include <core/utils/utils.h>

namespace core::utils {

std::string simple_encrypt::gen_key() {
  TRACE_INIT();
  //
  static char chars[] =
      R"(9zn*'l65=.m?o|pbui<^k{]@0da}_[7-h3`1f(c);t8jx#~+y4rs2q&%>gv,:e$w)";
  static std::size_t sz{std::strlen(chars)};
  //
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_real_distribution<double> dist(0.0, sz);

  // first shuffle the 'chars' above into kchars
  std::vector<char> sv{};
  for (std::size_t i{0UL}; i < sz; ++i) {
    sv.push_back(chars[i]);
  }
  std::string kchars(sz, 0);
  char *p{kchars.data()};
  while (!sv.empty()) {
    std::uniform_real_distribution<double> d(0.0, sv.size() - 1U);
    const auto idx{static_cast<std::uint32_t>(d(mt))};
    *p++ = sv[idx];
    sv.erase(sv.begin() + idx);
  }
  //
  std::string key((16 * 16), 0);
  p = key.data();
  // generate 16 'page index' chars, followed by 16 'pages' of 16 chars each.
  // each char within a page (including the index char page) has to be
  // unique within that page
  std::set<char> cset{};
  for (std::uint8_t i{0U}; i < 16U; ++i) {
    while (cset.size() < 16U) {
      const auto idx{static_cast<std::uint8_t>(dist(mt))};
      cset.insert(kchars[idx]);
    }
    for (const auto &c : cset) {
      *p++ = c;
    }
    cset.clear();
  }
  return key;
}

std::string simple_encrypt::encrypt(const std::string &key,
                                    const std::string &str) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !key.empty(), "invalid key argument (empty)");
  CHECK_T(std::invalid_argument, !str.empty(), "invalid str argument (empty)");
  //
  const char *s{key.data()};
  std::string res(str.length() * 3, 0);
  char *d{res.data()};
  std::uint8_t i{0U};
  for (const auto &c : str) {
    const std::uint8_t pg{static_cast<std::uint8_t>(c & 0xF)};
    *d++ = s[pg];  // page index/char
    const char *p{s + (pg * 0x10)};
    *d++ = p[(c >> 4) & 0xF];
    *d++ = p[c & 0xF];
    i += 16U;
  }
  return res;
}

std::string simple_encrypt::decrypt(const std::string &key,
                                    const std::string &str) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !key.empty(), "invalid key argument (empty)");
  CHECK_T(std::invalid_argument, !str.empty(), "invalid str argument (empty)");
  //
  const char *s{key.data()};
  std::string res(str.length() / 3, 0);
  char *d{res.data()};
  std::uint8_t i{0U};
  for (auto itr{str.begin()}; itr != str.end(); i += 16U) {
    // locate the page
    const std::uint8_t pg{
        static_cast<std::uint8_t>(std::strchr(s, *itr++) - s)};
    const char *p{s + (pg * 0x10)};
    std::uint8_t a{static_cast<std::uint8_t>(std::strchr(p, *itr++) - p)};
    std::uint8_t b{static_cast<std::uint8_t>(std::strchr(p, *itr++) - p)};
    *d++ = ((a << 4) | (b & 0xF));
  }
  return res;
}
}  // namespace core::utils