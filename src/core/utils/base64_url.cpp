// C++
#include <cstring>
#include <map>

// project/local
#include <core/utils/base64_url.h>
#include <core/utils/utils.h>

namespace core::utils {

std::string base64_url::encode(const char *s, const std::size_t length) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !is_str_null(s),
          "invalid s parameter (null/empty");
  const std::size_t cnt{length == static_cast<std::size_t>(-1L) ? std::strlen(s)
                                                                : length};
  std::string out{};
  out.resize(((cnt * 8) / 6) + (((cnt * 8) % 6) > 0 ? 1 : 0));
  char *d{out.data()};
  unsigned char bits{0U};
  unsigned char len{0U};
  for (std::size_t i{0UL}; i < cnt; ++i, ++s) {
    switch (len) {
      case 0U:
        *d++ = __alphabet[(*s >> 2) & 0x3F];
        bits = *s & 0x3;
        len = 2U;
        break;

      case 2U:
        *d++ = __alphabet[(bits << 4) | ((*s >> 4) & 0xF)];
        bits = *s & 0xF;
        len = 4U;
        break;

      case 4U:
        *d++ = __alphabet[(bits << 2) | ((*s >> 6) & 0x3)];
        *d++ = __alphabet[*s & 0x3F];
        bits = 0U;
        len = 0U;
        break;
    }
  }
  if (len != 0U) {
    *d++ = __alphabet[bits << (6U - len)];
  }
  return out;
}

std::string base64_url::encode(const std::string &str) {
  TRACE_INIT();
  //
  return encode(str.c_str(), str.length());
}

std::string base64_url::encode(const std::vector<unsigned char> &in) {
  TRACE_INIT();
  //
  const std::size_t cnt{in.size()};
  CHECK_T(std::invalid_argument, !in.empty(), "invalid in argument (empty)");
  std::string out{};
  out.resize(((cnt * 8) / 6) + (((cnt * 8) % 6) > 0 ? 1 : 0));
  char *d{out.data()};
  unsigned char bits{0U};
  unsigned char len{0U};
  const unsigned char *s{std::addressof(*in.begin())};
  for (std::size_t i{0UL}; i < cnt; ++i, ++s) {
    switch (len) {
      case 0U:
        *d++ = __alphabet[(*s >> 2) & 0x3F];
        bits = *s & 0x3;
        len = 2U;
        break;

      case 2U:
        *d++ = __alphabet[(bits << 4) | ((*s >> 4) & 0xF)];
        bits = *s & 0xF;
        len = 4U;
        break;

      case 4U:
        *d++ = __alphabet[(bits << 2) | ((*s >> 6) & 0x3)];
        *d++ = __alphabet[*s & 0x3F];
        bits = 0U;
        len = 0U;
        break;
    }
  }
  if (len != 0U) {
    *d++ = __alphabet[bits << (6U - len)];
  }
  return out;
}

std::vector<unsigned char> base64_url::decode(const std::string &str) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !str.empty(), "invalid str argument (empty)");
  const std::size_t cnt{str.length()};
  std::vector<unsigned char> res{};
  res.resize(((cnt * 6) / 8) + (((cnt * 6) % 8) > 0 ? 1 : 0));
  unsigned char *d{std::addressof(*res.begin())};
  unsigned char bits{0U};
  unsigned char len{0U};
  for (const auto &c : str) {
    const auto p{[&c]() {
      const auto i{__alphabet_map.find(c)};
      CHECK(
          i != __alphabet_map.end(),
          fmt::format("Unable to locate entry for value {:c} in alphabet", c));
      return i->second;
    }()};
    switch (len) {
      case 0U:
        bits = p;
        len = 6U;
        break;

      case 6U:
        *d++ = ((bits << 2) | ((p >> 4) & 0x3));
        bits = p & 0xF;
        len = 4U;
        break;

      case 4U:
        *d++ = ((bits << 4) | ((p >> 2) & 0xF));
        bits = p & 0x3;
        len = 2U;
        break;

      case 2U:
        *d++ = (bits << 6) | p;
        bits = 0U;
        len = 0U;
        break;
    }
  }
  if (bits != 0U) {
    *d++ = bits >> (8 - len);
  }
  std::size_t sz{static_cast<std::size_t>(d - std::addressof(*res.begin()))};
  if (sz != res.size()) {
    res.resize(sz);
  }
  return res;
}

// statics
const std::string core::utils::base64_url::__alphabet{
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"};

const std::map<char, unsigned char> core::utils::base64_url::__alphabet_map{
    []() {
      std::map<char, unsigned char> res{};
      unsigned char p{0U};
      for (const auto &c : __alphabet) {
        res[c] = p++;
      }
      return res;
    }()};

}  // namespace core::utils