#ifdef _WIN32
#undef min
#undef max
#define NOMINMAX

#include <iomanip>
#endif //WINDOWS

// rapidjson
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

// Poco
#include <Poco/UUIDGenerator.h>

// project/local
#include <core/utils/utils.h>

namespace core::utils {

///////////////////////////////////////////////////////////////////////////////
// functions
std::string get_os_error_string() {
  static std::mutex mtx;
  //
  TRACE_INIT();
  //
  fmt::memory_buffer buf;
  std::lock_guard<std::mutex> lg{mtx};
  buf.clear();
  fmt::format_system_error(buf, errno, "Error");
  return fmt::to_string(buf);
}

std::string json_to_string(const rapidjson::Value& json, const bool pretty) {
  TRACE_INIT();
  //
  rapidjson::StringBuffer buffer;
  buffer.Clear();
  if (pretty) {
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
    json.Accept(writer);
    return buffer.GetString();
  }
  rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
  json.Accept(writer);
  return buffer.GetString();
}

rapidjson::Document string_to_json(const std::string& str) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !str.empty(), "invalid std argument (empty)");
  rapidjson::Document d{};
  d.Parse(str);
  return d;
}

std::string gen_uuid() {
  TRACE_INIT();
  //
  return Poco::UUIDGenerator{}.createRandom().toString();
}

std::time_t now_time_t() {
  TRACE_INIT();
  //
  return std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
}

std::string& string_trim(std::string& str) {
  TRACE_INIT();
  //
  if (str.empty()) {
    return str;
  }
  auto s{str.begin()};
  while (s != str.end() && std::isspace(*s)) {
    ++s;
  }
  auto e = str.rbegin();
  while (e != str.rend() && std::isspace(*e)) {
    ++e;
  }
  if (s == str.end() || e == str.rend()) {
    str.clear();
  } else {
    str = std::move(std::string(s, e.base()));
  }
  return str;
}

std::string string_trim(const std::string& str) {
  TRACE_INIT();
  //
	std::string s{str};
	return string_trim(s);
}

///////////////////////////////////////////////////////////////////////////////
// class timer
timer::timer() : _beg(std::chrono::high_resolution_clock::now()) {}

timer::timer(const timer& o) : _beg(o._beg) {}

timer& timer::operator=(const timer& o) {
  if (this != &o) {
    _beg = o._beg;
  }
  return *this;
}

void timer::reset() { _beg = std::chrono::high_resolution_clock::now(); }

///////////////////////////////////////////////////////////////////////////////
// class byte_array

byte_array::byte_array() : _data{nullptr}, _length{0UL} { TRACE_INIT(); }

byte_array::byte_array(const std::size_t size)
    : _data{[&]() {
        TRACE_INIT();
        return size > 0UL ? (char*)new char[size] : (char*)nullptr;
      }()},
      _length{size},
      _is_str{false} {
  TRACE_INIT();
  if (size > 0UL) {
    std::memset(_data, 0, size);
  }
}

byte_array::byte_array(char* data)
    : _data{is_str_null(data) ? nullptr : data},
      _length{is_str_null(data) ? 0UL : std::strlen(data)},
      _is_str{is_str_null(data) ? false : true} {
  TRACE_INIT();
}

byte_array::byte_array(const char* data)
    : _data{copy_string(data)},
      _length{_data == nullptr ? 0UL : std::strlen(_data)},
      _is_str{_data != nullptr} {
  TRACE_INIT();
}

byte_array::byte_array(unsigned char* data, const std::size_t length)
    : _data{length > 0UL ? (char*)data : nullptr},
      _length{length},
      _is_str{false} {
  TRACE_INIT();
}

byte_array::byte_array(const unsigned char* data, const std::size_t length)
    : _data{length > 0UL ? copy_data(data, length) : nullptr},
      _length{length},
      _is_str{false} {
  TRACE_INIT();
}

byte_array::byte_array(const std::string& str)
    : _data{str.empty() ? nullptr : copy_string(str.c_str())},
      _length{str.length()},
      _is_str{true} {
  TRACE_INIT();
}

byte_array::byte_array(const byte_array& o)
    : _data{o._is_str ? copy_string(o._data)
                      : copy_data((const unsigned char*)o._data, o._length)},
      _length{o._length},
      _is_str{o._is_str} {
  TRACE_INIT();
}

byte_array::byte_array(byte_array&& o)
    : _data{o._data}, _length{o._length}, _is_str{o._is_str} {
  TRACE_INIT();
  o.reset(false);
}

byte_array::~byte_array() {
  TRACE_INIT();
  reset(true);
}

byte_array& byte_array::operator=(const byte_array& o) {
  TRACE_INIT();
  if (this != &o) {
    reset(true);
    _data = copy_data((const unsigned char*)o._data, o._length);
    _length = o._length;
    _is_str = o._is_str;
  }
  return *this;
}

byte_array& byte_array::operator=(byte_array&& o) {
  TRACE_INIT();
  if (this != &o) {
    reset(true);
    _data = o._data;
    _length = o._length;
    _is_str = o._is_str;
    o.reset(false);
  }
  return *this;
}

const char* byte_array::get() const {
  TRACE_INIT();
  return _data;
}

std::size_t byte_array::length() const {
  TRACE_INIT();
  return _length;
}

bool byte_array::is_str() const {
  TRACE_INIT();
  return _is_str;
}

bool byte_array::is_null() const {
  TRACE_INIT();
  return _data == nullptr;
}

void byte_array::reset(const bool free) {
  TRACE_INIT();
  if (_data != nullptr && free) {
    delete[] _data;
  }
  _data = nullptr;
  _length = -1L;
  _is_str = false;
}

char* byte_array::copy_string(const char* str) {
  TRACE_INIT();
  if (is_str_null(str)) {
    return nullptr;
  }
  const std::size_t size{strlen(str)};
  if (size < 1UL) {
    return nullptr;
  }
  char* data{new char[size + 1]};
  std::memcpy(data, str, size);
  data[size] = 0;  // null terminator
  return data;
}

char* byte_array::copy_data(const unsigned char* src, std::size_t size) {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, src != nullptr, "invalid src parameter");
  CHECK_T(std::invalid_argument, size > 0L, "invalid size parameter");
  char* data{new char[size]};
  std::memcpy(data, src, size);
  return data;
}

///////////////////////////////////////////////////////////////////////////////
// class raii_func
raii_func::raii_func(func f) : _f(f) { TRACE_INIT(); }

raii_func::~raii_func() {
  TRACE_INIT();
  if (_f != nullptr) {
    try {
      _f();
    } catch (const std::exception& ex) {
      LOG_SEVERE(std::string{"exception encountered executing raii_func: "}
                     .append(ex.what())
                     .c_str());
    } catch (...) {
      LOG_SEVERE("exception encountered executing raii_func");
    }
  }
}

#ifdef _WIN32
char* strptime(const char* s, const char* f, struct tm* tm) {
  // Isn't the C++ standard lib nice? std::get_time is defined such that its
  // format parameters are the exact same as strptime. Of course, we have to
  // create a string stream first, and imbue it with the current C locale, and
  // we also have to make sure we return the right things if it fails, or
  // if it succeeds, but this is still far simpler an implementation than any
  // of the versions in any of the C standard libraries.
  std::istringstream input(s);
  input.imbue(std::locale(setlocale(LC_ALL, nullptr)));
  input >> std::get_time(tm, f);
  if (input.fail()) {
    return nullptr;
  }
  return (char*)(s + input.tellg());
}
#endif

}  // namespace core::utils
