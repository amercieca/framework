// C++
#include <sstream>

// Poco
#include <Poco/Logger.h>

// fmt
#define FMT_STRING_ALIAS 1
#include <fmt/format.h>

// local/project
#include <core/utils/trace.h>

namespace core::utils {

///////////////////////////////////////////////////////////////////////////////
// class trace_mgr::stack

auto trace_mgr::stack::rbegin() { return base::c.rbegin(); }
auto trace_mgr::stack::rend() { return base::c.rend(); }
auto trace_mgr::stack::rbegin() const { return base::c.rbegin(); }
auto trace_mgr::stack::rend() const { return base::c.rend(); }

///////////////////////////////////////////////////////////////////////////////
// class trace_mgr

inline void trace_mgr::push(std::string &&str) {
  get_stack().push(std::move(str));
}

inline void trace_mgr::pop() { get_stack().pop(); }

inline trace_mgr::stack &trace_mgr::get_stack() {
  if (!__stack.get()) {
    __stack.reset(new stack{});
  }
  return *__stack.get();
}

std::string trace_mgr::dump_stack_trace() {
  std::stringstream sstr{};
  const auto &stack{get_stack()};
  for (auto i{stack.rbegin()}; i != stack.rend(); ++i) {
    sstr << '\t' << *i << '\n';
  }
  sstr.flush();
  return sstr.str();
}

void trace_mgr::dump_stack_trace(std::ostream &ostr) {
  const auto &stack{get_stack()};
  for (auto i{stack.rbegin()}; i != stack.rend(); ++i) {
    ostr << '\t' << *i << '\n';
  }
  ostr.flush();
}

///////////////////////////////////////////////////////////////////////////////
// class trace_ctx

trace_ctx::trace_ctx(const char *file, const char *func, std::size_t line)
    : _file(file), _func(func), _line(line) {
  trace_mgr::push(fmt::format("[{:s}:{:d} ({:s})]", _file, _line, _func));
}

trace_ctx::~trace_ctx() { trace_mgr::pop(); }

const std::string &trace_ctx::file() const { return _file; }

const std::string &trace_ctx::func() const { return _func; }

std::size_t trace_ctx::line() const { return _line; }

// statics
thread_local std::unique_ptr<core::utils::trace_mgr::stack>
    core::utils::trace_mgr::__stack{nullptr};

}  // namespace core::utils