// Poco
#include <Poco/Net/HTMLForm.h>
#include <Poco/Util/Application.h>

// project/local
#include <core/auth/auth.h>
#include <core/http/result.h>
#include <core/http/route.h>
#include <core/utils/utils.h>

namespace core::http {

///////////////////////////////////////////////////////////////////////////////
// class route

route::route(const std::int32_t id, const std::string& path,
             const std::uint8_t flags,
             const Poco::Net::HTTPServerRequest::PRIORITY priority,
             const std::uint32_t timeout, const std::uint32_t time_span,
             const std::uint32_t request_limit, const bool has_file_uploads,
             const std::uint32_t file_upload_size_limit,
             const std::uint32_t allowed_methods)
    : _id{id},
      _path{[&path]() {
        CHECK_T(std::invalid_argument, !path.empty(),
                "invalid path parameter (empty)");
        return path;
      }()},
      _flags{[&flags]() {
        CHECK_T(std::invalid_argument, flags != 0U,
                "invalid flags parameter (0); a route needs access flags");
        return flags;
      }()},
      _priority{priority},
      _timeout{[&timeout]() {
        CHECK_T(std::invalid_argument, timeout > 0U,
                "invalid timeout parameter (<=0)");
        return timeout;
      }()},
      _time_span{[&time_span, &request_limit]() {
        if (request_limit > 0U) {
          CHECK_T(std::invalid_argument, time_span > 0U,
                  "invalid time_span parameter (<=0)");
        }
        return time_span;
      }()},
      _request_limit{request_limit},  // request_limit can be zero
      _has_file_uploads{has_file_uploads},
      _file_upload_size_limit{[&file_upload_size_limit]() {
        CHECK_T(std::invalid_argument, file_upload_size_limit > 0U,
                "invalid file_upload_size_limit argument (<=0)");
        return file_upload_size_limit;
      }()},
      _allowed_methods([&allowed_methods]() {
        //
        CHECK_T(std::invalid_argument, allowed_methods != 0U,
                "invalid allowed_methods argument (zero); a route has to have "
                "one or more allowed methods");
        return allowed_methods;
      }()) {
  TRACE_INIT();
  //
}

route::~route() {
  TRACE_INIT();
  //
}

bool route::operator<(const route& o) const {
  TRACE_INIT();
  //
  if (this == &o) {
    return false;
  }
  // routes are sorted by path
  return _path < o._path;
}

std::int32_t route::id() const {
  TRACE_INIT();
  //
  return _id;
}

const std::string& route::path() const {
  TRACE_INIT();
  //
  return _path;
}

std::uint8_t route::flags() const {
  TRACE_INIT();
  //
  return _flags;
}

Poco::Net::HTTPServerRequest::PRIORITY route::priority() const {
  TRACE_INIT();
  //
  return _priority;
}

std::uint32_t route::timeout() const {
  TRACE_INIT();
  //
  return _timeout;
}

std::uint32_t route::time_span() const {
  TRACE_INIT();
  //
  return _time_span;
}

std::uint32_t route::request_limit() const {
  TRACE_INIT();
  //
  return _request_limit;
}

bool route::has_file_uploads() const {
  TRACE_INIT();
  //
  return _has_file_uploads;
}

std::uint32_t route::file_upload_size_limit() const {
  TRACE_INIT();
  //
  return _file_upload_size_limit;
}

std::uint32_t route::allowed_methods() const {
  TRACE_INIT();
  //
  return _allowed_methods;
}

bool route::is_method_allowed(const std::string& method) const {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !method.empty(),
          "invalid method argument (empty)");
  if (method == Poco::Net::HTTPRequest::HTTP_GET) {
    return (_allowed_methods & 0x1) == 0x1;
  }
  if (method == Poco::Net::HTTPRequest::HTTP_POST) {
    return (_allowed_methods & 0x2) == 0x2;
  }
  // all other methods are disallowed at this point in time
  return false;
}

bool route::has_too_many_requests() const {
  TRACE_INIT();
  //
  std::lock_guard<std::mutex> lg(_mtx);
  if (_request_limit < 1U) {
    // if request throttling is switched off for this route
    // then we always return false (i.e. the request can be processed)
    return false;
  }
  //
  const auto now{core::utils::now().count()};
  if (_time_points.size() >= _request_limit) {
    // if limit has been reached, try to remove any entries
    // which fall out of the preset timespan
    for (auto i{_time_points.begin()}; i != _time_points.end();) {
      const auto age{now - *i};
      if (age < static_cast<std::uint64_t>(_time_span)) {
        // if this entry is still within the _time_span,
        // then there is no need to check the other subsequent entries,
        // as these would be even younger than this entry and so
        // there is no need to check them (_time_points is an ordered set)
        break;
      }
      // otherwise remove this entry which is now out of the timespan
      i = _time_points.erase(i);
    }
  }
  // if there is enough space, return false
  // (adding 'now' to the timepoint set)
  if (_time_points.size() < _request_limit) {
    _time_points.insert(now);
    return false;
  }
  // otherwise return true
  //(i.e. too many requests for this route within its timespan)
  return true;
}

void route::load(core::db::connection& conn, route_factory_func rff) {
  TRACE_INIT();
  //
  static const std::uint32_t _sys_file_upload_limit_{static_cast<std::uint32_t>(
      Poco::Util::Application::instance().config().getInt(
          "httpd.requests.file_upload_size_limit_kb"))};
  //
  std::lock_guard<std::mutex> lg{__mtx};
  __routes.clear();
  const auto rs{conn.exec_select(
      "select id, name, f_create, f_read, f_update, f_delete, f_execute, "
      "priority, timeout, timespan, request_limit, has_file_uploads, "
      "file_upload_size_limit, allowed_methods from auth.route where active = "
      "true and id not in (1,2)")};
  while (rs->next()) {
    const auto& [id, n, c, r, u, d, x, p, t, s, l, f, z, m] =
        rs->get<std::int32_t, std::string, bool, bool, bool, bool, bool,
                std::int32_t, std::int32_t, std::int32_t, std::int32_t, bool,
                std::int32_t, std::int32_t>(
            "id", "name", "f_create", "f_read", "f_update", "f_delete",
            "f_execute", "priority", "timeout", "timespan", "request_limit",
            "has_file_uploads", "file_upload_size_limit", "allowed_methods");
    //
    CHECK((bool)id, "null value retrieved for 'id' field");
    CHECK((bool)n, "null value retrieved for 'name' field");
    CHECK((bool)c, "null value retrieved for 'f_create' field");
    CHECK((bool)r, "null value retrieved for 'f_read' field");
    CHECK((bool)u, "null value retrieved for 'f_update' field");
    CHECK((bool)d, "null value retrieved for 'f_delete' field");
    CHECK((bool)x, "null value retrieved for 'f_execute' field");
    CHECK((bool)p, "null value retrieved for 'priority' field");
    CHECK((bool)t, "null value retrieved for 'timeout' field");
    CHECK((bool)s, "null value retrieved for 'timespan' field");
    CHECK((bool)l, "null value retrieved for 'request_limit' field");
    CHECK((bool)f, "null value retrieved for 'has_file_uploads' field");
    CHECK((bool)z, "null value retrieved for 'file_upload_size_limit' field");
    CHECK((bool)m, "null value retrieved for 'allowed_methods' field");
    //
    const std::string name{*n};
    CHECK(!name.empty(), "invalid value for field 'name' retrieved (empty)");
    CHECK(*p >= 0 && *p <= 3, fmt::format("Invalid priority value encountered "
                                          "for route entry: {:d}:{:s}:{:d}",
                                          *id, name, *p));
    CHECK(*t >= 0L, fmt::format("Invalid timeout value encountered for route "
                                "entry: {:d}:{:s}:{:d}",
                                *id, name, *t));
    CHECK(*s >= 0L, fmt::format("Invalid timespan value encountered for route "
                                "entry: {:d}:{:s}:{:d}",
                                *id, name, *s));
    CHECK(*s >= 0L,
          fmt::format("Invalid request_limit value encountered for route "
                      "entry: {:d}:{:s}:{:d}",
                      *id, name, *l));
    if (*f) {  // has file uploads
      CHECK(*z > 0,
            "invalid field value retrieve for 'file_upload_size_limit' field "
            "(<1)");
      //
      if (static_cast<std::uint32_t>(*z) > _sys_file_upload_limit_) {
        LOG_WARN(
            fmt::format("Invalid file_upload_size_limit value encountered (> "
                        "httpd.requests.file_upload_size_limit_kb) for route "
                        "entry {:d}: {:s} ({:d} > {:d})",
                        *id, name, *z, _sys_file_upload_limit_));
      }
    }

    CHECK(__routes.find(name) == __routes.end(),
          fmt::format("Duplicate route [{:s}] encountered.", name));
    CHECK(__routes
              .insert(std::make_pair(
                  name,
                  rff(name, *id,
                      core::auth::compile_auth_flags(*c, *r, *u, *d, *x),
                      static_cast<Poco::Net::HTTPServerRequest::PRIORITY>(*p),
                      static_cast<std::uint32_t>(*t),
                      static_cast<std::uint32_t>(*s),
                      static_cast<std::uint32_t>(*l), *f,
                      static_cast<std::uint64_t>(*z),
                      static_cast<std::uint32_t>(*m))))
              .second,
          fmt::format(
              "Error encountered inserting route entry {:s} into route map",
              name));
  }
}

void route::send_ok_response(Poco::Net::HTTPServerRequest&,
                             Poco::Net::HTTPServerResponse& response,
                             core::http::result& result) {
  TRACE_INIT();
  //
  const std::string content{core::utils::json_to_string(result.to_json())};
  CHECK(!content.empty(),
        fmt::format("result content for request {:s} is blank/empty",
                    result.uuid()));
  response.setStatusAndReason(Poco::Net::HTTPResponse::HTTP_OK);
  response.setContentType(__CT_APPLICATION_JSON);
  response.setContentLength(content.length());
  (response.send() << content).flush();
  //
  set_response_result_value(response, content);
}

void route::send_error_response(Poco::Net::HTTPServerRequest& request,
                                Poco::Net::HTTPServerResponse& response,
                                Poco::Net::HTTPResponse::HTTPStatus status,
                                const std::string& request_id,
                                const std::string& message) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !request_id.empty(),
          "invalid request_id parameter");
  response.setStatusAndReason(status);
  core::http::result result{
      request_id, static_cast<std::uint16_t>(status),
      Poco::Net::HTTPResponse::getReasonForStatus(status)};
  if (!message.empty()) {
    result.set("error", fmt::format("[{:s}] {:s}", request.getURI(), message));
  }
  const std::string content{core::utils::json_to_string(result.to_json())};
  CHECK(!content.empty(),
        fmt::format("result content for request {:s} is blank/empty",
                    result.uuid()));
  response.setContentType(__CT_APPLICATION_JSON);
  response.setContentLength(content.length());
  (response.send() << content).flush();
  response.setKeepAlive(false);
  //
  set_response_result_value(response, content);
}

const route* route::get_route(const std::string& path) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !path.empty(),
          "invalid path parameter (empty)");
  std::lock_guard<std::mutex> lg{__mtx};
  auto i{__routes.find(path)};
  return i == __routes.end() ? nullptr : i->second.get();
}

void route::set_response_result_value(Poco::Net::HTTPServerResponse& response,
                                      const std::string& result) {
  TRACE_INIT();
  CHECK_T(std::invalid_argument, !result.empty(),
          "invalid result parameter (empty)");
  // set the 'Result' value on the response
  //(only do this after the call to send() so that the value for
  //'Result' is not send back as a header to the caller
  CHECK(response.sent(),
        "call to set_result_value before response has been sent!");

  // Note that this entry is required so that it's contents are logged
  // to the database (as a request_data ('X') type record)
  response.set("Result", result);
}

// statics
const std::string core::http::route::__CT_PLAIN_TEXT{"text/plain"};
const std::string core::http::route::__CT_TEXT_HTML{"text/html"};
const std::string core::http::route::__CT_APPLICATION_JSON{"application/json"};
const std::string core::http::route::__ROUTE_NOT_FOUND{"/not-found-route"};
const std::string core::http::route::__ROUTE_LOGIN{"/login-route"};
const std::int32_t core::http::route::__ROUTE_ID_NOT_FOUND{1};
const std::int32_t core::http::route::__ROUTE_ID_LOGIN{2};
std::map<std::string, std::unique_ptr<route>> core::http::route::__routes{};
std::mutex core::http::route::__mtx{};

}  // namespace core::http