#include <core/http/result.h>

namespace core::http {

result::result(const std::string &uuid, const std::chrono::milliseconds instant,
               const std::uint16_t status_code, const std::string &status_msg)
    : _uuid{[&]() {
        CHECK(!uuid.empty(), "invalid uuid parameter (empty)");
        return uuid;
      }()},
      _instant{instant},
      _status_code{status_code},
      _status_msg{status_msg},
      _alloc{_doc.GetAllocator()} {
  TRACE_INIT();
  //
  _doc.AddMember("payload", rapidjson::Value{rapidjson::kObjectType}, _alloc);
  _payload = &_doc["payload"];
}

result::result(const std::string &uuid, const std::uint16_t status_code,
               const std::string &status_msg)
    : _uuid{[&]() {
        CHECK(!uuid.empty(), "invalid uuid parameter (empty)");
        return uuid;
      }()},
      _instant{core::utils::now()},
      _status_code{status_code},
      _status_msg{status_msg},
      _alloc{_doc.GetAllocator()} {
  TRACE_INIT();
  //
  _doc.AddMember("payload", rapidjson::Value{rapidjson::kObjectType}, _alloc);
  _payload = &_doc["payload"];
}

const std::string &result::uuid() const {
  TRACE_INIT();
  //
  return _uuid;
}

std::chrono::milliseconds result::instant() const {
  TRACE_INIT();
  //
  return _instant;
}

std::uint16_t result::status_code() const {
  TRACE_INIT();
  //
  return _status_code;
}

const std::string &result::status_msg() const {
  TRACE_INIT();
  //
  return _status_msg;
}

result &result::set(const std::string &name, const std::string &value) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !name.empty(),
          "invalid name parameter (empty)");
  remove_member(name);
  _payload->AddMember(rapidjson::Value{name, _alloc}.Move(),
                      rapidjson::Value{value, _alloc}.Move(), _alloc);
  return *this;
}

result &result::set(const std::string &name, rapidjson::Value &v) {
  TRACE_INIT();
  //
  CHECK_T(std::invalid_argument, !name.empty(),
          "invalid name parameter (empty)");
  remove_member(name);
  _payload->AddMember(rapidjson::Value{name, _alloc}.Move(), v, _alloc);
  return *this;
}

result &result::set(rapidjson::Value &v) {
  TRACE_INIT();
  //
  remove_member("payload");
  _doc.AddMember("payload", v, _alloc);
  _payload = &_doc["payload"];
  return *this;
}

void result::remove_member(const std::string &name) {
  TRACE_INIT();
  //
  auto i{_payload->FindMember(name)};
  if (i != _payload->MemberEnd()) {
    _payload->RemoveMember(i);
  }
}

const rapidjson::Document &result::to_json() {
  TRACE_INIT();
  //
  remove_member("uuid");
  remove_member("instant");
  remove_member("status_code");
  remove_member("status_msg");
  _doc.AddMember("uuid", _uuid, _alloc)
      .AddMember("instant", _instant.count(), _alloc)
      .AddMember("status_code", _status_code, _alloc)
      .AddMember("status_msg", _status_msg, _alloc);
  return _doc;
}

}  // namespace core::http