#!/bin/bash

PROC_NAME=`pwd`/../build/bin/x
CMD="start-stop-daemon -q"

#environment variables
export MYSQL_UNIX_PORT=/var/run/mysqld/mysqld.sock

usage() {
	echo "usage: ${0} [start|stop|status|restart|console]";
	exit -1;
}

console() {
	${CMD} --status --exec ${PROC_NAME} && stop;	
	${CMD} --start --exec ${PROC_NAME};
	return ${?};
}

start() {
	${CMD} --start --exec ${PROC_NAME} -- --daemon;
	return ${?};
}

stop() {
	${CMD} --stop --signal 15 --exec ${PROC_NAME} --retry 5;
	return ${?};
}

restart() {
	${CMD} --status --exec ${PROC_NAME} && stop;
	start;
	return ${?};
}

status() {
	${CMD} --status --exec ${PROC_NAME};
	case ${?} in 
		0|1)
		echo "${PROC_NAME} is running";
		;;
		
		3)
		echo "${PROC_NAME} is NOT running";
		;;		
		
		*)
		echo "Unable to determine ${PROC_NAME} status";
		;;
	esac
	return ${?}
}


[ ${#} -lt 1 ] && usage;

case ${1} in 
	console)
	console;
	;;
	
	start) 
	start;
	;;
	
	stop)
	stop;
	;;
	
	status)
	status;
	;;
	
	restart)
	restart;
	;;
	
	*)
	usage;
	;;
esac;
	

exit ${?}
